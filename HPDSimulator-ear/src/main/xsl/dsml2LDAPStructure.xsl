<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    version="2.0">
    <xsl:output encoding="UTF-8" method="xml"/>
    
    <xsl:template match="/">
        <xsl:element name="LDAPStructure">
            <xsl:attribute name="name">GeneratedFile</xsl:attribute>
            <xsl:attribute name="version">1.0</xsl:attribute>
            <xsl:apply-templates select="node()"/>
        </xsl:element>
    </xsl:template>
    
    <xsl:template match="text()">
        <!-- do nothing there -->
    </xsl:template>
    
    <xsl:template match="searchResultEntry[matches(@dn, 'ou=attributeTypes', 'i')]">
        <xsl:element name="attributeType">
            <xsl:attribute name="name"><xsl:value-of select="attr[@name='m-name']/value"/></xsl:attribute>
            <xsl:attribute name="oid"><xsl:value-of select="attr[@name='m-oid']/value"/></xsl:attribute>
            <xsl:comment>dn="<xsl:value-of select="@dn"/>"</xsl:comment>
            <xsl:element name="description"><xsl:value-of select="attr[@name='m-description']/value"/></xsl:element>
            <xsl:if test="count(attr[@name='m-singlevalue']) &gt; 0">
                <xsl:element name="singlevalue"><xsl:value-of select="lower-case(attr[@name='m-singlevalue']/value)"/></xsl:element>
            </xsl:if>
            <xsl:if test="count(attr[@name='m-syntax']) &gt; 0">
                <xsl:element name="syntax"><xsl:value-of select="attr[@name='m-syntax']/value"/></xsl:element>
            </xsl:if>
            <xsl:if test="count(attr[@name='m-equality']) &gt; 0">
                <xsl:element name="equality"><xsl:value-of select="attr[@name='m-equality']/value"/></xsl:element>
            </xsl:if>
            <xsl:if test="count(attr[@name='m-ordering']) &gt; 0">
                <xsl:element name="ordering"><xsl:value-of select="attr[@name='m-ordering']/value"/></xsl:element>
            </xsl:if>
            <xsl:if test="count(attr[@name='m-substr']) &gt; 0">
                <xsl:element name="substr"><xsl:value-of select="attr[@name='m-substr']/value"/></xsl:element>
            </xsl:if>
            <xsl:if test="count(attr[@name='m-length']) &gt; 0">
            <xsl:element name="length"><xsl:value-of select="attr[@name='m-length']/value"/></xsl:element>
            </xsl:if>
        </xsl:element>
    </xsl:template>
    <xsl:template match="searchResultEntry[matches(@dn, 'ou=objectClasses', 'i')]">
        <xsl:element name="objectClass">
            <xsl:attribute name="name"><xsl:value-of select="attr[@name='m-name']/value"/></xsl:attribute>
            <xsl:attribute name="oid"><xsl:value-of select="attr[@name='m-oid']/value"/></xsl:attribute>
            <xsl:comment>dn="<xsl:value-of select="@dn"/>"</xsl:comment>
            <xsl:element name="description"><xsl:value-of select="attr[@name='m-description']/value"/></xsl:element>
            <xsl:element name="supobjectclass"><xsl:value-of select="attr[@name='m-supobjectclass']/value"/></xsl:element>
            <xsl:if test="count(attr[@name='m-typeobjectclass']) &gt; 0">
                <xsl:element name="typeobjectclass"><xsl:value-of select="attr[@name='m-typeobjectclass']/value"/></xsl:element>
            </xsl:if>
            <xsl:for-each select="attr[@name='m-must']/value">
                <xsl:element name="must"><xsl:value-of select="text()"/></xsl:element>
            </xsl:for-each>
            <xsl:for-each select="attr[@name='m-may']/value">
                <xsl:element name="may"><xsl:value-of select="text()"/></xsl:element>
            </xsl:for-each>
        </xsl:element>
    </xsl:template>
</xsl:stylesheet>