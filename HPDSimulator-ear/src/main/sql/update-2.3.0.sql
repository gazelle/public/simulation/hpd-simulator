ALTER TABLE ldap_partition ADD COLUMN simulator character varying(255);
UPDATE ldap_partition SET simulator = 'HPD_PROV_INFO_DIR';


-- eHealthSuisse only
INSERT INTO app_configuration (id, variable, value) VALUES (nextval('app_configuration_id_seq'), 'cpi_prov_wsdl', 'http://localhost:8080/HPDSimulator-ejb/CPIProvider_Service/CPIProvider_PortType?wsdl');
INSERT INTO tf_transaction(id, keyword, name, description) VALUES (nextval('tf_transaction_id_seq'), 'CIQ', 'Community Information Query', 'EPD specific transaction: Community Information Query');
INSERT INTO affinity_domain_transactions (affinity_domain_id, transaction_id) VALUES ((SELECT id from affinity_domain WHERE keyword = 'CH'), (select id from tf_transaction where keyword = 'CIQ'));
INSERT INTO usage_metadata (id, transaction_id, affinity_id, action) VALUES (nextval('usage_id_seq'), (select id from tf_transaction where keyword = 'CIQ'), (SELECT id from affinity_domain WHERE keyword = 'CH'), 'Provider Information Delta Download');
INSERT INTO tf_actor(id, description, keyword, name, can_act_as_responder) VALUES (nextval('tf_actor_id_seq'), 'CPI Provider', 'CPI_PROV', 'CPI Provider', true);
INSERT INTO tf_actor(id, description, keyword, name, can_act_as_responder) VALUES (nextval('tf_actor_id_seq'), 'CPI Consumer', 'CPI_CONS', 'CPI Consumer', false);
INSERT INTO ldap_partition (id, base_dn, creation_date, creator, description, dn_modify_regex, dn_search_regex, is_default, name, server_ip, server_port, simulator) VALUES (nextval('ldap_partition_id_seq'), 'dc=CPI,o=BAG,c=CH', null, null, 'CH:CPI partition', null, '((uid=.*,)?ou=(CHEndpoint|CHCommunity),)?dc=CPI,o=BAG,c=CH', false, 'Gazelle CPI Directory', '127.0.0.1', 10389, 'CPI_PROV');

