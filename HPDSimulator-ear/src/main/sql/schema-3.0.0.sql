--
-- PostgreSQL database dump
--

-- Dumped from database version 9.6.7
-- Dumped by pg_dump version 9.6.7

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SET check_function_bodies = false;
SET client_min_messages = warning;
SET row_security = off;

--
-- Name: plpgsql; Type: EXTENSION; Schema: -; Owner: 
--

CREATE EXTENSION IF NOT EXISTS plpgsql WITH SCHEMA pg_catalog;


--
-- Name: EXTENSION plpgsql; Type: COMMENT; Schema: -; Owner: 
--

COMMENT ON EXTENSION plpgsql IS 'PL/pgSQL procedural language';


SET search_path = public, pg_catalog;

SET default_tablespace = '';

SET default_with_oids = false;

--
-- Name: affinity_domain; Type: TABLE; Schema: public; Owner: gazelle
--

CREATE TABLE affinity_domain (
    id integer NOT NULL,
    keyword character varying(255),
    label_to_display character varying(255),
    profile character varying(255)
);


ALTER TABLE affinity_domain OWNER TO gazelle;

--
-- Name: affinity_domain_id_seq; Type: SEQUENCE; Schema: public; Owner: gazelle
--

CREATE SEQUENCE affinity_domain_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE affinity_domain_id_seq OWNER TO gazelle;

--
-- Name: affinity_domain_transactions; Type: TABLE; Schema: public; Owner: gazelle
--

CREATE TABLE affinity_domain_transactions (
    affinity_domain_id integer NOT NULL,
    transaction_id integer NOT NULL
);


ALTER TABLE affinity_domain_transactions OWNER TO gazelle;

--
-- Name: app_configuration; Type: TABLE; Schema: public; Owner: gazelle
--

CREATE TABLE app_configuration (
    id integer NOT NULL,
    value character varying(255),
    variable character varying(255)
);


ALTER TABLE app_configuration OWNER TO gazelle;

--
-- Name: app_configuration_id_seq; Type: SEQUENCE; Schema: public; Owner: gazelle
--

CREATE SEQUENCE app_configuration_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE app_configuration_id_seq OWNER TO gazelle;

--
-- Name: cfg_configuration; Type: TABLE; Schema: public; Owner: gazelle
--

CREATE TABLE cfg_configuration (
    id integer NOT NULL,
    comment character varying(255),
    approved boolean NOT NULL,
    is_secured boolean,
    host_id integer
);


ALTER TABLE cfg_configuration OWNER TO gazelle;

--
-- Name: cfg_configuration_id_seq; Type: SEQUENCE; Schema: public; Owner: gazelle
--

CREATE SEQUENCE cfg_configuration_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE cfg_configuration_id_seq OWNER TO gazelle;

--
-- Name: cfg_dicom_scp_configuration; Type: TABLE; Schema: public; Owner: gazelle
--

CREATE TABLE cfg_dicom_scp_configuration (
    id integer NOT NULL,
    comments character varying(255),
    ae_title character varying(16) NOT NULL,
    modality_type character varying(255),
    port integer,
    port_secure integer,
    transfer_role character varying(255) NOT NULL,
    configuration_id integer NOT NULL,
    gs_system_id integer,
    sop_class_id integer NOT NULL
);


ALTER TABLE cfg_dicom_scp_configuration OWNER TO gazelle;

--
-- Name: cfg_dicom_scp_configuration_id_seq; Type: SEQUENCE; Schema: public; Owner: gazelle
--

CREATE SEQUENCE cfg_dicom_scp_configuration_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE cfg_dicom_scp_configuration_id_seq OWNER TO gazelle;

--
-- Name: cfg_dicom_scu_configuration; Type: TABLE; Schema: public; Owner: gazelle
--

CREATE TABLE cfg_dicom_scu_configuration (
    id integer NOT NULL,
    comments character varying(255),
    ae_title character varying(16) NOT NULL,
    modality_type character varying(255),
    port integer,
    port_secure integer,
    transfer_role character varying(255) NOT NULL,
    configuration_id integer NOT NULL,
    gs_system_id integer,
    sop_class_id integer NOT NULL
);


ALTER TABLE cfg_dicom_scu_configuration OWNER TO gazelle;

--
-- Name: cfg_dicom_scu_configuration_id_seq; Type: SEQUENCE; Schema: public; Owner: gazelle
--

CREATE SEQUENCE cfg_dicom_scu_configuration_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE cfg_dicom_scu_configuration_id_seq OWNER TO gazelle;

--
-- Name: cfg_hl7_initiator_configuration; Type: TABLE; Schema: public; Owner: gazelle
--

CREATE TABLE cfg_hl7_initiator_configuration (
    id integer NOT NULL,
    comments character varying(255),
    assigning_authority character varying(255),
    sending_receiving_application character varying(255) NOT NULL,
    sending_receiving_facility character varying(255) NOT NULL,
    configuration_id integer NOT NULL,
    gs_system_id integer
);


ALTER TABLE cfg_hl7_initiator_configuration OWNER TO gazelle;

--
-- Name: cfg_hl7_initiator_configuration_id_seq; Type: SEQUENCE; Schema: public; Owner: gazelle
--

CREATE SEQUENCE cfg_hl7_initiator_configuration_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE cfg_hl7_initiator_configuration_id_seq OWNER TO gazelle;

--
-- Name: cfg_hl7_responder_configuration; Type: TABLE; Schema: public; Owner: gazelle
--

CREATE TABLE cfg_hl7_responder_configuration (
    id integer NOT NULL,
    comments character varying(255),
    assigning_authority character varying(255),
    sending_receiving_application character varying(255) NOT NULL,
    sending_receiving_facility character varying(255) NOT NULL,
    port integer,
    port_out integer,
    port_secure integer,
    configuration_id integer NOT NULL,
    gs_system_id integer
);


ALTER TABLE cfg_hl7_responder_configuration OWNER TO gazelle;

--
-- Name: cfg_hl7_responder_configuration_id_seq; Type: SEQUENCE; Schema: public; Owner: gazelle
--

CREATE SEQUENCE cfg_hl7_responder_configuration_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE cfg_hl7_responder_configuration_id_seq OWNER TO gazelle;

--
-- Name: cfg_hl7_v3_initiator_configuration; Type: TABLE; Schema: public; Owner: gazelle
--

CREATE TABLE cfg_hl7_v3_initiator_configuration (
    id integer NOT NULL,
    comments character varying(255),
    assigning_authority character varying(255),
    sending_receiving_application character varying(255) NOT NULL,
    sending_receiving_facility character varying(255) NOT NULL,
    configuration_id integer NOT NULL,
    gs_system_id integer
);


ALTER TABLE cfg_hl7_v3_initiator_configuration OWNER TO gazelle;

--
-- Name: cfg_hl7_v3_initiator_configuration_id_seq; Type: SEQUENCE; Schema: public; Owner: gazelle
--

CREATE SEQUENCE cfg_hl7_v3_initiator_configuration_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE cfg_hl7_v3_initiator_configuration_id_seq OWNER TO gazelle;

--
-- Name: cfg_hl7_v3_responder_configuration; Type: TABLE; Schema: public; Owner: gazelle
--

CREATE TABLE cfg_hl7_v3_responder_configuration (
    id integer NOT NULL,
    comments character varying(255),
    assigning_authority character varying(255),
    sending_receiving_application character varying(255) NOT NULL,
    sending_receiving_facility character varying(255) NOT NULL,
    port integer,
    port_secured integer,
    url character varying(255) NOT NULL,
    usage character varying(255),
    configuration_id integer NOT NULL,
    gs_system_id integer
);


ALTER TABLE cfg_hl7_v3_responder_configuration OWNER TO gazelle;

--
-- Name: cfg_hl7_v3_responder_configuration_id_seq; Type: SEQUENCE; Schema: public; Owner: gazelle
--

CREATE SEQUENCE cfg_hl7_v3_responder_configuration_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE cfg_hl7_v3_responder_configuration_id_seq OWNER TO gazelle;

--
-- Name: cfg_host; Type: TABLE; Schema: public; Owner: gazelle
--

CREATE TABLE cfg_host (
    id integer NOT NULL,
    alias character varying(255),
    comment character varying(255),
    hostname character varying(255) NOT NULL,
    ip character varying(255)
);


ALTER TABLE cfg_host OWNER TO gazelle;

--
-- Name: cfg_host_id_seq; Type: SEQUENCE; Schema: public; Owner: gazelle
--

CREATE SEQUENCE cfg_host_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE cfg_host_id_seq OWNER TO gazelle;

--
-- Name: cfg_sop_class; Type: TABLE; Schema: public; Owner: gazelle
--

CREATE TABLE cfg_sop_class (
    id integer NOT NULL,
    keyword character varying(255) NOT NULL,
    name character varying(255)
);


ALTER TABLE cfg_sop_class OWNER TO gazelle;

--
-- Name: cfg_sop_class_id_seq; Type: SEQUENCE; Schema: public; Owner: gazelle
--

CREATE SEQUENCE cfg_sop_class_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE cfg_sop_class_id_seq OWNER TO gazelle;

--
-- Name: cfg_syslog_configuration; Type: TABLE; Schema: public; Owner: gazelle
--

CREATE TABLE cfg_syslog_configuration (
    id integer NOT NULL,
    comments character varying(255),
    port integer,
    configuration_id integer NOT NULL,
    gs_system_id integer
);


ALTER TABLE cfg_syslog_configuration OWNER TO gazelle;

--
-- Name: cfg_syslog_configuration_id_seq; Type: SEQUENCE; Schema: public; Owner: gazelle
--

CREATE SEQUENCE cfg_syslog_configuration_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE cfg_syslog_configuration_id_seq OWNER TO gazelle;

--
-- Name: cfg_web_service_configuration; Type: TABLE; Schema: public; Owner: gazelle
--

CREATE TABLE cfg_web_service_configuration (
    id integer NOT NULL,
    comments character varying(255),
    port integer,
    port_secured integer,
    url character varying(255) NOT NULL,
    usage character varying(255),
    assigning_authority character varying(255),
    configuration_id integer NOT NULL,
    gs_system_id integer
);


ALTER TABLE cfg_web_service_configuration OWNER TO gazelle;

--
-- Name: cfg_web_service_configuration_id_seq; Type: SEQUENCE; Schema: public; Owner: gazelle
--

CREATE SEQUENCE cfg_web_service_configuration_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE cfg_web_service_configuration_id_seq OWNER TO gazelle;

--
-- Name: cmn_company_details; Type: TABLE; Schema: public; Owner: gazelle
--

CREATE TABLE cmn_company_details (
    id integer NOT NULL,
    company_keyword character varying(255)
);


ALTER TABLE cmn_company_details OWNER TO gazelle;

--
-- Name: cmn_company_details_id_seq; Type: SEQUENCE; Schema: public; Owner: gazelle
--

CREATE SEQUENCE cmn_company_details_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE cmn_company_details_id_seq OWNER TO gazelle;

--
-- Name: cmn_home; Type: TABLE; Schema: public; Owner: gazelle
--

CREATE TABLE cmn_home (
    id integer NOT NULL,
    home_title character varying(255),
    iso3_language character varying(255),
    main_content text
);


ALTER TABLE cmn_home OWNER TO gazelle;

--
-- Name: cmn_home_id_seq; Type: SEQUENCE; Schema: public; Owner: gazelle
--

CREATE SEQUENCE cmn_home_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE cmn_home_id_seq OWNER TO gazelle;

--
-- Name: cmn_ip_address; Type: TABLE; Schema: public; Owner: gazelle
--

CREATE TABLE cmn_ip_address (
    id integer NOT NULL,
    added_by character varying(255),
    added_on timestamp without time zone,
    value character varying(255),
    company_details_id integer
);


ALTER TABLE cmn_ip_address OWNER TO gazelle;

--
-- Name: cmn_ip_address_id_seq; Type: SEQUENCE; Schema: public; Owner: gazelle
--

CREATE SEQUENCE cmn_ip_address_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE cmn_ip_address_id_seq OWNER TO gazelle;

--
-- Name: cmn_message_instance; Type: TABLE; Schema: public; Owner: gazelle
--

CREATE TABLE cmn_message_instance (
    id integer NOT NULL,
    content bytea,
    issuer character varying(255),
    type character varying(255),
    validation_detailed_result bytea,
    validation_status character varying(255),
    issuing_actor integer,
    issuer_ip_address character varying(255)
);


ALTER TABLE cmn_message_instance OWNER TO gazelle;

--
-- Name: cmn_message_instance_id_seq; Type: SEQUENCE; Schema: public; Owner: gazelle
--

CREATE SEQUENCE cmn_message_instance_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE cmn_message_instance_id_seq OWNER TO gazelle;

--
-- Name: cmn_message_instance_metadata; Type: TABLE; Schema: public; Owner: gazelle
--

CREATE TABLE cmn_message_instance_metadata (
    id integer NOT NULL,
    label character varying(255),
    value character varying(255),
    message_instance_id integer
);


ALTER TABLE cmn_message_instance_metadata OWNER TO gazelle;

--
-- Name: cmn_message_instance_metadata_id_seq; Type: SEQUENCE; Schema: public; Owner: gazelle
--

CREATE SEQUENCE cmn_message_instance_metadata_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE cmn_message_instance_metadata_id_seq OWNER TO gazelle;

--
-- Name: cmn_receiver_console; Type: TABLE; Schema: public; Owner: gazelle
--

CREATE TABLE cmn_receiver_console (
    id integer NOT NULL,
    code character varying(255),
    comment text,
    message_identifier character varying(255),
    message_type character varying(255),
    sut character varying(255),
    "timestamp" timestamp without time zone,
    domain_id integer,
    simulated_actor_id integer,
    transaction_id integer
);


ALTER TABLE cmn_receiver_console OWNER TO gazelle;

--
-- Name: cmn_receiver_console_id_seq; Type: SEQUENCE; Schema: public; Owner: gazelle
--

CREATE SEQUENCE cmn_receiver_console_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE cmn_receiver_console_id_seq OWNER TO gazelle;

--
-- Name: cmn_transaction_instance; Type: TABLE; Schema: public; Owner: gazelle
--

CREATE TABLE cmn_transaction_instance (
    id integer NOT NULL,
    standard integer,
    "timestamp" timestamp without time zone,
    is_visible boolean,
    domain_id integer,
    request_id integer,
    response_id integer,
    simulated_actor_id integer,
    transaction_id integer,
    company_keyword character varying(255)
);


ALTER TABLE cmn_transaction_instance OWNER TO gazelle;

--
-- Name: cmn_transaction_instance_id_seq; Type: SEQUENCE; Schema: public; Owner: gazelle
--

CREATE SEQUENCE cmn_transaction_instance_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE cmn_transaction_instance_id_seq OWNER TO gazelle;

--
-- Name: cmn_validator_usage; Type: TABLE; Schema: public; Owner: gazelle
--

CREATE TABLE cmn_validator_usage (
    id integer NOT NULL,
    caller character varying(255),
    date date,
    status character varying(255),
    type character varying(255)
);


ALTER TABLE cmn_validator_usage OWNER TO gazelle;

--
-- Name: cmn_validator_usage_id_seq; Type: SEQUENCE; Schema: public; Owner: gazelle
--

CREATE SEQUENCE cmn_validator_usage_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE cmn_validator_usage_id_seq OWNER TO gazelle;

--
-- Name: cmn_value_set; Type: TABLE; Schema: public; Owner: gazelle
--

CREATE TABLE cmn_value_set (
    id integer NOT NULL,
    accessible boolean,
    last_check timestamp without time zone,
    usage character varying(255),
    value_set_keyword character varying(255),
    value_set_name character varying(255),
    value_set_oid character varying(255)
);


ALTER TABLE cmn_value_set OWNER TO gazelle;

--
-- Name: cmn_value_set_id_seq; Type: SEQUENCE; Schema: public; Owner: gazelle
--

CREATE SEQUENCE cmn_value_set_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE cmn_value_set_id_seq OWNER TO gazelle;

--
-- Name: gs_contextual_information; Type: TABLE; Schema: public; Owner: gazelle
--

CREATE TABLE gs_contextual_information (
    id integer NOT NULL,
    label character varying(255) NOT NULL,
    value character varying(255),
    path integer NOT NULL
);


ALTER TABLE gs_contextual_information OWNER TO gazelle;

--
-- Name: gs_contextual_information_id_seq; Type: SEQUENCE; Schema: public; Owner: gazelle
--

CREATE SEQUENCE gs_contextual_information_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE gs_contextual_information_id_seq OWNER TO gazelle;

--
-- Name: gs_contextual_information_instance; Type: TABLE; Schema: public; Owner: gazelle
--

CREATE TABLE gs_contextual_information_instance (
    id integer NOT NULL,
    form character varying(255),
    value character varying(255),
    contextual_information_id integer NOT NULL,
    test_steps_instance_id integer NOT NULL
);


ALTER TABLE gs_contextual_information_instance OWNER TO gazelle;

--
-- Name: gs_contextual_information_instance_id_seq; Type: SEQUENCE; Schema: public; Owner: gazelle
--

CREATE SEQUENCE gs_contextual_information_instance_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE gs_contextual_information_instance_id_seq OWNER TO gazelle;

--
-- Name: gs_message; Type: TABLE; Schema: public; Owner: gazelle
--

CREATE TABLE gs_message (
    id integer NOT NULL,
    message_content bytea,
    message_type_id character varying(255),
    time_stamp timestamp without time zone,
    test_instance_participants_receiver_id integer,
    test_instance_participants_sender_id integer,
    transaction_id integer
);


ALTER TABLE gs_message OWNER TO gazelle;

--
-- Name: gs_message_id_seq; Type: SEQUENCE; Schema: public; Owner: gazelle
--

CREATE SEQUENCE gs_message_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE gs_message_id_seq OWNER TO gazelle;

--
-- Name: gs_system; Type: TABLE; Schema: public; Owner: gazelle
--

CREATE TABLE gs_system (
    id integer NOT NULL,
    institution_keyword character varying(255),
    keyword character varying(255),
    system_owner character varying(255)
);


ALTER TABLE gs_system OWNER TO gazelle;

--
-- Name: gs_system_id_seq; Type: SEQUENCE; Schema: public; Owner: gazelle
--

CREATE SEQUENCE gs_system_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE gs_system_id_seq OWNER TO gazelle;

--
-- Name: gs_test_instance; Type: TABLE; Schema: public; Owner: gazelle
--

CREATE TABLE gs_test_instance (
    id integer NOT NULL,
    server_test_instance_id character varying(255) NOT NULL,
    test_instance_status_id integer
);


ALTER TABLE gs_test_instance OWNER TO gazelle;

--
-- Name: gs_test_instance_id_seq; Type: SEQUENCE; Schema: public; Owner: gazelle
--

CREATE SEQUENCE gs_test_instance_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE gs_test_instance_id_seq OWNER TO gazelle;

--
-- Name: gs_test_instance_oid; Type: TABLE; Schema: public; Owner: gazelle
--

CREATE TABLE gs_test_instance_oid (
    oid_configuration_id integer NOT NULL,
    test_instance_id integer NOT NULL
);


ALTER TABLE gs_test_instance_oid OWNER TO gazelle;

--
-- Name: gs_test_instance_participants; Type: TABLE; Schema: public; Owner: gazelle
--

CREATE TABLE gs_test_instance_participants (
    id integer NOT NULL,
    server_aipo_id character varying(255) NOT NULL,
    server_test_instance_participants_id character varying(255) NOT NULL,
    aipo_id integer,
    system_id integer
);


ALTER TABLE gs_test_instance_participants OWNER TO gazelle;

--
-- Name: gs_test_instance_participants_id_seq; Type: SEQUENCE; Schema: public; Owner: gazelle
--

CREATE SEQUENCE gs_test_instance_participants_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE gs_test_instance_participants_id_seq OWNER TO gazelle;

--
-- Name: gs_test_instance_status; Type: TABLE; Schema: public; Owner: gazelle
--

CREATE TABLE gs_test_instance_status (
    id integer NOT NULL,
    description character varying(255),
    keyword character varying(255),
    label_to_display character varying(255)
);


ALTER TABLE gs_test_instance_status OWNER TO gazelle;

--
-- Name: gs_test_instance_status_id_seq; Type: SEQUENCE; Schema: public; Owner: gazelle
--

CREATE SEQUENCE gs_test_instance_status_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE gs_test_instance_status_id_seq OWNER TO gazelle;

--
-- Name: gs_test_instance_test_instance_participants; Type: TABLE; Schema: public; Owner: gazelle
--

CREATE TABLE gs_test_instance_test_instance_participants (
    test_instance_participants_id integer NOT NULL,
    test_instance_id integer NOT NULL
);


ALTER TABLE gs_test_instance_test_instance_participants OWNER TO gazelle;

--
-- Name: gs_test_steps_instance; Type: TABLE; Schema: public; Owner: gazelle
--

CREATE TABLE gs_test_steps_instance (
    id integer NOT NULL,
    dicom_scp_config_id integer,
    dicom_scu_config_id integer,
    hl7v2_initiator_config_id integer,
    hl7v2_responder_config_id integer,
    hl7v3_initiator_config_id integer,
    hl7v3_responder_config_id integer,
    syslog_config_id integer,
    testinstance_id integer,
    web_service_config_id integer
);


ALTER TABLE gs_test_steps_instance OWNER TO gazelle;

--
-- Name: gs_test_steps_instance_id_seq; Type: SEQUENCE; Schema: public; Owner: gazelle
--

CREATE SEQUENCE gs_test_steps_instance_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE gs_test_steps_instance_id_seq OWNER TO gazelle;

--
-- Name: hpd_directory_error_message; Type: TABLE; Schema: public; Owner: gazelle
--

CREATE TABLE hpd_directory_error_message (
    id integer NOT NULL,
    client character varying(255),
    code integer,
    code_meaning character varying(255),
    message bytea,
    messagetype character varying(255),
    request_id character varying(255),
    "timestamp" timestamp without time zone,
    transaction_instance_id integer,
    transaction_keyword character varying(255)
);


ALTER TABLE hpd_directory_error_message OWNER TO gazelle;

--
-- Name: hpd_directory_error_message_id_seq; Type: SEQUENCE; Schema: public; Owner: gazelle
--

CREATE SEQUENCE hpd_directory_error_message_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE hpd_directory_error_message_id_seq OWNER TO gazelle;

--
-- Name: hpd_ldap_attribute_types; Type: TABLE; Schema: public; Owner: gazelle
--

CREATE TABLE hpd_ldap_attribute_types (
    id integer NOT NULL,
    description character varying(255),
    name character varying(255),
    oid character varying(255),
    equality integer,
    length integer,
    ordering integer,
    singlevalue boolean,
    substr integer,
    syntax character varying(255)
);


ALTER TABLE hpd_ldap_attribute_types OWNER TO gazelle;

--
-- Name: hpd_ldap_attribute_types_id_seq; Type: SEQUENCE; Schema: public; Owner: gazelle
--

CREATE SEQUENCE hpd_ldap_attribute_types_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE hpd_ldap_attribute_types_id_seq OWNER TO gazelle;

--
-- Name: hpd_ldap_object_classes; Type: TABLE; Schema: public; Owner: gazelle
--

CREATE TABLE hpd_ldap_object_classes (
    id integer NOT NULL,
    description character varying(255),
    name character varying(255),
    oid character varying(255),
    sup_object_class character varying(255),
    type_object_class integer
);


ALTER TABLE hpd_ldap_object_classes OWNER TO gazelle;

--
-- Name: hpd_ldap_object_classes_id_seq; Type: SEQUENCE; Schema: public; Owner: gazelle
--

CREATE SEQUENCE hpd_ldap_object_classes_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE hpd_ldap_object_classes_id_seq OWNER TO gazelle;

--
-- Name: hpd_ldap_object_classes_may; Type: TABLE; Schema: public; Owner: gazelle
--

CREATE TABLE hpd_ldap_object_classes_may (
    object_classes_id integer NOT NULL,
    may character varying(255)
);


ALTER TABLE hpd_ldap_object_classes_may OWNER TO gazelle;

--
-- Name: hpd_ldap_object_classes_must; Type: TABLE; Schema: public; Owner: gazelle
--

CREATE TABLE hpd_ldap_object_classes_must (
    object_classes_id integer NOT NULL,
    must character varying(255)
);


ALTER TABLE hpd_ldap_object_classes_must OWNER TO gazelle;

--
-- Name: ldap_node; Type: TABLE; Schema: public; Owner: gazelle
--

CREATE TABLE ldap_node (
    id integer NOT NULL,
    description text,
    name character varying(255) NOT NULL
);


ALTER TABLE ldap_node OWNER TO gazelle;

--
-- Name: ldap_node_id_seq; Type: SEQUENCE; Schema: public; Owner: gazelle
--

CREATE SEQUENCE ldap_node_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE ldap_node_id_seq OWNER TO gazelle;

--
-- Name: ldap_node_object_class; Type: TABLE; Schema: public; Owner: gazelle
--

CREATE TABLE ldap_node_object_class (
    ldap_node_id integer NOT NULL,
    object_class_id integer NOT NULL
);


ALTER TABLE ldap_node_object_class OWNER TO gazelle;

--
-- Name: ldap_partition; Type: TABLE; Schema: public; Owner: gazelle
--

CREATE TABLE ldap_partition (
    id integer NOT NULL,
    base_dn character varying(255),
    creation_date date,
    creator character varying(255),
    description text,
    dn_modify_regex character varying(255),
    dn_search_regex character varying(255),
    is_default boolean,
    name character varying(255) NOT NULL,
    server_ip character varying(255),
    server_port integer
);


ALTER TABLE ldap_partition OWNER TO gazelle;

--
-- Name: ldap_partition_id_seq; Type: SEQUENCE; Schema: public; Owner: gazelle
--

CREATE SEQUENCE ldap_partition_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE ldap_partition_id_seq OWNER TO gazelle;

--
-- Name: ldap_partition_node; Type: TABLE; Schema: public; Owner: gazelle
--

CREATE TABLE ldap_partition_node (
    ldap_partition_id integer NOT NULL,
    ldap_node_id integer NOT NULL
);


ALTER TABLE ldap_partition_node OWNER TO gazelle;

--
-- Name: mbv_assertion; Type: TABLE; Schema: public; Owner: gazelle
--

CREATE TABLE mbv_assertion (
    id integer NOT NULL,
    assertionid character varying(255),
    idscheme character varying(255),
    constraint_id integer
);


ALTER TABLE mbv_assertion OWNER TO gazelle;

--
-- Name: mbv_assertion_id_seq; Type: SEQUENCE; Schema: public; Owner: gazelle
--

CREATE SEQUENCE mbv_assertion_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE mbv_assertion_id_seq OWNER TO gazelle;

--
-- Name: mbv_class_type; Type: TABLE; Schema: public; Owner: gazelle
--

CREATE TABLE mbv_class_type (
    dtype character varying(31) NOT NULL,
    id integer NOT NULL,
    name character varying(255),
    parent_identifier character varying(255),
    xml_name character varying(255),
    constraintadvanced text,
    path character varying(255),
    templateid character varying(255),
    documentation_spec_id integer,
    package_id integer
);


ALTER TABLE mbv_class_type OWNER TO gazelle;

--
-- Name: mbv_class_type_id_seq; Type: SEQUENCE; Schema: public; Owner: gazelle
--

CREATE SEQUENCE mbv_class_type_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE mbv_class_type_id_seq OWNER TO gazelle;

--
-- Name: mbv_constraint; Type: TABLE; Schema: public; Owner: gazelle
--

CREATE TABLE mbv_constraint (
    id integer NOT NULL,
    author character varying(255),
    datecreation character varying(255),
    description text,
    history text,
    kind character varying(255),
    lastchange character varying(255),
    name character varying(255),
    ocl text,
    svsref bytea,
    type character varying(255),
    classtype_id integer
);


ALTER TABLE mbv_constraint OWNER TO gazelle;

--
-- Name: mbv_constraint_id_seq; Type: SEQUENCE; Schema: public; Owner: gazelle
--

CREATE SEQUENCE mbv_constraint_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE mbv_constraint_id_seq OWNER TO gazelle;

--
-- Name: mbv_documentation_spec; Type: TABLE; Schema: public; Owner: gazelle
--

CREATE TABLE mbv_documentation_spec (
    id integer NOT NULL,
    description text,
    document character varying(255),
    name character varying(255),
    paragraph character varying(255)
);


ALTER TABLE mbv_documentation_spec OWNER TO gazelle;

--
-- Name: mbv_documentation_spec_id_seq; Type: SEQUENCE; Schema: public; Owner: gazelle
--

CREATE SEQUENCE mbv_documentation_spec_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE mbv_documentation_spec_id_seq OWNER TO gazelle;

--
-- Name: mbv_package; Type: TABLE; Schema: public; Owner: gazelle
--

CREATE TABLE mbv_package (
    id integer NOT NULL,
    description character varying(255),
    name character varying(255),
    package_name character varying(255),
    namespace character varying(255)
);


ALTER TABLE mbv_package OWNER TO gazelle;

--
-- Name: mbv_package_id_seq; Type: SEQUENCE; Schema: public; Owner: gazelle
--

CREATE SEQUENCE mbv_package_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE mbv_package_id_seq OWNER TO gazelle;

--
-- Name: mbv_standards; Type: TABLE; Schema: public; Owner: gazelle
--

CREATE TABLE mbv_standards (
    package_id integer NOT NULL,
    standards character varying(255)
);


ALTER TABLE mbv_standards OWNER TO gazelle;

--
-- Name: sys_conf_type_usages; Type: TABLE; Schema: public; Owner: gazelle
--

CREATE TABLE sys_conf_type_usages (
    system_configuration_id integer NOT NULL,
    listusages_id integer NOT NULL
);


ALTER TABLE sys_conf_type_usages OWNER TO gazelle;

--
-- Name: system_configuration; Type: TABLE; Schema: public; Owner: gazelle
--

CREATE TABLE system_configuration (
    dtype character varying(31) NOT NULL,
    id integer NOT NULL,
    is_available boolean,
    is_public boolean,
    name character varying(255),
    owner character varying(255),
    system_name character varying(255),
    url character varying(255),
    base_dn character varying(255),
    owner_company character varying(255)
);


ALTER TABLE system_configuration OWNER TO gazelle;

--
-- Name: system_configuration_id_seq; Type: SEQUENCE; Schema: public; Owner: gazelle
--

CREATE SEQUENCE system_configuration_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE system_configuration_id_seq OWNER TO gazelle;

--
-- Name: tf_actor; Type: TABLE; Schema: public; Owner: gazelle
--

CREATE TABLE tf_actor (
    id integer NOT NULL,
    description character varying(2048),
    keyword character varying(128) NOT NULL,
    name character varying(128),
    can_act_as_responder boolean
);


ALTER TABLE tf_actor OWNER TO gazelle;

--
-- Name: tf_actor_id_seq; Type: SEQUENCE; Schema: public; Owner: gazelle
--

CREATE SEQUENCE tf_actor_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE tf_actor_id_seq OWNER TO gazelle;

--
-- Name: tf_actor_integration_profile; Type: TABLE; Schema: public; Owner: gazelle
--

CREATE TABLE tf_actor_integration_profile (
    id integer NOT NULL,
    actor_id integer,
    integration_profile_id integer
);


ALTER TABLE tf_actor_integration_profile OWNER TO gazelle;

--
-- Name: tf_actor_integration_profile_id_seq; Type: SEQUENCE; Schema: public; Owner: gazelle
--

CREATE SEQUENCE tf_actor_integration_profile_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE tf_actor_integration_profile_id_seq OWNER TO gazelle;

--
-- Name: tf_actor_integration_profile_option; Type: TABLE; Schema: public; Owner: gazelle
--

CREATE TABLE tf_actor_integration_profile_option (
    id integer NOT NULL,
    actor_integration_profile_id integer NOT NULL,
    integration_profile_option_id integer
);


ALTER TABLE tf_actor_integration_profile_option OWNER TO gazelle;

--
-- Name: tf_actor_integration_profile_option_id_seq; Type: SEQUENCE; Schema: public; Owner: gazelle
--

CREATE SEQUENCE tf_actor_integration_profile_option_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE tf_actor_integration_profile_option_id_seq OWNER TO gazelle;

--
-- Name: tf_domain; Type: TABLE; Schema: public; Owner: gazelle
--

CREATE TABLE tf_domain (
    id integer NOT NULL,
    description character varying(2048),
    keyword character varying(128) NOT NULL,
    name character varying(128)
);


ALTER TABLE tf_domain OWNER TO gazelle;

--
-- Name: tf_domain_id_seq; Type: SEQUENCE; Schema: public; Owner: gazelle
--

CREATE SEQUENCE tf_domain_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE tf_domain_id_seq OWNER TO gazelle;

--
-- Name: tf_domain_integration_profiles; Type: TABLE; Schema: public; Owner: gazelle
--

CREATE TABLE tf_domain_integration_profiles (
    domain_id integer NOT NULL,
    integration_profile_id integer NOT NULL
);


ALTER TABLE tf_domain_integration_profiles OWNER TO gazelle;

--
-- Name: tf_integration_profile; Type: TABLE; Schema: public; Owner: gazelle
--

CREATE TABLE tf_integration_profile (
    id integer NOT NULL,
    description character varying(2048),
    keyword character varying(128) NOT NULL,
    name character varying(128)
);


ALTER TABLE tf_integration_profile OWNER TO gazelle;

--
-- Name: tf_integration_profile_id_seq; Type: SEQUENCE; Schema: public; Owner: gazelle
--

CREATE SEQUENCE tf_integration_profile_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE tf_integration_profile_id_seq OWNER TO gazelle;

--
-- Name: tf_integration_profile_option; Type: TABLE; Schema: public; Owner: gazelle
--

CREATE TABLE tf_integration_profile_option (
    id integer NOT NULL,
    description character varying(2048),
    keyword character varying(128) NOT NULL,
    name character varying(128)
);


ALTER TABLE tf_integration_profile_option OWNER TO gazelle;

--
-- Name: tf_integration_profile_option_id_seq; Type: SEQUENCE; Schema: public; Owner: gazelle
--

CREATE SEQUENCE tf_integration_profile_option_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE tf_integration_profile_option_id_seq OWNER TO gazelle;

--
-- Name: tf_transaction; Type: TABLE; Schema: public; Owner: gazelle
--

CREATE TABLE tf_transaction (
    id integer NOT NULL,
    description character varying(2048),
    keyword character varying(128) NOT NULL,
    name character varying(128)
);


ALTER TABLE tf_transaction OWNER TO gazelle;

--
-- Name: tf_transaction_id_seq; Type: SEQUENCE; Schema: public; Owner: gazelle
--

CREATE SEQUENCE tf_transaction_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE tf_transaction_id_seq OWNER TO gazelle;

--
-- Name: tm_oid; Type: TABLE; Schema: public; Owner: gazelle
--

CREATE TABLE tm_oid (
    id integer NOT NULL,
    label character varying(255) NOT NULL,
    oid character varying(255) NOT NULL,
    system_id integer
);


ALTER TABLE tm_oid OWNER TO gazelle;

--
-- Name: tm_oid_id_seq; Type: SEQUENCE; Schema: public; Owner: gazelle
--

CREATE SEQUENCE tm_oid_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE tm_oid_id_seq OWNER TO gazelle;

--
-- Name: tm_path; Type: TABLE; Schema: public; Owner: gazelle
--

CREATE TABLE tm_path (
    id integer NOT NULL,
    description character varying(255) NOT NULL,
    keyword character varying(255) NOT NULL,
    type character varying(255)
);


ALTER TABLE tm_path OWNER TO gazelle;

--
-- Name: tm_path_id_seq; Type: SEQUENCE; Schema: public; Owner: gazelle
--

CREATE SEQUENCE tm_path_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE tm_path_id_seq OWNER TO gazelle;

--
-- Name: usage_id_seq; Type: SEQUENCE; Schema: public; Owner: gazelle
--

CREATE SEQUENCE usage_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE usage_id_seq OWNER TO gazelle;

--
-- Name: usage_metadata; Type: TABLE; Schema: public; Owner: gazelle
--

CREATE TABLE usage_metadata (
    id integer NOT NULL,
    action character varying(255),
    keyword character varying(255),
    affinity_id integer,
    transaction_id integer
);


ALTER TABLE usage_metadata OWNER TO gazelle;

--
-- Name: affinity_domain affinity_domain_pkey; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY affinity_domain
    ADD CONSTRAINT affinity_domain_pkey PRIMARY KEY (id);


--
-- Name: app_configuration app_configuration_pkey; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY app_configuration
    ADD CONSTRAINT app_configuration_pkey PRIMARY KEY (id);


--
-- Name: cfg_configuration cfg_configuration_pkey; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY cfg_configuration
    ADD CONSTRAINT cfg_configuration_pkey PRIMARY KEY (id);


--
-- Name: cfg_dicom_scp_configuration cfg_dicom_scp_configuration_pkey; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY cfg_dicom_scp_configuration
    ADD CONSTRAINT cfg_dicom_scp_configuration_pkey PRIMARY KEY (id);


--
-- Name: cfg_dicom_scu_configuration cfg_dicom_scu_configuration_pkey; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY cfg_dicom_scu_configuration
    ADD CONSTRAINT cfg_dicom_scu_configuration_pkey PRIMARY KEY (id);


--
-- Name: cfg_hl7_initiator_configuration cfg_hl7_initiator_configuration_pkey; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY cfg_hl7_initiator_configuration
    ADD CONSTRAINT cfg_hl7_initiator_configuration_pkey PRIMARY KEY (id);


--
-- Name: cfg_hl7_responder_configuration cfg_hl7_responder_configuration_pkey; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY cfg_hl7_responder_configuration
    ADD CONSTRAINT cfg_hl7_responder_configuration_pkey PRIMARY KEY (id);


--
-- Name: cfg_hl7_v3_initiator_configuration cfg_hl7_v3_initiator_configuration_pkey; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY cfg_hl7_v3_initiator_configuration
    ADD CONSTRAINT cfg_hl7_v3_initiator_configuration_pkey PRIMARY KEY (id);


--
-- Name: cfg_hl7_v3_responder_configuration cfg_hl7_v3_responder_configuration_pkey; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY cfg_hl7_v3_responder_configuration
    ADD CONSTRAINT cfg_hl7_v3_responder_configuration_pkey PRIMARY KEY (id);


--
-- Name: cfg_host cfg_host_pkey; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY cfg_host
    ADD CONSTRAINT cfg_host_pkey PRIMARY KEY (id);


--
-- Name: cfg_sop_class cfg_sop_class_pkey; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY cfg_sop_class
    ADD CONSTRAINT cfg_sop_class_pkey PRIMARY KEY (id);


--
-- Name: cfg_syslog_configuration cfg_syslog_configuration_pkey; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY cfg_syslog_configuration
    ADD CONSTRAINT cfg_syslog_configuration_pkey PRIMARY KEY (id);


--
-- Name: cfg_web_service_configuration cfg_web_service_configuration_pkey; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY cfg_web_service_configuration
    ADD CONSTRAINT cfg_web_service_configuration_pkey PRIMARY KEY (id);


--
-- Name: cmn_company_details cmn_company_details_pkey; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY cmn_company_details
    ADD CONSTRAINT cmn_company_details_pkey PRIMARY KEY (id);


--
-- Name: cmn_home cmn_home_pkey; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY cmn_home
    ADD CONSTRAINT cmn_home_pkey PRIMARY KEY (id);


--
-- Name: cmn_ip_address cmn_ip_address_pkey; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY cmn_ip_address
    ADD CONSTRAINT cmn_ip_address_pkey PRIMARY KEY (id);


--
-- Name: cmn_message_instance_metadata cmn_message_instance_metadata_pkey; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY cmn_message_instance_metadata
    ADD CONSTRAINT cmn_message_instance_metadata_pkey PRIMARY KEY (id);


--
-- Name: cmn_message_instance cmn_message_instance_pkey; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY cmn_message_instance
    ADD CONSTRAINT cmn_message_instance_pkey PRIMARY KEY (id);


--
-- Name: cmn_receiver_console cmn_receiver_console_pkey; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY cmn_receiver_console
    ADD CONSTRAINT cmn_receiver_console_pkey PRIMARY KEY (id);


--
-- Name: cmn_transaction_instance cmn_transaction_instance_pkey; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY cmn_transaction_instance
    ADD CONSTRAINT cmn_transaction_instance_pkey PRIMARY KEY (id);


--
-- Name: cmn_validator_usage cmn_validator_usage_pkey; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY cmn_validator_usage
    ADD CONSTRAINT cmn_validator_usage_pkey PRIMARY KEY (id);


--
-- Name: cmn_value_set cmn_value_set_pkey; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY cmn_value_set
    ADD CONSTRAINT cmn_value_set_pkey PRIMARY KEY (id);


--
-- Name: gs_contextual_information_instance gs_contextual_information_instance_pkey; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY gs_contextual_information_instance
    ADD CONSTRAINT gs_contextual_information_instance_pkey PRIMARY KEY (id);


--
-- Name: gs_contextual_information gs_contextual_information_pkey; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY gs_contextual_information
    ADD CONSTRAINT gs_contextual_information_pkey PRIMARY KEY (id);


--
-- Name: gs_message gs_message_pkey; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY gs_message
    ADD CONSTRAINT gs_message_pkey PRIMARY KEY (id);


--
-- Name: gs_system gs_system_pkey; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY gs_system
    ADD CONSTRAINT gs_system_pkey PRIMARY KEY (id);


--
-- Name: gs_test_instance_participants gs_test_instance_participants_pkey; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY gs_test_instance_participants
    ADD CONSTRAINT gs_test_instance_participants_pkey PRIMARY KEY (id);


--
-- Name: gs_test_instance gs_test_instance_pkey; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY gs_test_instance
    ADD CONSTRAINT gs_test_instance_pkey PRIMARY KEY (id);


--
-- Name: gs_test_instance_status gs_test_instance_status_pkey; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY gs_test_instance_status
    ADD CONSTRAINT gs_test_instance_status_pkey PRIMARY KEY (id);


--
-- Name: gs_test_steps_instance gs_test_steps_instance_pkey; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY gs_test_steps_instance
    ADD CONSTRAINT gs_test_steps_instance_pkey PRIMARY KEY (id);


--
-- Name: hpd_directory_error_message hpd_directory_error_message_pkey; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY hpd_directory_error_message
    ADD CONSTRAINT hpd_directory_error_message_pkey PRIMARY KEY (id);


--
-- Name: hpd_ldap_attribute_types hpd_ldap_attribute_types_pkey; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY hpd_ldap_attribute_types
    ADD CONSTRAINT hpd_ldap_attribute_types_pkey PRIMARY KEY (id);


--
-- Name: hpd_ldap_object_classes hpd_ldap_object_classes_pkey; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY hpd_ldap_object_classes
    ADD CONSTRAINT hpd_ldap_object_classes_pkey PRIMARY KEY (id);


--
-- Name: ldap_node ldap_node_pkey; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY ldap_node
    ADD CONSTRAINT ldap_node_pkey PRIMARY KEY (id);


--
-- Name: ldap_partition ldap_partition_pkey; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY ldap_partition
    ADD CONSTRAINT ldap_partition_pkey PRIMARY KEY (id);


--
-- Name: mbv_assertion mbv_assertion_pkey; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY mbv_assertion
    ADD CONSTRAINT mbv_assertion_pkey PRIMARY KEY (id);


--
-- Name: mbv_class_type mbv_class_type_pkey; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY mbv_class_type
    ADD CONSTRAINT mbv_class_type_pkey PRIMARY KEY (id);


--
-- Name: mbv_constraint mbv_constraint_pkey; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY mbv_constraint
    ADD CONSTRAINT mbv_constraint_pkey PRIMARY KEY (id);


--
-- Name: mbv_documentation_spec mbv_documentation_spec_pkey; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY mbv_documentation_spec
    ADD CONSTRAINT mbv_documentation_spec_pkey PRIMARY KEY (id);


--
-- Name: mbv_package mbv_package_pkey; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY mbv_package
    ADD CONSTRAINT mbv_package_pkey PRIMARY KEY (id);


--
-- Name: system_configuration system_configuration_pkey; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY system_configuration
    ADD CONSTRAINT system_configuration_pkey PRIMARY KEY (id);


--
-- Name: tf_actor_integration_profile_option tf_actor_integration_profile_option_pkey; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY tf_actor_integration_profile_option
    ADD CONSTRAINT tf_actor_integration_profile_option_pkey PRIMARY KEY (id);


--
-- Name: tf_actor_integration_profile tf_actor_integration_profile_pkey; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY tf_actor_integration_profile
    ADD CONSTRAINT tf_actor_integration_profile_pkey PRIMARY KEY (id);


--
-- Name: tf_actor tf_actor_pkey; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY tf_actor
    ADD CONSTRAINT tf_actor_pkey PRIMARY KEY (id);


--
-- Name: tf_domain tf_domain_pkey; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY tf_domain
    ADD CONSTRAINT tf_domain_pkey PRIMARY KEY (id);


--
-- Name: tf_integration_profile_option tf_integration_profile_option_pkey; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY tf_integration_profile_option
    ADD CONSTRAINT tf_integration_profile_option_pkey PRIMARY KEY (id);


--
-- Name: tf_integration_profile tf_integration_profile_pkey; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY tf_integration_profile
    ADD CONSTRAINT tf_integration_profile_pkey PRIMARY KEY (id);


--
-- Name: tf_transaction tf_transaction_pkey; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY tf_transaction
    ADD CONSTRAINT tf_transaction_pkey PRIMARY KEY (id);


--
-- Name: tm_oid tm_oid_pkey; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY tm_oid
    ADD CONSTRAINT tm_oid_pkey PRIMARY KEY (id);


--
-- Name: tm_path tm_path_pkey; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY tm_path
    ADD CONSTRAINT tm_path_pkey PRIMARY KEY (id);


--
-- Name: app_configuration uk_20rnkdjn5jvlvmsb5f7io4b1o; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY app_configuration
    ADD CONSTRAINT uk_20rnkdjn5jvlvmsb5f7io4b1o UNIQUE (variable);


--
-- Name: system_configuration uk_2iwxlu65fuwpbhmeg96ebawhw; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY system_configuration
    ADD CONSTRAINT uk_2iwxlu65fuwpbhmeg96ebawhw UNIQUE (name);


--
-- Name: tf_domain uk_436tct1jl8811q2xgd8bjth9q; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY tf_domain
    ADD CONSTRAINT uk_436tct1jl8811q2xgd8bjth9q UNIQUE (keyword);


--
-- Name: tf_transaction uk_6nt35dm69gbrrj0aegkkqalr2; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY tf_transaction
    ADD CONSTRAINT uk_6nt35dm69gbrrj0aegkkqalr2 UNIQUE (keyword);


--
-- Name: ldap_partition uk_6pqvvkuv4gvb7tf4gni8qbbfj; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY ldap_partition
    ADD CONSTRAINT uk_6pqvvkuv4gvb7tf4gni8qbbfj UNIQUE (name);


--
-- Name: gs_test_instance_participants uk_7rrch4mgjwqsbug2p4fgu9nwm; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY gs_test_instance_participants
    ADD CONSTRAINT uk_7rrch4mgjwqsbug2p4fgu9nwm UNIQUE (server_aipo_id, server_test_instance_participants_id);


--
-- Name: tf_actor_integration_profile_option uk_8b4tb6bcp3eh7mtsucokgh7fx; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY tf_actor_integration_profile_option
    ADD CONSTRAINT uk_8b4tb6bcp3eh7mtsucokgh7fx UNIQUE (actor_integration_profile_id, integration_profile_option_id);


--
-- Name: tm_path uk_8ufv5mgjsoifbtbq28b64r87s; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY tm_path
    ADD CONSTRAINT uk_8ufv5mgjsoifbtbq28b64r87s UNIQUE (keyword);


--
-- Name: tf_actor_integration_profile uk_b6ejd87o8v27xinqw27nn1hss; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY tf_actor_integration_profile
    ADD CONSTRAINT uk_b6ejd87o8v27xinqw27nn1hss UNIQUE (actor_id, integration_profile_id);


--
-- Name: ldap_node_object_class uk_fld90go3tjdwg220xbkv5tw8a; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY ldap_node_object_class
    ADD CONSTRAINT uk_fld90go3tjdwg220xbkv5tw8a UNIQUE (ldap_node_id, object_class_id);


--
-- Name: tf_actor uk_fpb6vpa9bnw6cjsb1pucuuivw; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY tf_actor
    ADD CONSTRAINT uk_fpb6vpa9bnw6cjsb1pucuuivw UNIQUE (keyword);


--
-- Name: hpd_ldap_object_classes uk_gcuvfhoc1fbgpgmyetttimyom; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY hpd_ldap_object_classes
    ADD CONSTRAINT uk_gcuvfhoc1fbgpgmyetttimyom UNIQUE (name);


--
-- Name: ldap_node uk_gi06ff257hr7273jayk3vdtum; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY ldap_node
    ADD CONSTRAINT uk_gi06ff257hr7273jayk3vdtum UNIQUE (name);


--
-- Name: affinity_domain uk_ia6h2gkv0i2omgnnsnwyqwnqs; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY affinity_domain
    ADD CONSTRAINT uk_ia6h2gkv0i2omgnnsnwyqwnqs UNIQUE (keyword);


--
-- Name: tf_domain_integration_profiles uk_iw9qgddyj9xsshmek3gr6u588; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY tf_domain_integration_profiles
    ADD CONSTRAINT uk_iw9qgddyj9xsshmek3gr6u588 UNIQUE (integration_profile_id, domain_id);


--
-- Name: gs_test_instance_participants uk_l1ffr6swiqyeuvjixaptkpsk9; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY gs_test_instance_participants
    ADD CONSTRAINT uk_l1ffr6swiqyeuvjixaptkpsk9 UNIQUE (server_test_instance_participants_id);


--
-- Name: cmn_home uk_mv2quil5gwcd8bxyc76v4vyy1; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY cmn_home
    ADD CONSTRAINT uk_mv2quil5gwcd8bxyc76v4vyy1 UNIQUE (iso3_language);


--
-- Name: tf_integration_profile uk_ny4glgtyovt2w045nwct19arx; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY tf_integration_profile
    ADD CONSTRAINT uk_ny4glgtyovt2w045nwct19arx UNIQUE (keyword);


--
-- Name: ldap_partition_node uk_q7e5s52idr33lnoji222ww8hq; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY ldap_partition_node
    ADD CONSTRAINT uk_q7e5s52idr33lnoji222ww8hq UNIQUE (ldap_partition_id, ldap_node_id);


--
-- Name: gs_test_instance uk_qprd2o4wqtcw4eify2dmrvprn; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY gs_test_instance
    ADD CONSTRAINT uk_qprd2o4wqtcw4eify2dmrvprn UNIQUE (server_test_instance_id);


--
-- Name: affinity_domain_transactions uk_r4g9ud7n9b2gx7tia9engpipe; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY affinity_domain_transactions
    ADD CONSTRAINT uk_r4g9ud7n9b2gx7tia9engpipe UNIQUE (affinity_domain_id, transaction_id);


--
-- Name: cmn_company_details uk_r6osh086b3nqbuxpswcp1hcc2; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY cmn_company_details
    ADD CONSTRAINT uk_r6osh086b3nqbuxpswcp1hcc2 UNIQUE (company_keyword);


--
-- Name: tf_integration_profile_option uk_thqc1vykug4qhn8jdij04d1bh; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY tf_integration_profile_option
    ADD CONSTRAINT uk_thqc1vykug4qhn8jdij04d1bh UNIQUE (keyword);


--
-- Name: hpd_ldap_attribute_types uk_xpm0rgr3crhwjvqndqj4i0; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY hpd_ldap_attribute_types
    ADD CONSTRAINT uk_xpm0rgr3crhwjvqndqj4i0 UNIQUE (name);


--
-- Name: usage_metadata usage_metadata_pkey; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY usage_metadata
    ADD CONSTRAINT usage_metadata_pkey PRIMARY KEY (id);


--
-- Name: cfg_dicom_scu_configuration fk_1uyssmhimn7bgvrpbosg76akr; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY cfg_dicom_scu_configuration
    ADD CONSTRAINT fk_1uyssmhimn7bgvrpbosg76akr FOREIGN KEY (configuration_id) REFERENCES cfg_configuration(id);


--
-- Name: gs_test_instance_participants fk_2721m2mcive6uurmopwl26osu; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY gs_test_instance_participants
    ADD CONSTRAINT fk_2721m2mcive6uurmopwl26osu FOREIGN KEY (aipo_id) REFERENCES tf_actor_integration_profile_option(id);


--
-- Name: gs_test_instance_test_instance_participants fk_2k4qd5vu7dd0i5qim72ixbkqw; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY gs_test_instance_test_instance_participants
    ADD CONSTRAINT fk_2k4qd5vu7dd0i5qim72ixbkqw FOREIGN KEY (test_instance_id) REFERENCES gs_test_instance(id);


--
-- Name: gs_test_instance_test_instance_participants fk_2suwyu48vy0hed31ggom84fmu; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY gs_test_instance_test_instance_participants
    ADD CONSTRAINT fk_2suwyu48vy0hed31ggom84fmu FOREIGN KEY (test_instance_participants_id) REFERENCES gs_test_instance_participants(id);


--
-- Name: affinity_domain_transactions fk_3oy78tmux2c99fvrqkegtiq6v; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY affinity_domain_transactions
    ADD CONSTRAINT fk_3oy78tmux2c99fvrqkegtiq6v FOREIGN KEY (transaction_id) REFERENCES tf_transaction(id);


--
-- Name: gs_test_instance fk_3u278s3asb3u74gte7oems8hr; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY gs_test_instance
    ADD CONSTRAINT fk_3u278s3asb3u74gte7oems8hr FOREIGN KEY (test_instance_status_id) REFERENCES gs_test_instance_status(id);


--
-- Name: cfg_hl7_responder_configuration fk_4f82i9wrql9oc6mnvetcy7is9; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY cfg_hl7_responder_configuration
    ADD CONSTRAINT fk_4f82i9wrql9oc6mnvetcy7is9 FOREIGN KEY (gs_system_id) REFERENCES gs_system(id);


--
-- Name: sys_conf_type_usages fk_4j6febpxhud8qe1uabp31nl19; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY sys_conf_type_usages
    ADD CONSTRAINT fk_4j6febpxhud8qe1uabp31nl19 FOREIGN KEY (system_configuration_id) REFERENCES system_configuration(id);


--
-- Name: gs_message fk_4lkwhox4s91pq6sjfftcajcn4; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY gs_message
    ADD CONSTRAINT fk_4lkwhox4s91pq6sjfftcajcn4 FOREIGN KEY (transaction_id) REFERENCES tf_transaction(id);


--
-- Name: cfg_hl7_v3_initiator_configuration fk_57xerkmwir86jmw899uw3ykpf; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY cfg_hl7_v3_initiator_configuration
    ADD CONSTRAINT fk_57xerkmwir86jmw899uw3ykpf FOREIGN KEY (configuration_id) REFERENCES cfg_configuration(id);


--
-- Name: cfg_hl7_v3_initiator_configuration fk_5fhnf9kr45rd5m1gd9riur2ev; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY cfg_hl7_v3_initiator_configuration
    ADD CONSTRAINT fk_5fhnf9kr45rd5m1gd9riur2ev FOREIGN KEY (gs_system_id) REFERENCES gs_system(id);


--
-- Name: tf_actor_integration_profile fk_5ilf7sfvvixf9f51lmphnwyoa; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY tf_actor_integration_profile
    ADD CONSTRAINT fk_5ilf7sfvvixf9f51lmphnwyoa FOREIGN KEY (integration_profile_id) REFERENCES tf_integration_profile(id);


--
-- Name: tf_actor_integration_profile fk_5yubxkv7cw5mqpv5u0axd4f1x; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY tf_actor_integration_profile
    ADD CONSTRAINT fk_5yubxkv7cw5mqpv5u0axd4f1x FOREIGN KEY (actor_id) REFERENCES tf_actor(id);


--
-- Name: gs_test_steps_instance fk_60pebu4cydl5h0hj12nkoisje; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY gs_test_steps_instance
    ADD CONSTRAINT fk_60pebu4cydl5h0hj12nkoisje FOREIGN KEY (syslog_config_id) REFERENCES cfg_syslog_configuration(id);


--
-- Name: gs_message fk_60qoisu4eph1wty4x7e55co98; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY gs_message
    ADD CONSTRAINT fk_60qoisu4eph1wty4x7e55co98 FOREIGN KEY (test_instance_participants_receiver_id) REFERENCES gs_test_instance_participants(id);


--
-- Name: cmn_message_instance_metadata fk_6pxpnca029j7ewvr7dus8q3sf; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY cmn_message_instance_metadata
    ADD CONSTRAINT fk_6pxpnca029j7ewvr7dus8q3sf FOREIGN KEY (message_instance_id) REFERENCES cmn_message_instance(id);


--
-- Name: cmn_receiver_console fk_6s2mr59uvd93xmp2dpkepfdvp; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY cmn_receiver_console
    ADD CONSTRAINT fk_6s2mr59uvd93xmp2dpkepfdvp FOREIGN KEY (domain_id) REFERENCES tf_domain(id);


--
-- Name: cmn_message_instance fk_6vrp0236251naw1qr1sk7y58r; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY cmn_message_instance
    ADD CONSTRAINT fk_6vrp0236251naw1qr1sk7y58r FOREIGN KEY (issuing_actor) REFERENCES tf_actor(id);


--
-- Name: ldap_node_object_class fk_7i2ni2nem4c5me5cd0qri8ews; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY ldap_node_object_class
    ADD CONSTRAINT fk_7i2ni2nem4c5me5cd0qri8ews FOREIGN KEY (object_class_id) REFERENCES hpd_ldap_object_classes(id);


--
-- Name: hpd_ldap_object_classes_must fk_7r53fh6lkkd3ovbk508p7y8yw; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY hpd_ldap_object_classes_must
    ADD CONSTRAINT fk_7r53fh6lkkd3ovbk508p7y8yw FOREIGN KEY (object_classes_id) REFERENCES hpd_ldap_object_classes(id);


--
-- Name: cmn_ip_address fk_8b3pqht8rw1vyiu1o0dm1ewtt; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY cmn_ip_address
    ADD CONSTRAINT fk_8b3pqht8rw1vyiu1o0dm1ewtt FOREIGN KEY (company_details_id) REFERENCES cmn_company_details(id);


--
-- Name: gs_contextual_information fk_8j7rd6ceveqykc8jl4gufcl75; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY gs_contextual_information
    ADD CONSTRAINT fk_8j7rd6ceveqykc8jl4gufcl75 FOREIGN KEY (path) REFERENCES tm_path(id);


--
-- Name: cfg_syslog_configuration fk_9n8k78nx4ygpn1aiv61gdhx8n; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY cfg_syslog_configuration
    ADD CONSTRAINT fk_9n8k78nx4ygpn1aiv61gdhx8n FOREIGN KEY (gs_system_id) REFERENCES gs_system(id);


--
-- Name: cfg_hl7_initiator_configuration fk_9uygh1fl3hk66l9fhpnov62ov; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY cfg_hl7_initiator_configuration
    ADD CONSTRAINT fk_9uygh1fl3hk66l9fhpnov62ov FOREIGN KEY (configuration_id) REFERENCES cfg_configuration(id);


--
-- Name: cfg_syslog_configuration fk_a5s6a6cfwata8c4iae37f30md; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY cfg_syslog_configuration
    ADD CONSTRAINT fk_a5s6a6cfwata8c4iae37f30md FOREIGN KEY (configuration_id) REFERENCES cfg_configuration(id);


--
-- Name: mbv_constraint fk_a9f1f4s3giknx17lapvv09vb3; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY mbv_constraint
    ADD CONSTRAINT fk_a9f1f4s3giknx17lapvv09vb3 FOREIGN KEY (classtype_id) REFERENCES mbv_class_type(id);


--
-- Name: cfg_dicom_scu_configuration fk_atmtmfff6dh3cfmgymfvuv32x; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY cfg_dicom_scu_configuration
    ADD CONSTRAINT fk_atmtmfff6dh3cfmgymfvuv32x FOREIGN KEY (gs_system_id) REFERENCES gs_system(id);


--
-- Name: mbv_class_type fk_awc0r3uu9khjgxisjms3x4mih; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY mbv_class_type
    ADD CONSTRAINT fk_awc0r3uu9khjgxisjms3x4mih FOREIGN KEY (documentation_spec_id) REFERENCES mbv_documentation_spec(id);


--
-- Name: cfg_dicom_scp_configuration fk_bb0i01umf0nkm6s0ntwf16jve; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY cfg_dicom_scp_configuration
    ADD CONSTRAINT fk_bb0i01umf0nkm6s0ntwf16jve FOREIGN KEY (sop_class_id) REFERENCES cfg_sop_class(id);


--
-- Name: gs_test_steps_instance fk_bq3udwuexl1gy795hpwru5dqe; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY gs_test_steps_instance
    ADD CONSTRAINT fk_bq3udwuexl1gy795hpwru5dqe FOREIGN KEY (dicom_scu_config_id) REFERENCES cfg_dicom_scu_configuration(id);


--
-- Name: cfg_web_service_configuration fk_cam5r7r5pfmnofleffgk0e32f; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY cfg_web_service_configuration
    ADD CONSTRAINT fk_cam5r7r5pfmnofleffgk0e32f FOREIGN KEY (configuration_id) REFERENCES cfg_configuration(id);


--
-- Name: cfg_hl7_responder_configuration fk_cnmpmi83qdgkf29ca5vp0nlk0; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY cfg_hl7_responder_configuration
    ADD CONSTRAINT fk_cnmpmi83qdgkf29ca5vp0nlk0 FOREIGN KEY (configuration_id) REFERENCES cfg_configuration(id);


--
-- Name: gs_test_steps_instance fk_d6l3ub5y7jlicc47udu6pc8gf; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY gs_test_steps_instance
    ADD CONSTRAINT fk_d6l3ub5y7jlicc47udu6pc8gf FOREIGN KEY (hl7v2_responder_config_id) REFERENCES cfg_hl7_responder_configuration(id);


--
-- Name: gs_test_steps_instance fk_d7nna7jtx4je8ruwukta41upc; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY gs_test_steps_instance
    ADD CONSTRAINT fk_d7nna7jtx4je8ruwukta41upc FOREIGN KEY (dicom_scp_config_id) REFERENCES cfg_dicom_scp_configuration(id);


--
-- Name: mbv_standards fk_dch4reyp4qvadb9xwq495xf4q; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY mbv_standards
    ADD CONSTRAINT fk_dch4reyp4qvadb9xwq495xf4q FOREIGN KEY (package_id) REFERENCES mbv_package(id);


--
-- Name: tf_actor_integration_profile_option fk_dd4cllr6xpdtoxp5wnjv4i2jn; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY tf_actor_integration_profile_option
    ADD CONSTRAINT fk_dd4cllr6xpdtoxp5wnjv4i2jn FOREIGN KEY (actor_integration_profile_id) REFERENCES tf_actor_integration_profile(id);


--
-- Name: tf_domain_integration_profiles fk_ditnlab3yy7ipby4do2dl4k9v; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY tf_domain_integration_profiles
    ADD CONSTRAINT fk_ditnlab3yy7ipby4do2dl4k9v FOREIGN KEY (integration_profile_id) REFERENCES tf_integration_profile(id);


--
-- Name: cmn_receiver_console fk_dvjns4jx634sntdq34baohlny; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY cmn_receiver_console
    ADD CONSTRAINT fk_dvjns4jx634sntdq34baohlny FOREIGN KEY (transaction_id) REFERENCES tf_transaction(id);


--
-- Name: gs_test_steps_instance fk_e47nbep6bi6div7m4h09j40yx; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY gs_test_steps_instance
    ADD CONSTRAINT fk_e47nbep6bi6div7m4h09j40yx FOREIGN KEY (hl7v3_responder_config_id) REFERENCES cfg_hl7_v3_responder_configuration(id);


--
-- Name: gs_message fk_f0ffk1767j7rom46uil2fojou; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY gs_message
    ADD CONSTRAINT fk_f0ffk1767j7rom46uil2fojou FOREIGN KEY (test_instance_participants_sender_id) REFERENCES gs_test_instance_participants(id);


--
-- Name: gs_test_instance_participants fk_g5b1negarlf7einpqd9dyxhy5; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY gs_test_instance_participants
    ADD CONSTRAINT fk_g5b1negarlf7einpqd9dyxhy5 FOREIGN KEY (system_id) REFERENCES gs_system(id);


--
-- Name: cfg_web_service_configuration fk_g9fr1fk5hwvt5qd0b151x68b9; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY cfg_web_service_configuration
    ADD CONSTRAINT fk_g9fr1fk5hwvt5qd0b151x68b9 FOREIGN KEY (gs_system_id) REFERENCES gs_system(id);


--
-- Name: gs_test_steps_instance fk_gci5txhtlwlkonbqjdfeue6k9; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY gs_test_steps_instance
    ADD CONSTRAINT fk_gci5txhtlwlkonbqjdfeue6k9 FOREIGN KEY (web_service_config_id) REFERENCES cfg_web_service_configuration(id);


--
-- Name: cfg_dicom_scp_configuration fk_gmhl29xoaas6axade58e6h18y; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY cfg_dicom_scp_configuration
    ADD CONSTRAINT fk_gmhl29xoaas6axade58e6h18y FOREIGN KEY (gs_system_id) REFERENCES gs_system(id);


--
-- Name: usage_metadata fk_h6b2lqp7q4crn70h4noft57dm; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY usage_metadata
    ADD CONSTRAINT fk_h6b2lqp7q4crn70h4noft57dm FOREIGN KEY (transaction_id) REFERENCES tf_transaction(id);


--
-- Name: tm_oid fk_hcooq91f0xb0dt3lw69f4mhrp; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY tm_oid
    ADD CONSTRAINT fk_hcooq91f0xb0dt3lw69f4mhrp FOREIGN KEY (system_id) REFERENCES gs_system(id);


--
-- Name: gs_test_instance_oid fk_he8jcif34crxu9bx6bjpf70ro; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY gs_test_instance_oid
    ADD CONSTRAINT fk_he8jcif34crxu9bx6bjpf70ro FOREIGN KEY (test_instance_id) REFERENCES gs_test_instance(id);


--
-- Name: gs_test_steps_instance fk_hhvsfxik1plfet5oenwrhmmsq; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY gs_test_steps_instance
    ADD CONSTRAINT fk_hhvsfxik1plfet5oenwrhmmsq FOREIGN KEY (testinstance_id) REFERENCES gs_test_instance(id);


--
-- Name: tf_actor_integration_profile_option fk_ichumu56164vfu1j44qb0uos3; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY tf_actor_integration_profile_option
    ADD CONSTRAINT fk_ichumu56164vfu1j44qb0uos3 FOREIGN KEY (integration_profile_option_id) REFERENCES tf_integration_profile_option(id);


--
-- Name: gs_test_steps_instance fk_ijvev88dlp0g3wyawbewr0k98; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY gs_test_steps_instance
    ADD CONSTRAINT fk_ijvev88dlp0g3wyawbewr0k98 FOREIGN KEY (hl7v2_initiator_config_id) REFERENCES cfg_hl7_initiator_configuration(id);


--
-- Name: cmn_transaction_instance fk_iuc2f38isetr9m12flpqj049i; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY cmn_transaction_instance
    ADD CONSTRAINT fk_iuc2f38isetr9m12flpqj049i FOREIGN KEY (transaction_id) REFERENCES tf_transaction(id);


--
-- Name: tf_domain_integration_profiles fk_j3fpkysr19ckcu98bb7hbj8nu; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY tf_domain_integration_profiles
    ADD CONSTRAINT fk_j3fpkysr19ckcu98bb7hbj8nu FOREIGN KEY (domain_id) REFERENCES tf_domain(id);


--
-- Name: gs_test_instance_oid fk_j4ksfu8dbved4pd1jgcrtagqf; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY gs_test_instance_oid
    ADD CONSTRAINT fk_j4ksfu8dbved4pd1jgcrtagqf FOREIGN KEY (oid_configuration_id) REFERENCES tm_oid(id);


--
-- Name: hpd_ldap_object_classes_may fk_kcdc641q5hjh74f1ekyek0p79; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY hpd_ldap_object_classes_may
    ADD CONSTRAINT fk_kcdc641q5hjh74f1ekyek0p79 FOREIGN KEY (object_classes_id) REFERENCES hpd_ldap_object_classes(id);


--
-- Name: mbv_class_type fk_kcyt8tgre58f5uasosbq0q12i; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY mbv_class_type
    ADD CONSTRAINT fk_kcyt8tgre58f5uasosbq0q12i FOREIGN KEY (package_id) REFERENCES mbv_package(id);


--
-- Name: gs_test_steps_instance fk_knidtr35pirurpxk347rf9l6d; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY gs_test_steps_instance
    ADD CONSTRAINT fk_knidtr35pirurpxk347rf9l6d FOREIGN KEY (hl7v3_initiator_config_id) REFERENCES cfg_hl7_v3_initiator_configuration(id);


--
-- Name: cmn_transaction_instance fk_kp300ev0h6s2ocpy5j8djco4v; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY cmn_transaction_instance
    ADD CONSTRAINT fk_kp300ev0h6s2ocpy5j8djco4v FOREIGN KEY (request_id) REFERENCES cmn_message_instance(id);


--
-- Name: cmn_transaction_instance fk_leqiasjr73fuy7ffhqmrhhsaa; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY cmn_transaction_instance
    ADD CONSTRAINT fk_leqiasjr73fuy7ffhqmrhhsaa FOREIGN KEY (response_id) REFERENCES cmn_message_instance(id);


--
-- Name: cfg_configuration fk_lpgg35tul90orrn8vvvtyb80r; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY cfg_configuration
    ADD CONSTRAINT fk_lpgg35tul90orrn8vvvtyb80r FOREIGN KEY (host_id) REFERENCES cfg_host(id);


--
-- Name: sys_conf_type_usages fk_mvt7iq3yl2ublo7ab7rpg98s; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY sys_conf_type_usages
    ADD CONSTRAINT fk_mvt7iq3yl2ublo7ab7rpg98s FOREIGN KEY (listusages_id) REFERENCES usage_metadata(id);


--
-- Name: cfg_dicom_scp_configuration fk_o8cpt1ws21yuupo411fqdvf0t; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY cfg_dicom_scp_configuration
    ADD CONSTRAINT fk_o8cpt1ws21yuupo411fqdvf0t FOREIGN KEY (configuration_id) REFERENCES cfg_configuration(id);


--
-- Name: ldap_node_object_class fk_odwl0qmo2tykpmh01tp89rfsv; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY ldap_node_object_class
    ADD CONSTRAINT fk_odwl0qmo2tykpmh01tp89rfsv FOREIGN KEY (ldap_node_id) REFERENCES ldap_node(id);


--
-- Name: affinity_domain_transactions fk_oonw5dnptgu8fqtebob7173p5; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY affinity_domain_transactions
    ADD CONSTRAINT fk_oonw5dnptgu8fqtebob7173p5 FOREIGN KEY (affinity_domain_id) REFERENCES affinity_domain(id);


--
-- Name: cfg_dicom_scu_configuration fk_p47iuej292p3f2sawsexrx4m2; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY cfg_dicom_scu_configuration
    ADD CONSTRAINT fk_p47iuej292p3f2sawsexrx4m2 FOREIGN KEY (sop_class_id) REFERENCES cfg_sop_class(id);


--
-- Name: cfg_hl7_initiator_configuration fk_p641ssuuyy3ag6tg9uunvy1pf; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY cfg_hl7_initiator_configuration
    ADD CONSTRAINT fk_p641ssuuyy3ag6tg9uunvy1pf FOREIGN KEY (gs_system_id) REFERENCES gs_system(id);


--
-- Name: gs_contextual_information_instance fk_pa62xsmkceyisp0dclhsy36tl; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY gs_contextual_information_instance
    ADD CONSTRAINT fk_pa62xsmkceyisp0dclhsy36tl FOREIGN KEY (test_steps_instance_id) REFERENCES gs_test_steps_instance(id);


--
-- Name: cfg_hl7_v3_responder_configuration fk_phc7kx0qg1jjfrs9b13uaclta; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY cfg_hl7_v3_responder_configuration
    ADD CONSTRAINT fk_phc7kx0qg1jjfrs9b13uaclta FOREIGN KEY (gs_system_id) REFERENCES gs_system(id);


--
-- Name: usage_metadata fk_pl8yqxsqsua7w6nk02nmibrym; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY usage_metadata
    ADD CONSTRAINT fk_pl8yqxsqsua7w6nk02nmibrym FOREIGN KEY (affinity_id) REFERENCES affinity_domain(id);


--
-- Name: cfg_hl7_v3_responder_configuration fk_pwniunln1hw1lb8tb8jp3cxd9; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY cfg_hl7_v3_responder_configuration
    ADD CONSTRAINT fk_pwniunln1hw1lb8tb8jp3cxd9 FOREIGN KEY (configuration_id) REFERENCES cfg_configuration(id);


--
-- Name: ldap_partition_node fk_qa7m7wwd997emv7cp928qmqmu; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY ldap_partition_node
    ADD CONSTRAINT fk_qa7m7wwd997emv7cp928qmqmu FOREIGN KEY (ldap_partition_id) REFERENCES ldap_partition(id);


--
-- Name: mbv_assertion fk_qn571k10ispaog0vfcunl16xr; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY mbv_assertion
    ADD CONSTRAINT fk_qn571k10ispaog0vfcunl16xr FOREIGN KEY (constraint_id) REFERENCES mbv_constraint(id);


--
-- Name: cmn_transaction_instance fk_qosa869w00k0f6cp3u3i4i6y8; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY cmn_transaction_instance
    ADD CONSTRAINT fk_qosa869w00k0f6cp3u3i4i6y8 FOREIGN KEY (simulated_actor_id) REFERENCES tf_actor(id);


--
-- Name: gs_contextual_information_instance fk_r37ybyowksux2ytociytsbeij; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY gs_contextual_information_instance
    ADD CONSTRAINT fk_r37ybyowksux2ytociytsbeij FOREIGN KEY (contextual_information_id) REFERENCES gs_contextual_information(id);


--
-- Name: cmn_transaction_instance fk_s1syfhsdftpjsr8wk5pvk6idr; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY cmn_transaction_instance
    ADD CONSTRAINT fk_s1syfhsdftpjsr8wk5pvk6idr FOREIGN KEY (domain_id) REFERENCES tf_domain(id);


--
-- Name: ldap_partition_node fk_t78kqjp8oae7ipn83uat9m318; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY ldap_partition_node
    ADD CONSTRAINT fk_t78kqjp8oae7ipn83uat9m318 FOREIGN KEY (ldap_node_id) REFERENCES ldap_node(id);


--
-- Name: cmn_receiver_console fk_tnagd28ej30pkampt9gjs28d; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY cmn_receiver_console
    ADD CONSTRAINT fk_tnagd28ej30pkampt9gjs28d FOREIGN KEY (simulated_actor_id) REFERENCES tf_actor(id);


--
-- PostgreSQL database dump complete
--

