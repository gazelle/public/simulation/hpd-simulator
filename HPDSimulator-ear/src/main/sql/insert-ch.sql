INSERT INTO tf_transaction(id, keyword, name, description) VALUES (nextval('tf_transaction_id_seq'), 'PIDD', 'Provider Information Delta Download', 'EPD specific transaction: Provider Information Delta Download');
INSERT INTO affinity_domain (id, label_to_display, profile, keyword) VALUES (nextval('affinity_domain_id_seq'), 'EPD', 'CH:HPD', 'CH');
INSERT INTO affinity_domain_transactions (affinity_domain_id, transaction_id) VALUES ((SELECT id from affinity_domain WHERE keyword = 'CH'), (select id from tf_transaction where keyword = 'PIDD'));
INSERT INTO usage_metadata (id, transaction_id, affinity_id, action) VALUES (nextval('usage_id_seq'), (select id from tf_transaction where keyword = 'PIDD'), (SELECT id from affinity_domain WHERE keyword = 'CH'), 'Provider Information Delta Download');
INSERT INTO tf_domain (id, description, keyword, name) VALUES (nextval('tf_domain_id_seq'), 'Elektronische Patient Dossier', 'EPD', 'CH EPD');
INSERT INTO affinity_domain_transactions (affinity_domain_id, transaction_id) VALUES ((SELECT id from affinity_domain WHERE keyword = 'CH'), (select id from tf_transaction where keyword = 'ITI-58'));
INSERT INTO affinity_domain_transactions (affinity_domain_id, transaction_id) VALUES ((SELECT id from affinity_domain WHERE keyword = 'CH'), (select id from tf_transaction where keyword = 'ITI-59'));

INSERT INTO tf_transaction(id, keyword, name, description) VALUES (nextval('tf_transaction_id_seq'), 'CIQ', 'Community Information Query', 'EPD specific transaction: Community Information Query');
INSERT INTO affinity_domain_transactions (affinity_domain_id, transaction_id) VALUES ((SELECT id from affinity_domain WHERE keyword = 'CH'), (select id from tf_transaction where keyword = 'CIQ'));
INSERT INTO usage_metadata (id, transaction_id, affinity_id, action) VALUES (nextval('usage_id_seq'), (select id from tf_transaction where keyword = 'CIQ'), (SELECT id from affinity_domain WHERE keyword = 'CH'), 'Provider Information Delta Download');
INSERT INTO tf_actor(id, description, keyword, name, can_act_as_responder) VALUES (nextval('tf_actor_id_seq'), 'CPI Provider', 'CPI_PROV', 'CPI Provider', true);
INSERT INTO tf_actor(id, description, keyword, name, can_act_as_responder) VALUES (nextval('tf_actor_id_seq'), 'CPI Consumer', 'CPI_CONS', 'CPI Consumer', false);
