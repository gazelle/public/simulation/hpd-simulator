--
-- PostgreSQL database dump
--

-- Dumped from database version 9.6.7
-- Dumped by pg_dump version 9.6.7

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SET check_function_bodies = false;
SET client_min_messages = warning;
SET row_security = off;

SET search_path = public, pg_catalog;

SET default_tablespace = '';

SET default_with_oids = false;

--
-- Name: hpd_ldap_object_classes; Type: TABLE; Schema: public; Owner: gazelle
--

CREATE TABLE hpd_ldap_object_classes (
    id integer NOT NULL,
    description character varying(255),
    name character varying(255),
    oid character varying(255),
    sup_object_class character varying(255),
    type_object_class integer
);


ALTER TABLE hpd_ldap_object_classes OWNER TO gazelle;

--
-- Data for Name: hpd_ldap_object_classes; Type: TABLE DATA; Schema: public; Owner: gazelle
--

COPY hpd_ldap_object_classes (id, description, name, oid, sup_object_class, type_object_class) FROM stdin;
1	top of the superclass chain	top	2.5.6.0		1
2	RFC2256: a person	person	2.5.6.6	top	1
3	object class holding the naturalPerson attribute	naturalPerson	1.2.840.113549.1.9.24.2	top	0
5	object class holding the pkscEntity attribute	pkcsEntity	1.2.840.113549.1.9.24.1	top	0
6	RFC2256: a group of names (DNs)	groupOfNames	2.5.6.9	top	1
8	RFC2252: extensible object	extensibleObject	1.3.6.1.4.1.1466.101.120.111	top	0
9	RFC2256: an organization	organization	2.5.6.4	top	1
10		HPDProvider	1.3.6.1.4.1.19376.1.2.4.1	top	0
11		HPDProviderMembership	1.3.6.1.4.1.19376.1.2.4.4	top	1
12		HPDProviderCredential	1.3.6.1.4.1.19376.1.2.4.2	top	1
13	object class holding the hpdRejectMailFrom attribute	hpdMailPerson	1.3.6.1.4.1.1466.115.121.1.1.7		0
14		HPDElectronicService	1.3.6.1.4.1.19376.1.2.4.5	top	1
15	RFC2798: Internet Organizational Person	inetOrgPerson	2.16.840.1.113730.3.2.2	organizationalPerson	1
16		HCRegulatedOrganization	1.0.21091.1.4	organization	1
17		HCCodedReference	1.0.21091.1.10	top	0
18		HCStandardRole	1.0.21091.1.8	groupOfNames	1
19		HCPayer	1.0.21091.1.5	organization	1
20		HCSupportingOrganization	1.0.21091.1.6	organization	1
21		HCOrganizationalRole	1.0.21091.1.7	organizationalRole	1
22		HCConsumer	1.0.21091.1.1	inetOrgPerson	1
23		HCEmployee	1.0.21091.1.3	inetOrgPerson	1
24		HCProfessional	1.0.21091.1.2	inetOrgPerson	1
25		HCLocalRole	1.0.21091.1.9	groupOfNames	1
26		HCDevice	1.0.21091.1.11	top	0
27	Test	personTest	1.2.3.4	top	1
30	Test	personTest2	1.2.3.4	top	1
\.


--
-- Name: hpd_ldap_object_classes hpd_ldap_object_classes_pkey; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY hpd_ldap_object_classes
    ADD CONSTRAINT hpd_ldap_object_classes_pkey PRIMARY KEY (id);


--
-- Name: hpd_ldap_object_classes uk_gcuvfhoc1fbgpgmyetttimyom; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY hpd_ldap_object_classes
    ADD CONSTRAINT uk_gcuvfhoc1fbgpgmyetttimyom UNIQUE (name);


--
-- PostgreSQL database dump complete
--

