--
-- PostgreSQL database dump
--

SET statement_timeout = 0;
SET lock_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SET check_function_bodies = false;
SET client_min_messages = warning;

SET search_path = public, pg_catalog;

SET default_tablespace = '';

SET default_with_oids = false;

--
-- Name: ldap_partition; Type: TABLE; Schema: public; Owner: gazelle; Tablespace: 
--

CREATE TABLE ldap_partition (
    id integer NOT NULL,
    base_dn character varying(255),
    creation_date date,
    creator character varying(255),
    description text,
    dn_modify_regex character varying(255),
    dn_search_regex character varying(255),
    is_default boolean,
    name character varying(255) NOT NULL,
    server_ip character varying(255),
    server_port integer
);


ALTER TABLE ldap_partition OWNER TO gazelle;

--
-- Data for Name: ldap_partition; Type: TABLE DATA; Schema: public; Owner: gazelle
--

COPY ldap_partition (id, base_dn, creation_date, creator, description, dn_modify_regex, dn_search_regex, is_default, name, server_ip, server_port) FROM stdin;
1	dc=HPD,o=IHE-Europe,c=FRA	2014-06-26	aberge	default partition for connectathon testing purposes	uid=.*,ou=(HCRegulatedOrganization|HCProfessional|HCRelationship),dc=HPD,o=IHE-Europe,c=FRA	ou=(HCRegulatedOrganization|HCProfessional|HCRelationship),dc=HPD,o=IHE-Europe,c=FRA	t	Gazelle HPD Directory	94.23.247.108	10389
\.


--
-- Name: ldap_partition_name_key; Type: CONSTRAINT; Schema: public; Owner: gazelle; Tablespace: 
--

ALTER TABLE ONLY ldap_partition
    ADD CONSTRAINT ldap_partition_name_key UNIQUE (name);


--
-- Name: ldap_partition_pkey; Type: CONSTRAINT; Schema: public; Owner: gazelle; Tablespace: 
--

ALTER TABLE ONLY ldap_partition
    ADD CONSTRAINT ldap_partition_pkey PRIMARY KEY (id);


--
-- PostgreSQL database dump complete
--

