INSERT INTO app_configuration (id, variable, value) VALUES (1, 'xsd_location', '/home/aberge/Development/xsd/IHE/DSMLv2.xsd');
INSERT INTO app_configuration (id, variable, value) VALUES (2, 'application_url', 'http://localhost:8080/HPDSimulator');
INSERT INTO app_configuration (id, variable, value) VALUES (3, 'dsml_xsl_location', 'http://gazelle.ihe.net/xsl/dsmlStylesheet.xsl');
INSERT INTO app_configuration (id, variable, value) VALUES (4, 'results_xsl_location', 'http://gazelle.ihe.net/xsl/hl7v3validatorDetailedResult.xsl');
INSERT INTO app_configuration (id, variable, value) VALUES (5, 'application_release_notes_url', 'http://gazelle.ihe.net/jira/browse/HPD#selectedTab=com.atlassian.jira.plugin.system.project%3Achangelog-panel');
INSERT INTO app_configuration (id, variable, value) VALUES (6, 'documentation_url', 'http://gazelle.ihe.net/content/hpd-simulator');
INSERT INTO app_configuration (id, variable, value) VALUES (7, 'message_permanent_link', 'http://localhost:8080/HPDSimulator/messages/messageDisplay.seam?id=');
INSERT INTO app_configuration (id, variable, value) VALUES (8, 'application_name', 'HPD Simulator');
INSERT INTO app_configuration (id, variable, value) VALUES (9, 'NUMBER_OF_ITEMS_PER_PAGE', '20');
INSERT INTO app_configuration (id, variable, value) VALUES (10, 'prov_info_dir_wsdl', 'http://localhost:8080/HPDSimulator-ejb/ProviderInformationDirectory_Service/ProviderInformationDirectory_PortType?wsdl');
INSERT INTO app_configuration (id, variable, value) VALUES (11, 'time_zone', 'Europe/Paris');
INSERT INTO app_configuration (id, variable, value) VALUES (12, 'ldap_user', '');
INSERT INTO app_configuration (id, variable, value) VALUES (13, 'ldap_password', '');
INSERT INTO app_configuration (id, variable, value) VALUES (16, 'svs_repository_url', 'http://gazelle.ihe.net');
INSERT INTO app_configuration (id, variable, value) VALUES (17, 'restrict_access_to_messages', 'false');
SELECT pg_catalog.setval('app_configuration_id_seq', 17, true);

INSERT INTO tf_transaction(id, description, keyword, name) VALUES (1, 'Provider Information Query', 'ITI-58', 'ITI-58');
INSERT INTO tf_transaction(id, description, keyword, name) VALUES (2, 'Provider Information Feed', 'ITI-59', 'ITI-59');
SELECT pg_catalog.setval('tf_transaction_id_seq', 2, true);

INSERT INTO tf_actor(id, description, keyword, name, can_act_as_responder) VALUES (1, 'Provider Information Directory', 'PROV_INFO_DIR', 'Provider Information Directory', true);
INSERT INTO tf_actor(id, description, keyword, name, can_act_as_responder) VALUES (2, 'Provider Information Source', 'PROV_INFO_SRC', 'Provider Information Source', false);
INSERT INTO tf_actor(id, description, keyword, name, can_act_as_responder) VALUES (3, 'Provider Information Consumer', 'PROV_INFO_CONS', 'Provider Information Consumer', false);
SELECT pg_catalog.setval('tf_actor_id_seq', 3 , true);

INSERT INTO tf_integration_profile (id, description, keyword, name) VALUES (1, 'Healthcare Directory Provider', 'HPD', 'Healthcare Directory Provider');
SELECT pg_catalog.setval('tf_integration_profile_id_seq', 1, true);

INSERT INTO tf_domain (id, description, keyword, name) VALUES (1, 'IT Infrastructure', 'ITI', 'IT Infrastructure');
SELECT pg_catalog.setval('tf_domain_id_seq', 1, true);

INSERT INTO tf_domain_integration_profiles (integration_profile_id, domain_id) VALUES (1, 1);

INSERT INTO affinity_domain (id, label_to_display, profile, keyword) VALUES (1, 'IHE', 'HPD', 'IHE');
SELECT pg_catalog.setval('affinity_domain_id_seq', 1, true);

INSERT INTO affinity_domain_transactions (affinity_domain_id, transaction_id) VALUES (1, 1);
INSERT INTO affinity_domain_transactions (affinity_domain_id, transaction_id) VALUES (1, 2);

INSERT INTO usage_metadata (id, transaction_id, affinity_id, action) VALUES (1, 1, 1, 'Provider Information Query');
INSERT INTO usage_metadata (id, transaction_id, affinity_id, action) VALUES (2, 2, 1, 'Provider Information Feed');
SELECT pg_catalog.setval('usage_id_seq', 2, true);
