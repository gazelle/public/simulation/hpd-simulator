package net.ihe.gazelle.chcpi.simulator.cpiprovider;

public class SchemaValidationException extends Exception {

    public SchemaValidationException(String reason) {
        super(reason);
    }
}
