package net.ihe.gazelle.chcpi.action;

import net.ihe.gazelle.chcpi.model.CIDDMessage;
import net.ihe.gazelle.chcpi.model.CIDDMessageQuery;
import net.ihe.gazelle.chcpi.model.DateAdapter;
import net.ihe.gazelle.hql.providers.EntityManagerService;
import org.jboss.seam.Component;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.persistence.EntityManager;
import java.text.ParseException;
import java.util.Date;
import java.util.List;

public class CIDDMessageDAO {

    private static final Logger LOG = LoggerFactory.getLogger(CIDDMessageDAO.class);

    private CIDDMessageDAO() {
        throw new IllegalStateException("Utility class");
    }

    public static List<CIDDMessage> queryValidCIDDMessages(String fromDate, String toDate) {
        Date from = null;
        Date to = null;
        try {
            from = DateAdapter.unmarshal(fromDate);
            to = DateAdapter.unmarshal(toDate);
        } catch (ParseException e) {
            LOG.error(e.getMessage());
        }
        return queryValidCIDDMessages(from, to);
    }

    public static List<CIDDMessage> queryValidCIDDMessages(Date fromDate, Date toDate) {
        CIDDMessageQuery query = new CIDDMessageQuery();
        query.valid().eq(true);
        query.timestamp().ge(fromDate);
        if (toDate != null) {
            query.timestamp().le(toDate);
        }
        return query.getList();
    }

    public static List<CIDDMessage> queryAllCIDDMessages() {
        CIDDMessageQuery query = new CIDDMessageQuery();
        return query.getList();
    }

    public static void delete(CIDDMessage selectedCiddMessage) {
        EntityManager entityManager = EntityManagerService.provideEntityManager();
        delete(selectedCiddMessage, entityManager);
    }

    public static void delete(CIDDMessage selectedCiddMessage, EntityManager entityManager) {
        CIDDMessageQuery query = new CIDDMessageQuery();
        query.id().eq(selectedCiddMessage.getId());
        CIDDMessage ciddMessageToDelete = query.getUniqueResult();
        entityManager.remove(ciddMessageToDelete);
        entityManager.flush();
    }

    public static CIDDMessage save(CIDDMessage ciddMessageToSave) {
        EntityManager entityManager = (EntityManager) Component.getInstance("entityManager");
        return save(ciddMessageToSave, entityManager);
    }

    public static CIDDMessage save(CIDDMessage ciddMessageToSave, EntityManager entityManager) {
        CIDDMessage ciddMessage = entityManager.merge(ciddMessageToSave);
        entityManager.flush();
        return ciddMessage;
    }
}
