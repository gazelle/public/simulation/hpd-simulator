package net.ihe.gazelle.chcpi.model;

import net.ihe.gazelle.hpd.BatchRequest;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
import java.util.ArrayList;
import java.util.List;


/**
 * <p>Java class for DownloadResponse complex type.
 *
 * <p>The following schema fragment specifies the expected content contained within this class.
 *
 * <pre>
 * &lt;complexType name="DownloadResponse">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element ref="{urn:oasis:names:tc:DSML:2:0:core}batchRequest" maxOccurs="unbounded" minOccurs="0"/>
 *       &lt;/sequence>
 *       &lt;attribute name="requestID" type="{http://www.w3.org/2001/XMLSchema}string" />
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "DownloadResponse", namespace = "urn:ch:admin:bag:epr:2017", propOrder = {
        "batchRequest"
})
@XmlRootElement(
        namespace = "urn:ch:admin:bag:epr:2017",
        name = "downloadResponse"
)
public class DownloadResponse {

    @XmlElement(namespace = "urn:oasis:names:tc:DSML:2:0:core")
    protected List<BatchRequest> batchRequest;
    @XmlAttribute(name = "requestID")
    protected String requestID;

    /**
     * Gets the value of the batchRequest property.
     *
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the batchRequest property.
     *
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getBatchRequest().add(newItem);
     * </pre>
     *
     *
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link BatchRequest }
     */
    public List<BatchRequest> getBatchRequest() {
        if (batchRequest == null) {
            batchRequest = new ArrayList<BatchRequest>();
        }
        return this.batchRequest;
    }

    /**
     * Gets the value of the requestID property.
     *
     * @return possible object is
     * {@link String }
     */
    public String getRequestID() {
        return requestID;
    }

    /**
     * Sets the value of the requestID property.
     *
     * @param value allowed object is
     *              {@link String }
     */
    public void setRequestID(String value) {
        this.requestID = value;
    }

    public void setBatchRequest(List<BatchRequest> batchRequest) {
        this.batchRequest = batchRequest;
    }

    public void addBatchRequest(BatchRequest batchRequest_elt) {
        this.getBatchRequest().add(batchRequest_elt);
    }

    public void removeBatchRequest(BatchRequest batchRequest_elt) {
        this.getBatchRequest().remove(batchRequest_elt);
    }

}
