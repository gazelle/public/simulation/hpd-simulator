package net.ihe.gazelle.chcpi.model;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.TimeZone;

public class DateAdapter {

    private static final Logger LOG = LoggerFactory.getLogger(DateAdapter.class);


    // All format as defined in https://www.w3.org/TR/NOTE-datetime
    private static final String[] CUSTOM_FORMAT_STRING = new String[]{"yyyy-MM-dd'T'HH:mm:ss.SSSX", "yyyy-MM-dd'T'HH:mm:ssX", "yyyy-MM-dd'T'HH:mmX",
            "yyyy-MM-dd", "yyyy-MM", "yyyy"};

    public static Date unmarshal(String v) throws ParseException {
        for (String format : CUSTOM_FORMAT_STRING) {
            try {
                SimpleDateFormat simpleDateFormat = new SimpleDateFormat(format);
                simpleDateFormat.setTimeZone(TimeZone.getTimeZone("UTC"));
                simpleDateFormat.setLenient(false);
                return simpleDateFormat.parse(v);
            } catch (ParseException e) {
                LOG.error(e.getMessage() + " with format : " + format);
            }
        }
        throw new ParseException("Cannot parse the date" + v, 0);
    }

}
