package net.ihe.gazelle.ldap.model;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;

@XmlRegistry
public class ObjectFactory {

	private final static QName _DocumentRootXMLNSPrefixMap_QNAME = new QName("", "xmlns:prefix");
	private final static QName _DocumentRootXSISchemaLocation_QNAME = new QName("", "xsi:schemaLocation");
	private final static QName _DocumentRootLDAPStructure_QNAME = new QName("", "LDAPStructure");
	private final static QName _StructureObjectClass_QNAME = new QName("", "objectClass");
	private final static QName _StructureAttributeType_QNAME = new QName("", "attributeType");

	/**
	 * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: generated
	 * 
	 */
	public ObjectFactory() {
	}

	/**
	 * Create an instance of {@link net.ihe.gazelle.ldap.model.AttributeTypes}
	 * 
	 */
	public AttributeTypes createAttributeTypes() {
		return new AttributeTypes();
	}

	/**
	 * Create an instance of {@link net.ihe.gazelle.ldap.model.DocumentRoot}
	 * 
	 */
	public DocumentRoot createDocumentRoot() {
		return new DocumentRoot();
	}

	/**
	 * Create an instance of {@link net.ihe.gazelle.ldap.model.Structure}
	 * 
	 */
	public Structure createStructure() {
		return new Structure();
	}

	/**
	 * Create an instance of {@link net.ihe.gazelle.ldap.model.ObjectClasses}
	 * 
	 */
	public ObjectClasses createObjectClasses() {
		return new ObjectClasses();
	}

	/**
	 * Create an instance of {@link JAXBElement }
	 * 
	 */
	@XmlElementDecl(namespace = "", name = "xmlns:prefix", scope = DocumentRoot.class)
	public JAXBElement<String> createDocumentRootXMLNSPrefixMap(String value) {
		return new JAXBElement<String>(_DocumentRootXMLNSPrefixMap_QNAME, String.class, DocumentRoot.class, value);
	}

	/**
	 * Create an instance of {@link JAXBElement }
	 * 
	 */
	@XmlElementDecl(namespace = "", name = "xsi:schemaLocation", scope = DocumentRoot.class)
	public JAXBElement<String> createDocumentRootXSISchemaLocation(String value) {
		return new JAXBElement<String>(_DocumentRootXSISchemaLocation_QNAME, String.class, DocumentRoot.class, value);
	}

	/**
	 * Create an instance of {@link JAXBElement }
	 * 
	 */
	@XmlElementDecl(namespace = "", name = "LDAPStructure", scope = DocumentRoot.class)
	public JAXBElement<Structure> createDocumentRootLDAPStructure(Structure value) {
		return new JAXBElement<Structure>(_DocumentRootLDAPStructure_QNAME, Structure.class, DocumentRoot.class, value);
	}

	/**
	 * Create an instance of {@link JAXBElement }
	 * 
	 */
	@XmlElementDecl(namespace = "", name = "objectClass", scope = Structure.class)
	public JAXBElement<ObjectClasses> createStructureObjectClass(ObjectClasses value) {
		return new JAXBElement<ObjectClasses>(_StructureObjectClass_QNAME, ObjectClasses.class, Structure.class, value);
	}

	/**
	 * Create an instance of {@link JAXBElement }
	 * 
	 */
	@XmlElementDecl(namespace = "", name = "attributeType", scope = Structure.class)
	public JAXBElement<AttributeTypes> createStructureAttributeType(AttributeTypes value) {
		return new JAXBElement<AttributeTypes>(_StructureAttributeType_QNAME, AttributeTypes.class, Structure.class,
				value);
	}

}