package net.ihe.gazelle.ldap.model;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EntityManager;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.Lob;
import javax.persistence.ManyToMany;
import javax.persistence.PrePersist;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.UniqueConstraint;

import javax.validation.constraints.NotNull;

import net.ihe.gazelle.hpd.simulator.SimulatorType;
import org.hibernate.annotations.Type;
import org.jboss.seam.Component;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.security.Identity;

@Entity
@Name("ldapPartition")
@Table(name = "ldap_partition", schema = "public", uniqueConstraints = @UniqueConstraint(columnNames = "name"))
@SequenceGenerator(name = "ldap_partition_sequence", sequenceName = "ldap_partition_id_seq", allocationSize = 1)
public class LDAPPartition implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 393432663236956461L;

	@Id
	@GeneratedValue(generator = "ldap_partition_sequence", strategy = GenerationType.SEQUENCE)
	@NotNull
	@Column(name = "id")
	private Integer id;

	@Column(name = "name")
	@NotNull
	private String name;

	@Column(name = "base_dn")
	private String baseDN;

	@Column(name = "server_ip")
	private String serverIP;

	@Column(name = "server_port")
	private Integer serverPort;

	@Column(name = "is_default")
	private Boolean isDefault;

	/**
	 * The regex against which the DN is checked when receiving a searchRequest
	 */
	@Column(name = "dn_search_regex")
	private String dnSearchRegex;

	/**
	 * The regex against which the DN is checked when receiving a addRequest, delRequest, modifyRequest or modDNRequest
	 */
	@Column(name = "dn_modify_regex")
	private String dnModifyRegex;

	@Lob
	@Type(type = "text")
	@Column(name = "description")
	private String description;

	@Temporal(TemporalType.DATE)
	@Column(name = "creation_date")
	private Date creationDate;

	@Column(name = "creator")
	private String creator;

	@ManyToMany(fetch = FetchType.LAZY, targetEntity=LDAPNode.class)
	@JoinTable(name = "ldap_partition_node", joinColumns = @JoinColumn(name = "ldap_partition_id"), inverseJoinColumns = @JoinColumn(name = "ldap_node_id"), uniqueConstraints = @UniqueConstraint(columnNames = {
			"ldap_partition_id", "ldap_node_id" }))
	private List<LDAPNode> subordinatedNodes;

	@Enumerated(EnumType.STRING)
	@Column(name = "simulator")
	private SimulatorType simulator;

	public LDAPPartition() {

	}

	public LDAPPartition(LDAPPartition inPartition) {
		this.name = inPartition.getName().concat("_COPY");
		this.baseDN = inPartition.getBaseDN();
		this.description = "Copied from" + inPartition.getName();
		this.dnModifyRegex = inPartition.getDnModifyRegex();
		this.dnSearchRegex = inPartition.getDnSearchRegex();
		this.serverIP = inPartition.getServerIP();
		this.serverPort = inPartition.getServerPort();
		this.simulator = inPartition.getSimulator();
	}

	@PrePersist
	public void onPersist() {
		if (Identity.instance().isLoggedIn()) {
			this.creator = Identity.instance().getCredentials().getUsername();
		} else {
			this.creator = null;
		}
		this.creationDate = new Date();
	}

	public LDAPPartition save() {
		EntityManager entityManager = (EntityManager) Component.getInstance("entityManager");
		LDAPPartition partition = entityManager.merge(this);
		entityManager.flush();
		return partition;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getBaseDN() {
		return baseDN;
	}

	public void setBaseDN(String baseDN) {
		this.baseDN = baseDN;
	}

	public String getServerIP() {
		return serverIP;
	}

	public void setServerIP(String serverIP) {
		this.serverIP = serverIP;
	}

	public Integer getServerPort() {
		return serverPort;
	}

	public void setServerPort(Integer serverPort) {
		this.serverPort = serverPort;
	}

	public Boolean getIsDefault() {
		return isDefault;
	}

	public void setIsDefault(Boolean isDefault) {
		this.isDefault = isDefault;
	}

	public String getDnSearchRegex() {
		return dnSearchRegex;
	}

	public void setDnSearchRegex(String dnSearchRegex) {
		this.dnSearchRegex = dnSearchRegex;
	}

	public String getDnModifyRegex() {
		return dnModifyRegex;
	}

	public void setDnModifyRegex(String dnModifyRegex) {
		this.dnModifyRegex = dnModifyRegex;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public Date getCreationDate() {
		return creationDate;
	}

	public void setCreationDate(Date creationDate) {
		this.creationDate = creationDate;
	}

	public String getCreator() {
		return creator;
	}

	public void setCreator(String creator) {
		this.creator = creator;
	}

	public List<LDAPNode> getSubordinatedNodes() {
		return subordinatedNodes;
	}

	public void setSubordinatedNodes(List<LDAPNode> subordinatedNodes) {
		this.subordinatedNodes = subordinatedNodes;
	}

	public Integer getId() {
		return id;
	}

	public SimulatorType getSimulator() {
		return simulator;
	}

	public void setSimulator(SimulatorType simulator) {
		this.simulator = simulator;
	}

	public SimulatorType[] getSimulators() {
		return SimulatorType.values();
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = (prime * result) + ((name == null) ? 0 : name.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (getClass() != obj.getClass()) {
			return false;
		}
		LDAPPartition other = (LDAPPartition) obj;
		if (name == null) {
			if (other.name != null) {
				return false;
			}
		} else if (!name.equals(other.name)) {
			return false;
		}
		return true;
	}

}
