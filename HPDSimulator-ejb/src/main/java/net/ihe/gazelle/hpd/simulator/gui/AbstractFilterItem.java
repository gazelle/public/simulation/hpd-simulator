package net.ihe.gazelle.hpd.simulator.gui;

import net.ihe.gazelle.hpd.AttributeDescription;
import net.ihe.gazelle.hpd.AttributeValueAssertion;
import net.ihe.gazelle.hpd.Filter;
import net.ihe.gazelle.hpd.FilterSet;
import net.ihe.gazelle.hpd.MatchingRuleAssertion;
import net.ihe.gazelle.hpd.SubstringFilter;
import net.ihe.gazelle.hpd.simulator.providerinfoconsumer.FilterType;

import java.io.Serializable;

/**
 * Created by aberge on 23/06/17.
 */
public abstract class AbstractFilterItem implements Serializable{

    private static Integer globalIndex = 0;

    private boolean includeInNot;
    private Integer index;

    private FilterType filterType;

    public AbstractFilterItem(){
        this.includeInNot = false;
        synchronized (AbstractFilterItem.class) {
            this.index = globalIndex++;
        }
    }

    public Integer getIndex() {
        return index;
    }

    public boolean isIncludeInNot() {
        return includeInNot;
    }

    public void setIncludeInNot(boolean includeInNot) {
        this.includeInNot = includeInNot;
    }

    public FilterType getFilterType() {
        return filterType;
    }

    protected void setFilterType(FilterType inType){
        this.filterType = inType;
    }

    public abstract String getIncludeLink();

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof AbstractFilterItem)) {
            return false;
        }

        AbstractFilterItem that = (AbstractFilterItem) o;

        return index != null ? index.equals(that.index) : that.index == null;
    }

    @Override
    public int hashCode() {
        return index != null ? index.hashCode() : 0;
    }

    public abstract Serializable getFilterItem();

    public Filter createNotFilter() {
        Filter notFilter = new Filter();
        appendToFilter(notFilter);
        return notFilter;
    }

    public void appendToFilter(Filter filter) {
        Serializable filterItem = getFilterItem();
        switch (this.filterType){
            case OR:
                filter.setOr((FilterSet) filterItem);
                break;
            case AND:
                filter.setAnd((FilterSet) filterItem);
                break;
            case NOT:
                filter.setNot((Filter) filterItem);
                break;
            case SUBSTRING:
                filter.setSubstrings((SubstringFilter) filterItem);
                break;
            case EQUALITY_MATCH:
                filter.setEqualityMatch((AttributeValueAssertion) filterItem);
                break;
            case GREATER_OR_EQUAL:
                filter.setGreaterOrEqual((AttributeValueAssertion) filterItem);
                break;
            case LESS_OR_EQUAL:
                filter.setLessOrEqual((AttributeValueAssertion) filterItem);
                break;
            case PRESENT:
                filter.setPresent((AttributeDescription) filterItem);
                break;
            case APPROX_MATCH:
                filter.setApproxMatch((AttributeValueAssertion) filterItem);
                break;
            case EXTENSIBLE_MATCH:
                filter.setExtensibleMatch((MatchingRuleAssertion) filterItem);
                break;
        }
    }
}
