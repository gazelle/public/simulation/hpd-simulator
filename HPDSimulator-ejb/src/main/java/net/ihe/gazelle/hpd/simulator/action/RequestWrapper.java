package net.ihe.gazelle.hpd.simulator.action;

import net.ihe.gazelle.hpd.AddRequest;
import net.ihe.gazelle.hpd.AttributeDescription;
import net.ihe.gazelle.hpd.AttributeDescriptions;
import net.ihe.gazelle.hpd.DelRequest;
import net.ihe.gazelle.hpd.DerefAliasesType;
import net.ihe.gazelle.hpd.DsmlAttr;
import net.ihe.gazelle.hpd.DsmlModification;
import net.ihe.gazelle.hpd.Filter;
import net.ihe.gazelle.hpd.ModifyDNRequest;
import net.ihe.gazelle.hpd.ModifyRequest;
import net.ihe.gazelle.hpd.SearchRequest;
import net.ihe.gazelle.hpd.gui.DsmlAttrValue;
import net.ihe.gazelle.hpd.simulator.gui.AbstractFilterItem;
import net.ihe.gazelle.hql.providers.EntityManagerService;
import net.ihe.gazelle.ldap.model.AttributeTypes;
import net.ihe.gazelle.ldap.model.AttributeTypesQuery;
import net.ihe.gazelle.ldap.model.LDAPNode;
import net.ihe.gazelle.ldap.model.ObjectClasses;
import net.ihe.gazelle.ldap.model.ObjectClassesQuery;
import org.jboss.seam.faces.FacesMessages;
import org.jboss.seam.international.StatusMessage;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

public class RequestWrapper implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = -6814906437050756038L;
    private static final String SELECTED = "gzl-btn-green";
    private static final String UNSELECTED = "gzl-btn";

    private RequestType requestType;
    private String dn;
    private Integer index;
    private Serializable request;
    private LDAPNode selectedNode;
    private List<String> objectClassList;
    private List<String> availableAttributeTypes;
    private List<String> requiredAttributes;
    private DsmlAttr dsmlAttr;
    private DsmlModification dsmlModification;
    private String value;
    private Map<String, String> requestedAttributes;
    private AbstractFilterItem searchFilter;

    public RequestWrapper(Integer inIndex, RequestType inRequestType) {
        this.index = inIndex;
        this.requestType = inRequestType;
        switch (requestType) {
            case ADD:
                this.request = new AddRequest();
                dsmlAttr = new DsmlAttr();
                break;
            case DEL:
                this.request = new DelRequest();
                break;
            case MODIFY:
                this.request = new ModifyRequest();
                dsmlModification = new DsmlModification();
                break;
            case MODDN:
                this.request = new ModifyDNRequest();
                break;
            case SEARCH:
                this.request = new SearchRequest();
                // put default values to avoid missing required values (blocks the other parts of the form)
                ((SearchRequest) this.request).setDerefAliases(DerefAliasesType.NEVERDEREFALIASES);
                ((SearchRequest) this.request).setScope(net.ihe.gazelle.hpd.ScopeType.WHOLESUBTREE);
                ((SearchRequest) this.request).setFilter(new Filter());
                break;
            default:
                break;
        }
        this.selectedNode = null;
    }

    /**
     * Method isAttributeSingleValue.
     *
     * @param attributeName String
     *
     * @return boolean
     */
    public boolean isAttributeSingleValue(String attributeName) {
        AttributeTypesQuery query = new AttributeTypesQuery();
        query.name().eq(attributeName);
        AttributeTypes attribute = query.getUniqueResult();
        if (attribute != null) {
            return attribute.getSinglevalue();
        } else {
            return false;
        }
    }

    public void initializeListOfRequestedAttributes() {
        requestedAttributes = new HashMap<String, String>();
        if (availableAttributeTypes != null && !availableAttributeTypes.isEmpty()) {
            for (String attr : availableAttributeTypes) {
                requestedAttributes.put(attr, UNSELECTED);
            }
        }
    }

    public void populateAddWithRequiredAttributes() {
        for (String att : requiredAttributes) {
            if (att.equals("objectClass")) {
                DsmlAttr attribute = new DsmlAttr(att);
                for (String clazz : getObjectClassList()) {
                    attribute.addValueForGui(clazz);
                }
                ((AddRequest) request).getAttr().add(attribute);
            } else {
                ((AddRequest) request).getAttr().add(new DsmlAttr(att, "required attribute"));
            }
        }
    }

    public RequestType getRequestType() {
        return requestType;
    }

    public void setRequestType(RequestType requestType) {
        this.requestType = requestType;
    }

    public Integer getIndex() {
        return index;
    }

    public void setIndex(Integer index) {
        this.index = index;
    }

    public Serializable getRequest() {
        return request;
    }

    public void setRequest(Serializable request) {
        this.request = request;
    }

    public String getDn() {
        return dn;
    }

    public void setDn(String dn) {
        this.dn = dn;
    }

    public String getIncludeForEdit() {
        return this.requestType.getEditPage();
    }

    /**
     * Method isDsmlAttributeRequired.
     *
     * @param attr DsmlAttr
     *
     * @return boolean
     */
    public boolean isDsmlAttributeRequired(DsmlAttr attr) {
        if ((requiredAttributes == null) || requiredAttributes.isEmpty()) {
            return false;
        } else if ((attr != null) && requiredAttributes.contains(attr.getName())) {
            return true;
        } else {
            return false;
        }
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = (prime * result) + ((index == null) ? 0 : index.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        RequestWrapper other = (RequestWrapper) obj;
        if (index == null) {
            if (other.index != null) {
                return false;
            }
        } else if (!index.equals(other.index)) {
            return false;
        }
        return true;
    }

    public LDAPNode getSelectedNode() {
        return selectedNode;
    }

    public void setSelectedNode(LDAPNode selectedNode) {
        this.selectedNode = selectedNode;
    }

    /**
     * Method listAttributes.
     *
     * @param must       Boolean if true return required attributes, if false return optional attribute, if null return all
     * @param clazz      ObjectClasses
     * @param attributes List<String>
     *
     * @return List<String>
     */
    protected List<String> listAttributes(Boolean must, ObjectClasses clazz, List<String> attributes) {
        if (objectClassList == null) {
            objectClassList = new ArrayList<String>();
        }
        if (!objectClassList.contains(clazz.getName())) {
            objectClassList.add(clazz.getName());
        }
        List<String> attributesToAdd = new ArrayList<String>();
        List<String> mustAttributes = clazz.getMust();
        List<String> mayAttributes = clazz.getMay();
        if ((must == null || must) && mustAttributes != null) {
            attributesToAdd.addAll(mustAttributes);
        }
        if ((must == null || !must) && mayAttributes != null) {
            attributesToAdd.addAll(clazz.getMay());
        }
        // avoid duplication of attributes in the final list
        if (!attributesToAdd.isEmpty()) {
            for (String at : attributesToAdd) {
                if (attributes != null && !attributes.contains(at)) {
                    attributes.add(at);
                }
            }
        }
        if ((clazz.getSupobjectclass() != null) && !clazz.getSupobjectclass().isEmpty()) {
            ObjectClassesQuery query = new ObjectClassesQuery();
            query.name().eq(clazz.getSupobjectclass());
            ObjectClasses supClass = query.getUniqueResult();
            if (supClass != null) {
                attributes = listAttributes(must, supClass, attributes);
            }
        }
        return attributes;
    }

    /**
     * Method getObjectClassList.
     *
     * @return List<String>
     */
    public List<String> getObjectClassList() {
        return objectClassList;
    }

    /**
     * Method getAvailableAttributeTypes.
     *
     * @return List<String>
     */
    public List<String> getAvailableAttributeTypes() {
        return availableAttributeTypes;
    }

    /**
     * Field IGNORE_CASE_COMPARATOR.
     */
    private static final Comparator<String> IGNORE_CASE_COMPARATOR = new Comparator<String>() {
        @Override
        public int compare(String o1, String o2) {
            if (o1 == null) {
                return 1;
            } else if (o2 == null) {
                return -1;
            } else {
                return o1.compareToIgnoreCase(o2);
            }
        }
    };

    /**
     * Select all the attributes (or only must, or only may) defined for the selected node
     *
     * @param must if true lists only 'must' attributes, if false lists only 'may' attributes, if null lists all attributes
     *
     * @return List<String>
     */
    private List<String> listAttributes(Boolean must) {
        List<String> attributes = new ArrayList<String>();
        LDAPNode node = EntityManagerService.provideEntityManager().find(LDAPNode.class, selectedNode.getId());
        for (ObjectClasses clazz : node.getObjectClasses()) {
            attributes = listAttributes(must, clazz, attributes);
        }
        Collections.sort(attributes, IGNORE_CASE_COMPARATOR);
        return attributes;
    }

    public void listRequiredAttributes() {
        requiredAttributes = listAttributes(true);
    }

    public void listAllAvailableAttributes() {
        availableAttributeTypes = listAttributes(null);
    }

    /**
     * Method removeAttributeFromAddRequest.
     *
     * @param attr DsmlAttr
     */
    public void removeAttributeFromAddRequest(DsmlAttr attr) {
        ((AddRequest) request).getAttr().remove(attr);
    }

    /**
     * Method addAttributeToAddRequest.
     */
    public void addAttributeToAddRequest() {
        if ((dsmlAttr.getName() != null) && !dsmlAttr.getName().isEmpty()) {
            if ((value != null) && !value.isEmpty()) {
                dsmlAttr.addValueForGui(value);
                this.value = null;
            }
            ((AddRequest) request).getAttr().add(dsmlAttr);
            dsmlAttr = new DsmlAttr();
        } else {
            FacesMessages.instance().add(StatusMessage.Severity.ERROR, "Cannot add a DsmlAttr element without name attribute");
        }
    }


    public DsmlAttr getDsmlAttr() {
        return dsmlAttr;
    }


    /**
     * Method removeDsmlModification.
     *
     * @param inModification DsmlModification
     */
    public void removeDsmlModification(DsmlModification inModification) {
        ((ModifyRequest) request).getModification().remove(inModification);
    }

    /**
     * Method addDsmlModification.
     */
    public void addDsmlModification() {
        if ((dsmlModification.getName() != null) && !dsmlModification.getName().isEmpty()
                && (dsmlModification.getOperation() != null)) {
            if ((value != null) && !value.isEmpty()) {
                dsmlModification.addValueForGui(value);
            }
            ((ModifyRequest) request).getModification().add(dsmlModification);
            this.value = null;
            this.dsmlModification = new DsmlModification();
        } else {
            FacesMessages.instance().add(StatusMessage.Severity.ERROR, "Cannot add a DsmlModification element without name " +
                    "AND operation attributes");
        }
    }

    public List<String> getRequiredAttributes() {
        return requiredAttributes;
    }

    public DsmlModification getDsmlModification() {
        return dsmlModification;
    }

    public String getValue() {
        return value;
    }

    public void setDsmlAttr(DsmlAttr dsmlAttr) {
        this.dsmlAttr = dsmlAttr;
    }

    public void setDsmlModification(DsmlModification dsmlModification) {
        this.dsmlModification = dsmlModification;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public List<String> getRequestedAttributes() {
        if (requestedAttributes != null) {
            String[] keys = requestedAttributes.keySet().toArray(new String[requestedAttributes.keySet().size()]);
            List<String> orderedList = Arrays.asList(keys);
            Collections.sort(orderedList, IGNORE_CASE_COMPARATOR);
            return orderedList;
        } else {
            return null;
        }
    }

    public void changeAttributeState(String attributeName) {
        if (requestedAttributes == null) {
            requestedAttributes = new HashMap<String, String>();
        }
        String state = requestedAttributes.get(attributeName);
        SearchRequest searchRequest = (SearchRequest) request;
        if (searchRequest.getAttributes() == null) {
            searchRequest.setAttributes(new AttributeDescriptions());
        }
        if (state == null || UNSELECTED.equals(state)) {
            requestedAttributes.put(attributeName, SELECTED);
            // new state is "selected" add the attribute to the list of attributes to be returned
            AttributeDescription attribute = new AttributeDescription();
            attribute.setName(attributeName);
            searchRequest.getAttributes().addAttribute(attribute);
        } else {
            requestedAttributes.put(attributeName, UNSELECTED);
            // new state is "unselected" remove the attribute from the list of attributes to be returned
            for (AttributeDescription attrDescription : searchRequest.getAttributes().getAttribute()) {
                if (attributeName.equals(attrDescription.getName())) {
                    searchRequest.getAttributes().removeGroup(attrDescription);
                    break;
                }
            }
        }
    }

    public String getClassForAttribute(String attributeName) {
        if (requestedAttributes != null) {
            return requestedAttributes.get(attributeName);
        } else {
            return UNSELECTED;
        }
    }

    public AbstractFilterItem getSearchFilter() {
        return searchFilter;
    }

    public void setSearchFilter(AbstractFilterItem searchFilter) {
        this.searchFilter = searchFilter;
    }

    public void createFilterFromGUIFilter() {
        if (searchFilter != null) {
            SearchRequest searchRequest = (SearchRequest) request;
            searchRequest.setFilter(new Filter());
            if (searchFilter.isIncludeInNot()) {
                Filter notFilter = searchFilter.createNotFilter();
                searchRequest.getFilter().setNot(notFilter);
            } else {
                searchFilter.appendToFilter(searchRequest.getFilter());
            }
        }
    }

    public void addMayAttributeToAddRequest() {
        List<String> mayAttributes = listAttributes(false);
        if (!mayAttributes.isEmpty()) {
            AddRequest addRequest = (AddRequest) request;
            for (String attr : mayAttributes) {
                DsmlAttr dsmlAttr = new DsmlAttr(attr, "");
                addRequest.addAttr(dsmlAttr);
            }
        }
    }

    public void removeEmptyAttributesFromAddRequest() {
        AddRequest addRequest = (AddRequest) request;
        Iterator<DsmlAttr> iterator = addRequest.getAttr().iterator();
        while (iterator.hasNext()) {
            DsmlAttr attr = iterator.next();
            boolean valued = false;
            for (DsmlAttrValue value : attr.getValuesForGui()) {
                if (!value.getGuiValue().isEmpty()) {
                    valued = true;
                    break;
                }
            }
            if (!valued) {
                iterator.remove();
            }
        }
    }
}
