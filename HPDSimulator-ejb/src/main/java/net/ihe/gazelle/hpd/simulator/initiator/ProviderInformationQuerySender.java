package net.ihe.gazelle.hpd.simulator.initiator;

import net.ihe.gazelle.hpd.utils.HPDSoapConstants;
import net.ihe.gazelle.hpd.validator.ValidatorType;
import net.ihe.gazelle.simulator.common.tf.model.Actor;
import net.ihe.gazelle.simulator.common.tf.model.Transaction;
import net.ihe.gazelle.simulator.sut.model.SystemConfiguration;

public class ProviderInformationQuerySender extends SoapHPDClient {

    public ProviderInformationQuerySender(SystemConfiguration sut) {
        super(sut);
    }

    @Override
    protected String getAction(){
        return HPDSoapConstants.HPD_QUERY_ACTION;
    }

    @Override
    protected String getRequestType() {
        if (ITI.equals(domainKeyword)) {
            return ValidatorType.QUERY_REQUEST.getName();
        } else {
            return ValidatorType.CH_QUERY_REQUEST.getName();
        }
    }

    @Override
    protected String getResponseType() {
        if (ITI.equals(domainKeyword)) {
            return ValidatorType.QUERY_RESPONSE.getName();
        } else {
            return ValidatorType.CH_QUERY_RESPONSE.getName();
        }
    }


    @Override
    protected Actor getSimulatedActor() {
        return Actor.findActorWithKeyword("PROV_INFO_CONS");
    }

    @Override
    protected Transaction getSimulatedTransaction() {
        return Transaction.GetTransactionByKeyword("ITI-58");
    }

}
