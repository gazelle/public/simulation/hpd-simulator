package net.ihe.gazelle.hpd.simulator.providerinfodirectory;

import net.ihe.gazelle.common.interfacegenerator.GenerateInterface;
import net.ihe.gazelle.hpd.BatchRequest;
import net.ihe.gazelle.hpd.BatchResponse;
import net.ihe.gazelle.hpd.utils.HPDSoapConstants;
import net.ihe.gazelle.simulator.common.tf.model.Actor;
import net.ihe.gazelle.simulator.common.tf.model.Transaction;
import org.jboss.seam.annotations.Name;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.annotation.Resource;
import javax.ejb.Stateless;
import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebResult;
import javax.jws.WebService;
import javax.jws.soap.SOAPBinding;
import javax.servlet.http.HttpServletRequest;
import javax.xml.ws.Action;
import javax.xml.ws.BindingType;
import javax.xml.ws.RespectBinding;
import javax.xml.ws.WebServiceContext;
import javax.xml.ws.handler.MessageContext;
import javax.xml.ws.soap.Addressing;

@Stateless
@Name("providerInformationDirectoryService")
@WebService(portName = HPDSoapConstants.HPD_PORT_NAME, name = HPDSoapConstants.HPD_PORT_TYPE, targetNamespace =
        HPDSoapConstants.HPD_TARGET_NAMESPACE, serviceName = HPDSoapConstants.HPD_SERVICE_NAME)
// Unwrap parameters
@SOAPBinding(parameterStyle = SOAPBinding.ParameterStyle.BARE)
// Adds header in response and allows server to process mustUnderstand
@Addressing(enabled = true, required = true)
// Force SOAP 1.2
@BindingType(javax.xml.ws.soap.SOAPBinding.SOAP12HTTP_BINDING)
@RespectBinding(enabled = true)
@GenerateInterface(value = "ProviderInformationDirectoryServiceRemote", isLocal = false, isRemote = true)
public class ProviderInformationDirectoryService implements ProviderInformationDirectoryServiceRemote {

    // SOAP Context
    @Resource
    private WebServiceContext context;


    /**
     * ITI-58
     *
     * @param request : batchRequest received from the SUT
     *
     * @return the batchResponse
     */
    @Override
    @WebMethod(operationName = "ProviderInformationQueryRequest", action = HPDSoapConstants.HPD_QUERY_ACTION)
    @WebResult(name = "batchResponse", partName = "body", targetNamespace = HPDSoapConstants.DSML_NAMESPACE)
    @Action(input = HPDSoapConstants.HPD_QUERY_ACTION, output = HPDSoapConstants.HPD_QUERY_RESPONSE_ACTION)
    public BatchResponse ProviderInformationQueryRequest(
            @WebParam(name = "batchRequest", partName = "body", targetNamespace = HPDSoapConstants.DSML_NAMESPACE) BatchRequest request) {
        ProviderInformationDirectory directory = new ProviderInformationDirectory(
                Transaction.GetTransactionByKeyword("ITI-58"), Actor.findActorWithKeyword("PROV_INFO_CONS"), getRemoteHostFromContext());
        return directory.handleRequest(request);
    }

    /**
     * ITI-59
     *
     * @param request : batchRequest received from the SUT
     *
     * @return the batchResponse
     */
    @Override
    @WebMethod(operationName = "ProviderInformationFeedRequest", action = HPDSoapConstants.HPD_FEED_ACTION)
    @WebResult(name = "batchResponse", partName = "body", targetNamespace = HPDSoapConstants.DSML_NAMESPACE)
    @Action(input = HPDSoapConstants.HPD_FEED_ACTION, output = HPDSoapConstants.HPD_FEED_RESPONSE_ACTION)
    public BatchResponse ProviderInformationFeedRequest(
            @WebParam(name = "batchRequest", partName = "body", targetNamespace = HPDSoapConstants.DSML_NAMESPACE) BatchRequest request) {
        ProviderInformationDirectory directory = new ProviderInformationDirectory(
                Transaction.GetTransactionByKeyword("ITI-59"), Actor.findActorWithKeyword("PROV_INFO_SRC"), getRemoteHostFromContext());
        return directory.handleRequest(request);
    }

    private String getRemoteHostFromContext() {
        HttpServletRequest hRequest = (HttpServletRequest) context.getMessageContext()
                .get(MessageContext.SERVLET_REQUEST);
        return hRequest.getRemoteAddr();
    }

}
