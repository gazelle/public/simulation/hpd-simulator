package net.ihe.gazelle.hpd.ws;

import java.io.Serializable;
import java.util.List;

import javax.ejb.Stateless;
import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import javax.xml.soap.SOAPException;

import net.ihe.gazelle.simulator.common.action.AbstractSimulatorManager;
import net.ihe.gazelle.simulator.common.action.ResultSendMessage;
import net.ihe.gazelle.simulator.common.action.SimulatorManagerRemote;
import net.ihe.gazelle.simulator.common.model.ConfigurationForWS;
import net.ihe.gazelle.simulator.common.model.ContextualInformationInstance;
import net.ihe.gazelle.simulator.common.tf.model.Transaction;

import org.jboss.seam.annotations.Logger;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.log.Log;

@Stateless
@Name("SimulatorManagerWS")
@WebService(name = "GazelleSimulatorManagerWS", serviceName = "GazelleSimulatorManagerWSService", portName = "GazelleSimulatorManagerWSPort")
public class SimulatorWS extends AbstractSimulatorManager implements SimulatorManagerRemote, Serializable {

	/** Logger */
	@Logger
	private static Log log;

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Override
	@WebMethod
	public boolean startTestInstance(@WebParam(name = "testInstanceId") String testInstanceId) {
		log.info("Simulator:::startTestInstance");
		try {
			return super.startTestInstance(testInstanceId);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return false;
	}

	@Override
	@WebMethod
	public boolean stopTestInstance(@WebParam(name = "testInstanceId") String testInstanceId) {
		try {
			if (null == testInstanceId){
				testInstanceId = "";
			}
			return super.stopTestInstance(testInstanceId);
		} catch (SOAPException e) {
			e.printStackTrace();
			return false;
		}
	}

	@Override
	@WebMethod
	public boolean deleteTestInstance(@WebParam(name = "testInstanceId") String testInstanceId) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	@WebMethod
	public String confirmMessageReception(@WebParam(name = "testInstanceId") String testInstanceId,
			@WebParam(name = "testInstanceParticipantsId") String testInstanceParticipantsId,
			@WebParam(name = "transaction") Transaction transaction, @WebParam(name = "messageType") String messageType) {
		log.info("Simulator::confirmMessageReception()");
		return null;
	}

	@Override
	@WebMethod
	public ResultSendMessage sendMessage(
			@WebParam(name = "testInstanceId") String testInstanceId,
			@WebParam(name = "testInstanceParticipantsId") String testInstanceParticipantsId,
			@WebParam(name = "transaction") Transaction transaction,
			@WebParam(name = "messageType") String messageType,
			@WebParam(name = "responderConfiguration") ConfigurationForWS responderConfiguration,
			@WebParam(name = "listContextualInformationInstanceInput") List<ContextualInformationInstance> listContextualInformationInstanceInput,
			@WebParam(name = "listContextualInformationInstanceOutput") List<ContextualInformationInstance> listContextualInformationInstanceOutput) {
		log.info("Simu::sendMessage()");
		log.info("testInstanceId==" + testInstanceId);
		log.info("testInstanceParticipantsId==" + testInstanceParticipantsId);
		log.info("transaction==" + transaction.getKeyword());
		this.saveRelatedTestStepsInstance(testInstanceId, testInstanceParticipantsId, transaction, messageType,
				responderConfiguration, listContextualInformationInstanceInput, listContextualInformationInstanceOutput);
		return null;
	}

}
