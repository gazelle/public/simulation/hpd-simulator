package net.ihe.gazelle.hpd.simulator.messages;

import net.ihe.gazelle.common.util.XmlFormatter;
import net.ihe.gazelle.hpd.utils.SAXParserFactoryProvider;
import net.ihe.gazelle.hpd.validator.GazelleHPDValidator;
import net.ihe.gazelle.hpd.validator.ValidatorType;
import net.ihe.gazelle.simulator.common.model.ApplicationConfiguration;
import net.ihe.gazelle.simulator.message.action.AbstractTransactionInstanceDisplay;
import net.ihe.gazelle.simulator.message.model.MessageInstance;
import net.ihe.gazelle.simulator.message.model.TransactionInstance;
import net.ihe.gazelle.simulator.statistics.ValidatorUsage;
import net.ihe.gazelle.simulator.ws.TestReport;
import net.ihe.gazelle.validation.DetailedResult;
import net.ihe.gazelle.validator.hpd.util.XMLValidation;
import org.jboss.seam.Component;
import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.Create;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Scope;
import org.jboss.seam.faces.FacesMessages;
import org.jboss.seam.international.StatusMessage;

import javax.persistence.EntityManager;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Source;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.stream.StreamResult;
import javax.xml.transform.stream.StreamSource;
import java.io.ByteArrayOutputStream;
import java.io.StringReader;
import java.util.Date;

@Name("hpdMessageBrowser")
@Scope(ScopeType.PAGE)
public class HPDMessageBrowser extends AbstractTransactionInstanceDisplay {

    /**
     *
     */
    private static final long serialVersionUID = 236053079070523657L;

    private String selectedTab;
    private String xslPathForResults;
    private String xslPathForDSML;

    @Create
    public void onCreate() {
        super.getTransactionInstanceFromUrl();
        xslPathForResults = ApplicationConfiguration.getValueOfVariable("results_xsl_location");
        xslPathForDSML = ApplicationConfiguration.getValueOfVariable("dsml_xsl_location");
    }

    @Override
    public String getPrettyFormattedResult(MessageInstance messageInstance) {
        // If the transaction have a DetailedResult
        if (messageInstance.getValidationDetailedResult() != null) {
            // We get the xsl path from database
            if (xslPathForResults == null) {
                // if database variable is not set we inform user and return the DetailedResult as String
                FacesMessages.instance().add(StatusMessage.Severity.ERROR, "results_xsl_location variable is missing in app_configuration.");
                return new String(messageInstance.getValidationDetailedResult());
            } else {
                // else we transform the DetailedResult with the xsl stylesheet
                return xmlToHtml(new String(messageInstance.getValidationDetailedResult()), xslPathForResults);
            }
        } else {
            return null;
        }
    }

    @Override
    public void validateResponse() {
        if (selectedInstance != null) {
            GazelleHPDValidator validator = null;
            if (selectedInstance.getResponse().getType().equals(ValidatorType.CH_CIDD_RESPONSE.getName())) {
                validator = initializeCIDDValidator();
            } else {
                validator = initializeHPDValidator();
            }
            // validate response
            if ((selectedInstance != null) && (selectedInstance.getResponse() != null) && (selectedInstance.getResponse().getContent() != null)) {
                ValidatorType validatorType = ValidatorType.getValidatorTypeByLabel(selectedInstance.getResponse().getType());
                if (validatorType != null) {
                    DetailedResult responseValidationResult = validator.validate(selectedInstance.getResponse().getContentAsString(),
                            validatorType);
                    selectedInstance.getResponse().setValidationStatus(
                            responseValidationResult.getValidationResultsOverview().getValidationTestResult());
                    selectedInstance.getResponse().setValidationDetailedResult(
                            GazelleHPDValidator.getDetailedResultAsString(responseValidationResult).getBytes());
                    ValidatorUsage usage = new ValidatorUsage(new Date(), responseValidationResult
                            .getValidationResultsOverview().getValidationTestResult(), validatorType.getName(),
                            "SIMULATOR");
                    usage.save();
                    saveTransactionInstance(selectedInstance);
                }
            }
        }
    }

    @Override
    public void validateRequest() {
        if (selectedInstance != null) {
            GazelleHPDValidator validator = null;
            if (selectedInstance.getRequest().getType().equals(ValidatorType.CH_CIDD_REQUEST.getName())) {
                validator = initializeCIDDValidator();
            } else {
                validator = initializeHPDValidator();
            }
            // validate request
            if ((selectedInstance != null) && (selectedInstance.getRequest() != null) && (selectedInstance.getRequest().getContent() != null)) {
                ValidatorType validatorType = ValidatorType.getValidatorTypeByLabel(selectedInstance.getRequest().getType());
                if (validatorType != null) {
                    DetailedResult requestValidationResult = validator.validate(selectedInstance.getRequest().getContentAsString(),
                            validatorType);
                    selectedInstance.getRequest().setValidationStatus(
                            requestValidationResult.getValidationResultsOverview().getValidationTestResult());
                    selectedInstance.getRequest().setValidationDetailedResult(
                            GazelleHPDValidator.getDetailedResultAsString(requestValidationResult).getBytes());
                    ValidatorUsage usage = new ValidatorUsage(new Date(), requestValidationResult
                            .getValidationResultsOverview().getValidationTestResult(), validatorType.getName(),
                            "SIMULATOR");
                    usage.save();
                }
            }
        }
    }

    @Override
    public void validateResponse(TransactionInstance transactionInstance) {
        this.selectedInstance = transactionInstance;
        validateResponse();
    }

    private void saveTransactionInstance(TransactionInstance transactionInstance) {
        if (transactionInstance != null) {
            EntityManager entityManager = (EntityManager) Component.getInstance("entityManager");
            transactionInstance.save(entityManager);
        }
    }

    @Override
    public void validateRequest(TransactionInstance transactionInstance) {
        this.selectedInstance = transactionInstance;
        validateRequest();
    }

    @Override
    public void replayMessage() {

    }

    public String displayFormattedXML(MessageInstance instance) {
        if ((instance != null) && (instance.getContent() != null)) {
            try {
                return XmlFormatter.format(instance.getContentAsString());
            } catch (Exception e) {
                return instance.getContentAsString();
            }
        } else {
            return null;
        }
    }

    public String displayFormattedHTML(MessageInstance instance) {
        if ((instance != null) && (instance.getContent() != null)) {
            return xmlToHtml(instance.getContentAsString(), xslPathForDSML);
        } else {
            return null;
        }
    }

    private String xmlToHtml(String xml, String xslPath) {
        if ((xml == null) || xml.isEmpty()) {
            return null;
        } else {
            try {
                if ((xslPath == null) || xslPath.isEmpty()) {
                    return null;
                }

                Source xmlInput = new StreamSource(new StringReader(xml));
                TransformerFactory tFactory = TransformerFactory.newInstance();
                Transformer transformer = tFactory.newTransformer(new javax.xml.transform.stream.StreamSource(xslPath));
                transformer.setOutputProperty(OutputKeys.INDENT, "yes");
                ByteArrayOutputStream baos = new ByteArrayOutputStream();
                StreamResult out = new StreamResult(baos);
                transformer.transform(xmlInput, out);
                String tmp = new String(baos.toByteArray());
                return tmp;
            } catch (Exception e) {
                e.printStackTrace();
                return "The document cannot be displayed using this stylesheet";
            }
        }
    }

    @Override
    public String permanentLinkToTestReport() {
        return TestReport.buildTestReportRestUrl(selectedInstance.getId(), null);
    }

    @Override
    public void validate(TransactionInstance instance) {
        if (instance != null) {
            validateRequest(instance);
            validateResponse(instance);
        }
    }

    private GazelleHPDValidator initializeHPDValidator() {
        return new GazelleHPDValidator(SAXParserFactoryProvider.getHpdFactory(),
                SAXParserFactoryProvider.getXsd());
    }

    private GazelleHPDValidator initializeCIDDValidator() {
        String xsd = ApplicationConfiguration.getValueOfVariable("xsd_location_CIDD");
        return new GazelleHPDValidator(XMLValidation.initializeFactory(xsd), xsd);
    }

    public String getSelectedTab() {
        return selectedTab;
    }

    public void setSelectedTab(String selectedTab) {
        this.selectedTab = selectedTab;
    }
}
