package net.ihe.gazelle.hpd.validator;

import net.ihe.gazelle.chhpd.pidd.model.DownloadRequest;
import net.ihe.gazelle.chhpd.pidd.model.DownloadResponse;
import net.ihe.gazelle.hpd.BatchRequest;
import net.ihe.gazelle.hpd.BatchResponse;
import net.ihe.gazelle.validation.model.ValidatorDescription;

public enum ValidatorType implements ValidatorDescription {

    BATCH_REQUEST("DSMLv2 Batch Request", "IHE", BatchRequest.class, ProfileType.HPD, "batchRequest"),
    BATCH_RESPONSE("DSMLv2 Batch Response", "IHE", BatchResponse.class, ProfileType.HPD, "batchResponse"),
    FEED_REQUEST("Provider Information Feed Request", "IHE", BatchRequest.class, ProfileType.HPD, "batchRequest"),
    FEED_RESPONSE("Provider Information Feed Response", "IHE", BatchResponse.class, ProfileType.HPD, "batchResponse"),
    QUERY_REQUEST("Provider Information Query Request", "IHE", BatchRequest.class, ProfileType.HPD, "batchRequest"),
    QUERY_RESPONSE("Provider Information Query Response", "IHE", BatchResponse.class, ProfileType.HPD, "batchResponse"),
    KSA_QUERY_REQUEST("KSA - Provider Information Query Request", "KSA", BatchRequest.class, ProfileType.HPD, "batchRequest"),
    KSA_QUERY_RESPONSE("KSA - Provider Information Query Response", "KSA", BatchResponse.class, ProfileType.HPD, "batchResponse"),
    CH_FEED_REQUEST("CH - Provider Information Feed Request", "CH", BatchRequest.class, ProfileType.HPD, "batchRequest"),
    CH_FEED_RESPONSE("CH - Provider Information Feed Response", "CH", BatchResponse.class, ProfileType.HPD, "batchResponse"),
    CH_QUERY_REQUEST("CH - Provider Information Query Request", "CH", BatchRequest.class, ProfileType.HPD, "batchRequest"),
    CH_QUERY_RESPONSE("CH - Provider Information Query Response", "CH", BatchResponse.class, ProfileType.HPD, "batchResponse"),
    CH_PIDD_REQUEST("CH - Provider Information Delta Download Request", "CH", DownloadRequest.class, ProfileType.CH_PIDD, "downloadRequest"),
    CH_PIDD_RESPONSE("CH - Provider Information Delta Download Response", "CH", DownloadResponse.class, ProfileType.CH_PIDD, "downloadResponse"),
    CH_CPI_REQUEST("CH - Community Portal Index Request", "CH", BatchRequest.class, ProfileType.HPD, "batchRequest"),
    //CH_CPI_RESPONSE("CH - Community Portal Index Response", "CH", BatchRequest.class, ProfileType.HPD, "batchRequest"),
    CH_CIDD_REQUEST("CH - Community Information Delta Download Request", "CH", net.ihe.gazelle.chcpi.model.DownloadRequest.class, ProfileType.CH_CIDD, "downloadRequest"),
    CH_CIDD_RESPONSE("CH - Community Information Delta Download Response", "CH", net.ihe.gazelle.chcpi.model.DownloadResponse.class, ProfileType.CH_CIDD, "downloadResponse");


    String name;
    String descriminator;
    Class<?> typeClass;
    ProfileType profileType;
    String rootElement;
    String oid;

    @Override
    public String getOid() {
        // FIXME : assign OIDs to the validators
        return oid;
    }

    @Override
    public String getRootElement() {
        return rootElement;
    }

    @Override
    public String getNamespaceURI() {
        if (profileType != null && profileType.equals(ProfileType.CH_PIDD)) {
            return "urn:ehealth-suisse:names:tc:CS:1";
        } else if (profileType != null && profileType.equals(ProfileType.CH_CIDD)) {
            return "urn:ch:admin:bag:epr:2017";
        } else {
            return "urn:oasis:names:tc:DSML:2:0:core";
        }
    }

    public Class<?> getTypeClass() {
        return typeClass;
    }

    @Override
    public String getName() {
        return name;
    }

    public String getDescriminator() {
        return descriminator;
    }

    public ProfileType getProfileType() {
        return profileType;
    }

    @Override
    public boolean extractPartToValidate() {
        return true;
    }

    ValidatorType(String inName, String descriminator, Class<?> clazz, ProfileType profileType, String rootElement) {
        this.name = inName;
        this.typeClass = clazz;
        this.descriminator = descriminator;
        this.profileType = profileType;
        this.rootElement = rootElement;
    }

    public static ValidatorType getValidatorTypeByLabel(String inLabel) {
        ValidatorType[] validatorTypes = ValidatorType.values();
        for (ValidatorType validatorType : validatorTypes) {
            if (validatorType.getName().equalsIgnoreCase(inLabel)) {
                return validatorType;
            } else {
                continue;
            }
        }
        return null;
    }
}
