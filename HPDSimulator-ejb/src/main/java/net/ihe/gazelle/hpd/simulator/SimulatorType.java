package net.ihe.gazelle.hpd.simulator;

public enum SimulatorType {
    HPD_PROV_INFO_DIR("HPD Provider Information Directory"),
    CPI_PROV("CH:CPI Provider");

    String name;

    SimulatorType(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }
}
