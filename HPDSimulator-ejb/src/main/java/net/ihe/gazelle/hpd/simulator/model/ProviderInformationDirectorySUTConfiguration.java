package net.ihe.gazelle.hpd.simulator.model;

import javax.persistence.Column;
import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;

import net.ihe.gazelle.simulator.sut.model.SystemConfiguration;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import org.jboss.seam.annotations.Name;

@Name("providerInformationDirectorySUTConfiguration")
@Entity
@DiscriminatorValue("prov_info_dir")
public class ProviderInformationDirectorySUTConfiguration extends SystemConfiguration {

	/**
	 * 
	 */
	private static final long serialVersionUID = -1546363663867036043L;

	@Column(name = "base_dn")
	@NotNull
	private String baseDN;

	public ProviderInformationDirectorySUTConfiguration() {
		super();
	}

	public ProviderInformationDirectorySUTConfiguration(ProviderInformationDirectorySUTConfiguration inConfig) {
		super(inConfig);
		this.baseDN = inConfig.getBaseDN();
	}

	public String getBaseDN() {
		return baseDN;
	}

	public void setBaseDN(String baseDN) {
		this.baseDN = baseDN;
	}

}
