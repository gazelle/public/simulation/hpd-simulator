package net.ihe.gazelle.hpd.simulator.action;

import net.ihe.gazelle.hpd.AddRequest;
import net.ihe.gazelle.hpd.BatchRequest;
import net.ihe.gazelle.hpd.BatchResponse;
import net.ihe.gazelle.hpd.DelRequest;
import net.ihe.gazelle.hpd.ModifyDNRequest;
import net.ihe.gazelle.hpd.ModifyRequest;
import net.ihe.gazelle.hpd.SearchRequest;
import net.ihe.gazelle.hpd.simulator.initiator.SoapHPDClient;
import net.ihe.gazelle.hql.providers.EntityManagerService;
import net.ihe.gazelle.ldap.model.LDAPNode;
import net.ihe.gazelle.ldap.model.LDAPNodeQuery;
import net.ihe.gazelle.simulator.message.model.TransactionInstance;
import net.ihe.gazelle.simulator.sut.model.SystemConfiguration;
import net.ihe.gazelle.ssov7.authn.domain.GazelleIdentity;
import net.ihe.gazelle.ssov7.authn.interlay.adapter.GazelleIdentityImpl;
import org.jboss.seam.faces.FacesMessages;
import org.jboss.seam.international.StatusMessage;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.faces.model.SelectItem;
import javax.persistence.EntityManager;
import java.util.ArrayList;
import java.util.List;

public abstract class AbstractHPDInitiator extends AbstractInitiator{

	public static final int INDEX_INIT_VALUE = 1;
	private static Logger log = LoggerFactory.getLogger(AbstractHPDInitiator.class);

	private final GazelleIdentity gazelleIdentity = GazelleIdentityImpl.instance();
	private List<RequestWrapper> batchRequestMap;
	private RequestType selectedMessageType;
	private BatchResponse batchResponse;

	protected Integer index = INDEX_INIT_VALUE;

	public List<RequestWrapper> getBatchRequestMap() {
		if (this.batchRequestMap == null) {
			this.batchRequestMap = new ArrayList<RequestWrapper>();
		}
		return this.batchRequestMap;
	}
	
	public void setBatchRequestMap(List<RequestWrapper> requests){
		this.batchRequestMap = requests;
		if (requests == null){
			index = INDEX_INIT_VALUE;
		}
	}

	public void addRequestToBatch() {
		this.batchRequestMap.add(new RequestWrapper(index++, this.selectedMessageType));
	}

	public void removeRequestFromBatch(RequestWrapper inRequest) {
		this.getBatchRequestMap().remove(inRequest);
	}

	public List<LDAPNode> getAvailableNodes() {
		LDAPNodeQuery query = new LDAPNodeQuery();
		return query.getList();
	}
	
	protected List<TransactionInstance> sendMessageToSut(SystemConfiguration sut, List<RequestWrapper> requests){
		batchResponse = null; // make sure we will not keep the previous response if something goes wrong
		if (sut == null) {
			FacesMessages.instance().add(StatusMessage.Severity.ERROR,"You must first select the system under test to use");
			return null;
		} else if (requests == null || requests.isEmpty()) {
			FacesMessages.instance().add(StatusMessage.Severity.ERROR,"The batchReqest does not contain any request to be sent");
			return null;
		} else {
			BatchRequest batchRequest = new BatchRequest();
			for (RequestWrapper req : requests) {
				if (RequestType.ADD.equals(req.getRequestType())) {
					// set values from GUI into the actual request
					AddRequest addRequest = (AddRequest) req.getRequest();
					addRequest.valueAttributesFromGui();
					batchRequest.addAddRequest(addRequest);
				} else if (RequestType.DEL.equals(req.getRequestType())) {
					batchRequest.addDelRequest((DelRequest) req.getRequest());
				} else if (RequestType.MODIFY.equals(req.getRequestType())) {
					ModifyRequest modifyRequest = (ModifyRequest) req.getRequest();
					modifyRequest.valueAttributesFromGui();
					batchRequest.addModifyRequest(modifyRequest);
				} else if (RequestType.MODDN.equals(req.getRequestType())) {
					batchRequest.addModDNRequest((ModifyDNRequest) req.getRequest());
				} else if (RequestType.SEARCH.equals(req.getRequestType())) {
					req.createFilterFromGUIFilter();
					batchRequest.addSearchRequest((SearchRequest) req.getRequest());
				} else {
					FacesMessages.instance().add(StatusMessage.Severity.WARN,
							"This request: " + req.getRequestType() + " cannot be added to the batchRequest");
					continue;
				}
			}
			try {
				SoapHPDClient client = getSoapHPDClient(sut);
				batchResponse = client.sendBatchRequest(batchRequest);
				TransactionInstance instance = client.getTransactionInstance();
				if (gazelleIdentity.isLoggedIn()) {
					String company = gazelleIdentity.getOrganisationKeyword();
					instance.setCompanyKeyword(company);
				}
				EntityManager entityManager = EntityManagerService.provideEntityManager();
				instance = instance.save(entityManager);
				List<TransactionInstance> instances = new ArrayList<TransactionInstance>();
				instances.add(instance);
				return instances;
			} catch (Exception e) {
				FacesMessages.instance().add(StatusMessage.Severity.ERROR,"Unexpected error: " + e.getMessage());
				log.error("Unexpected error while sending message: " + e.getMessage());
			}
			return null;
		}
	}

	protected abstract SoapHPDClient getSoapHPDClient(SystemConfiguration sut);

	@Override
	public void sendMessage() {
		List<TransactionInstance> instances = sendMessageToSut(getSelectedSut(), getBatchRequestMap());
		setMessages(instances);
		if (getMessages() != null) {
			FacesMessages.instance().add(StatusMessage.Severity.INFO, "Message has been sent, scroll down to access the test report");
		}
	}

	@Override
	public void performAnotherTest() {
		setMessages(null);
		this.batchRequestMap = new ArrayList<RequestWrapper>();
		this.selectedMessageType = null;
	}

	public RequestType getSelectedMessageType() {
		return selectedMessageType;
	}

	public void setSelectedMessageType(RequestType selectedMessageType) {
		this.selectedMessageType = selectedMessageType;
	}


	public List<SelectItem> getAvailableRequestTypes(){
		List<SelectItem> items = RequestType.getRequestTypesForActor(getSimulatedActor().getKeyword());
		selectedMessageType = (RequestType) items.get(0).getValue();
		return items;
	}

	// replaces onNodeSelected(ValueChangeListener)
	public abstract void onNodeSelected(RequestWrapper currentRequest);

	public BatchResponse getBatchResponse() {
		return batchResponse;
	}

	public void setBatchResponse(BatchResponse batchResponse) {
		this.batchResponse = batchResponse;
	}
}
