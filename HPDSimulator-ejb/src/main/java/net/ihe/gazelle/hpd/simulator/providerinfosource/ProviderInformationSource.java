package net.ihe.gazelle.hpd.simulator.providerinfosource;

import net.ihe.gazelle.hpd.AddRequest;
import net.ihe.gazelle.hpd.DelRequest;
import net.ihe.gazelle.hpd.DsmlAttr;
import net.ihe.gazelle.hpd.DsmlModification;
import net.ihe.gazelle.hpd.ModifyDNRequest;
import net.ihe.gazelle.hpd.ModifyRequest;
import net.ihe.gazelle.hpd.OperationType;
import net.ihe.gazelle.hpd.gui.DsmlAttrValue;
import net.ihe.gazelle.hpd.simulator.action.AbstractHPDInitiator;
import net.ihe.gazelle.hpd.simulator.action.RequestWrapper;
import net.ihe.gazelle.hpd.simulator.initiator.ProviderInformationFeedSender;
import net.ihe.gazelle.hpd.simulator.initiator.SoapHPDClient;
import net.ihe.gazelle.hpd.simulator.model.ProviderInformationDirectorySUTConfiguration;
import net.ihe.gazelle.hpd.simulator.model.ProviderInformationDirectorySUTConfigurationQuery;
import net.ihe.gazelle.hql.restrictions.HQLRestrictions;
import net.ihe.gazelle.simulator.common.tf.model.Actor;
import net.ihe.gazelle.simulator.common.tf.model.Transaction;
import net.ihe.gazelle.simulator.sut.model.SystemConfiguration;
import org.jboss.seam.Component;
import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.Create;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Scope;
import org.jboss.seam.contexts.Contexts;
import org.jboss.seam.faces.FacesMessages;
import org.jboss.seam.international.StatusMessage;
import org.jboss.seam.security.Identity;

import javax.faces.model.SelectItem;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 */
@Name("providerInformationSource")
@Scope(ScopeType.PAGE)
public class ProviderInformationSource extends AbstractHPDInitiator implements Serializable {

    /**
     * Field serialVersionUID. (value is 5689936393123344480)
     */
    private static final long serialVersionUID = 5689936393123344480L;
    /**
     * Field availableOperations.
     */
    private static List<SelectItem> availableOperations;


    static {
        availableOperations = new ArrayList<SelectItem>();
        availableOperations.add(new SelectItem(null, "operation"));
        for (OperationType operationType : OperationType.values()) {
            availableOperations.add(new SelectItem(operationType, operationType.value()));
        }
    }

    /**
     * Method init.
     */
    @Create
    public void init() {
        setSelectedTransaction(Transaction.GetTransactionByKeyword("ITI-59"));
        setSimulatedActor(Actor.findActorWithKeyword("PROV_INFO_SRC"));
        @SuppressWarnings("unchecked")
        List<RequestWrapper> requests = (List<RequestWrapper>) Component.getInstance("newBatchRequest");
        if (requests != null && !requests.isEmpty()) {
            this.setBatchRequestMap(new ArrayList<RequestWrapper>(requests));
            // update the index in order to avoid collision if other requests are created before sending
            index = index + requests.size() + 1;
            Contexts.getSessionContext().set("newBatchRequest", null);
        }
    }

    /**
     * Method listAvailableSut.
     *
     * @return List<ProviderInformationDirectorySUTConfiguration>
     *
     */
    @Override
    public List<ProviderInformationDirectorySUTConfiguration> listAvailableSut() {
        ProviderInformationDirectorySUTConfigurationQuery query = new ProviderInformationDirectorySUTConfigurationQuery();
        query.listUsages().transaction().eq(getSelectedTransaction());
        if (!Identity.instance().isLoggedIn()) {
            query.isPublic().eq(true);
        } else if (!Identity.instance().hasRole("admin_role")) {
            query.addRestriction(HQLRestrictions.or(HQLRestrictions.eq("isPublic", true),
                    HQLRestrictions.eq("owner", Identity.instance().getCredentials().getUsername())));
        }
        return query.getList();
    }

    @Override
    protected SoapHPDClient getSoapHPDClient(SystemConfiguration sut) {
        return new ProviderInformationFeedSender(sut);
    }

    /**
     * Method onNodeSelected.
     *
     * @param currentRequest the request currently edited
     *
     */
    @Override
    public void onNodeSelected(RequestWrapper currentRequest) {
        String baseDN = "ou=" + currentRequest.getSelectedNode().getName().concat(",");
        if (this.getSelectedSut() != null) {
            baseDN = baseDN.concat(getSelectedSut().getBaseDN());
        }
        switch (currentRequest.getRequestType()) {
            case ADD:
                AddRequest addRequest = (AddRequest) currentRequest.getRequest();
                // if the user change the value of the node, we do not want to have the "old" attributes displayed
                addRequest.getAttr().clear();
                addRequest.setDn(baseDN);
                currentRequest.listRequiredAttributes();
                currentRequest.listAllAvailableAttributes();
                currentRequest.populateAddWithRequiredAttributes();
                break;
            case DEL:
                DelRequest delRequest = (DelRequest) currentRequest.getRequest();
                delRequest.setDn(baseDN);
                break;
            case MODIFY:
                ModifyRequest modifyRequest = (ModifyRequest) currentRequest.getRequest();
                modifyRequest.setDn(baseDN);
                currentRequest.listAllAvailableAttributes();
                break;
            case MODDN:
                ModifyDNRequest modDNRequest = (ModifyDNRequest) currentRequest.getRequest();
                modDNRequest.setDn(baseDN);
                modDNRequest.setDeleteoldrdn(false);
                break;
            default:
                FacesMessages.instance().add(StatusMessage.Severity.ERROR,"This message type is not allowed in ITI-59 transaction");
        }
    }

      /**
     * Method addValueToDsmlAttr.
     *
     */
    public void addValueToDsmlAttr(DsmlAttr dsmlAttr) {
        dsmlAttr.addValueForGui("new value");
    }

    /**
     * Method removeValueFromDsmlAttribute.
     *
     * @param inValue String
     */
    public void removeValueFromDsmlAttribute(DsmlAttr dsmlAttr, DsmlAttrValue inValue) {
        if (inValue != null) {
            dsmlAttr.removeValueForGui(inValue);
        }
    }

    /**
     * Method addValueToDsmlModification.
     *
     * @param inModification DsmlModification
     */
    public void addValueToDsmlModification(DsmlModification inModification) {
        inModification.addValueForGui("new value");
    }

    /**
     * Method removeValueFromDsmlModification.
     *
     * @param inValue        String
     * @param inModification DsmlModification
     */
    public void removeValueFromDsmlModification(DsmlAttrValue inValue, DsmlModification inModification) {
        if ((inValue != null) && (inModification != null)) {
            inModification.removeValueForGui(inValue);
        }
    }

    /**
     * Method getAvailableOperations.
     *
     * @return List<SelectItem>
     */
    public List<SelectItem> getAvailableOperations() {
        return availableOperations;
    }

}
