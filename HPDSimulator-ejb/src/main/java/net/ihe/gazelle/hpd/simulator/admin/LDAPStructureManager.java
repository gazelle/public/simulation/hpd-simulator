package net.ihe.gazelle.hpd.simulator.admin;

import net.ihe.gazelle.ldap.model.AttributeTypes;
import net.ihe.gazelle.ldap.model.ObjectClasses;
import net.ihe.gazelle.ldap.model.Structure;
import net.ihe.gazelle.ldap.model.TypeObjectClass;
import org.jboss.seam.Component;
import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Scope;
import org.richfaces.event.FileUploadEvent;
import org.richfaces.model.UploadedFile;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.persistence.EntityManager;
import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.io.Serializable;

@Name("ldapStructureManager")
@Scope(ScopeType.PAGE)
public class LDAPStructureManager implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = 5632879228001667092L;

    private static Logger log = LoggerFactory.getLogger(LDAPStructureManager.class);

    public void uploadEventListener(FileUploadEvent event) {
        UploadedFile item = event.getUploadedFile();
        InputStream bais = new ByteArrayInputStream(item.getData());
        try {
            Structure uploadedStructure = Structure.xmlToJava(bais);
            if (uploadedStructure != null) {
                EntityManager entityManager = (EntityManager) Component.getInstance("entityManager");
                for (AttributeTypes type : uploadedStructure.getAttributeType()) {
                    if (type.getSinglevalue() == null) {
                        type.setSinglevalue(false);
                    }
                    if ((type.getDescription() != null) && (type.getDescription().length() > 255)) {
                        type.setDescription(type.getDescription().substring(0, 254));
                    }
                    if ((type.getName() != null) && type.getName().trim().contains(" ")) {
                        String[] names = type.getName().trim().split(" ");
                        for (String name : names) {
                            AttributeTypes newType = new AttributeTypes(type, name);
                            entityManager.merge(newType);
                        }
                    } else {
                        entityManager.merge(type);
                        log.info("storing attributeType : " + type.getName());
                    }
                }
                entityManager.flush();
                for (ObjectClasses classes : uploadedStructure.getObjectClass()) {
                    if ((classes.getDescription() != null) && (classes.getDescription().length() > 255)) {
                        classes.setDescription(classes.getDescription().substring(0, 254));
                    }
                    if (classes.getTypeobjectclass() == null) {
                        classes.setTypeobjectclass(TypeObjectClass.STRUCTURAL);
                    }
                    entityManager.merge(classes);
                    log.info("storing objectClasses: " + classes.getName());
                }
                entityManager.flush();
            }
        } catch (Exception e) {
            log.error("Unable to convert file: " + item.getName() + " for the following reason: " + e.getMessage());
        }
    }
}
