package net.ihe.gazelle.hpd.validator;

import net.ihe.gazelle.gen.common.CommonOperations;
import net.ihe.gazelle.gen.common.ConstraintValidatorModule;
import net.ihe.gazelle.gen.common.SVSConsumer;
import net.ihe.gazelle.hpd.BatchRequest;
import net.ihe.gazelle.hpd.BatchResponse;
import net.ihe.gazelle.hpd.core.COREPackValidator;
import net.ihe.gazelle.hpd.validator.chcpi.CHCPIPackValidator;
import net.ihe.gazelle.hpd.validator.chhpdaddrequest.CHHPDADDREQUESTPackValidator;
import net.ihe.gazelle.hpd.validator.core.addrequest.ADDREQUESTPackValidator;
import net.ihe.gazelle.hpd.validator.core.deleterequest.DELETEREQUESTPackValidator;
import net.ihe.gazelle.hpd.validator.core.hpdcommon.HPDCOMMONPackValidator;
import net.ihe.gazelle.hpd.validator.core.ihehpd.IHEHPDPackValidator;
import net.ihe.gazelle.hpd.validator.core.ldapresult.LDAPRESULTPackValidator;
import net.ihe.gazelle.hpd.validator.core.modifydnrequest.MODIFYDNREQUESTPackValidator;
import net.ihe.gazelle.hpd.validator.core.modifyrequest.MODIFYREQUESTPackValidator;
import net.ihe.gazelle.hpd.validator.core.providerinformationfeedrequest.PROVIDERINFORMATIONFEEDREQUESTPackValidator;
import net.ihe.gazelle.hpd.validator.core.providerinformationfeedresponse.PROVIDERINFORMATIONFEEDRESPONSEPackValidator;
import net.ihe.gazelle.hpd.validator.core.providerinformationqueryrequest.PROVIDERINFORMATIONQUERYREQUESTPackValidator;
import net.ihe.gazelle.hpd.validator.core.providerinformationqueryresponse.PROVIDERINFORMATIONQUERYRESPONSEPackValidator;
import net.ihe.gazelle.hpd.validator.core.searchrequest.SEARCHREQUESTPackValidator;
import net.ihe.gazelle.hpd.validator.core.searchresponse.SEARCHRESPONSEPackValidator;
import net.ihe.gazelle.hpdTransformer.HPDTransformer;
import net.ihe.gazelle.khpd.validator.khpdcommon.KHPDCOMMONPackValidator;
import net.ihe.gazelle.preferences.PreferenceService;
import net.ihe.gazelle.validation.DetailedResult;
import net.ihe.gazelle.validation.DocumentValidXSD;
import net.ihe.gazelle.validation.DocumentWellFormed;
import net.ihe.gazelle.validation.Error;
import net.ihe.gazelle.validation.MDAValidation;
import net.ihe.gazelle.validation.Notification;
import net.ihe.gazelle.validation.ValidationResultsOverview;
import net.ihe.gazelle.validation.XSDMessage;
import net.ihe.gazelle.validator.hpd.util.ValidatorException;
import net.ihe.gazelle.validator.hpd.util.XMLValidation;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.parsers.SAXParserFactory;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.PrintStream;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Properties;

public class GazelleHPDValidator {

    private static final Logger LOG = LoggerFactory.getLogger(GazelleHPDValidator.class);

    private SAXParserFactory saxParserFactory;
    private String xsdLocation;

    static {
        CommonOperations.setValueSetProvider(new SVSConsumer() {
            @Override
            protected String getSVSRepositoryUrl() {
                return PreferenceService.getString("svs_repository_url");
            }
        });
    }

    public GazelleHPDValidator(SAXParserFactory factory, String xsdLocation) {
        this.saxParserFactory = factory;
        this.xsdLocation = xsdLocation;
    }

    public <T> DetailedResult validate(String messageToValidate, ValidatorType type) {
        DetailedResult result = new DetailedResult();
        // initialize list of modules with default validators
        List<ConstraintValidatorModule> validatorModules = new ArrayList<ConstraintValidatorModule>();
        if (type.getProfileType().equals(ProfileType.HPD)) {
            validatorModules.add(new COREPackValidator());
            if (!type.equals(ValidatorType.BATCH_REQUEST) && !type.equals(ValidatorType.BATCH_RESPONSE)) {
                validatorModules.add(new IHEHPDPackValidator());
                validatorModules.add(new HPDCOMMONPackValidator());
            }
        }
        // validate against schema
        validateAgainstSchema(result, messageToValidate, type);
        if (type.getProfileType().equals(ProfileType.HPD)) {
            // validate by module
            List<Notification> notifications = validateByModules(type.getTypeClass(), messageToValidate, validatorModules,
                    type);
            if (result.getMDAValidation() == null) {
                result.setMDAValidation(new MDAValidation());
            }
            result.getMDAValidation().setResult("PASSED");
            for (Notification notification : notifications) {
                result.getMDAValidation().getWarningOrErrorOrNote().add(notification);
                if (notification instanceof Error) {
                    result.getMDAValidation().setResult("FAILED");
                }
            }
        }
        addResultHeader(result, type);
        return result;
    }

    protected <T> List<Notification> validateByModules(Class<T> clazz, String messageToValidate,
                                                       List<ConstraintValidatorModule> modules, ValidatorType validatorType) {
        T objectToValidate = null;
        List<Notification> notifications = new ArrayList<Notification>();
        try {
            objectToValidate = HPDTransformer.unmarshallMessage(clazz,
                    new ByteArrayInputStream(messageToValidate.getBytes("UTF-8")));
            if (objectToValidate == null) {
                throw new Exception();
            } else {
                switch (validatorType) {
                    case CH_FEED_REQUEST:
                        modules.add(new CHHPDADDREQUESTPackValidator());
                        setModulesForFeedRequest(modules);
                        break;
                    case FEED_REQUEST:
                        setModulesForFeedRequest(modules);
                        break;
                    case CH_FEED_RESPONSE:
                    case FEED_RESPONSE:
                        modules.add(new PROVIDERINFORMATIONFEEDRESPONSEPackValidator());
                        modules.add(new LDAPRESULTPackValidator());
                        break;
                    case KSA_QUERY_REQUEST:
                    case QUERY_REQUEST:
                    case CH_QUERY_REQUEST:
                        modules.add(new PROVIDERINFORMATIONQUERYREQUESTPackValidator());
                        modules.add(new SEARCHREQUESTPackValidator());
                        break;
                    case QUERY_RESPONSE:
                    case CH_QUERY_RESPONSE:
                        modules.add(new PROVIDERINFORMATIONQUERYRESPONSEPackValidator());
                        modules.add(new SEARCHRESPONSEPackValidator());
                        break;
                    case KSA_QUERY_RESPONSE:
                        modules.add(new KHPDCOMMONPackValidator());
                        modules.add(new PROVIDERINFORMATIONQUERYRESPONSEPackValidator());
                        modules.add(new SEARCHRESPONSEPackValidator());
                        break;
                    case CH_CPI_REQUEST:
                        modules.add(new CHCPIPackValidator());
                        break;
                    default:
                        break;
                }
                if (validatorType.getTypeClass().equals(BatchRequest.class)) {
                    for (ConstraintValidatorModule module : modules) {
                        BatchRequest.validateByModule((BatchRequest) objectToValidate, "/batchRequest", module,
                                notifications);
                    }
                } else {
                    for (ConstraintValidatorModule module : modules) {
                        BatchResponse.validateByModule((BatchResponse) objectToValidate, "/batchResponse", module,
                                notifications);
                    }
                }
            }
        } catch (ValidatorException e) {
            errorWhenExtracting(e, notifications);
        } catch (Exception e) {
            errorWhenExtracting(null, notifications);
        }
        return notifications;
    }

    private void setModulesForFeedRequest(List<ConstraintValidatorModule> modules) {
        modules.add(new PROVIDERINFORMATIONFEEDREQUESTPackValidator());
        modules.add(new ADDREQUESTPackValidator());
        modules.add(new DELETEREQUESTPackValidator());
        modules.add(new MODIFYREQUESTPackValidator());
        modules.add(new MODIFYDNREQUESTPackValidator());
    }

    public static void addResultHeader(DetailedResult result, ValidatorType validator) {
        Date dd = new Date();
        DateFormat dateFormat = new SimpleDateFormat("yyyy, MM dd");
        DateFormat timeFormat = new SimpleDateFormat("hh:mm:ss");
        result.setValidationResultsOverview(new ValidationResultsOverview());
        result.getValidationResultsOverview().setValidationDate(dateFormat.format(dd));
        result.getValidationResultsOverview().setValidationTime(timeFormat.format(dd));
        Properties properties = new Properties();
        String validationEngine = null;
        try {
            InputStream is = GazelleHPDValidator.class.getClassLoader()
                    .getResourceAsStream("/META-INF/maven/net.ihe.gazelle.maven/ihehpd-validator-jar/pom.properties");
            if (is != null) {
                properties.load(is);
                validationEngine = properties.getProperty("artifactId");
                validationEngine = validationEngine.concat(":");
                validationEngine = validationEngine.concat(properties.getProperty("version"));
            } else {
                LOG.info("cannot read properties from metadata");
            }
        } catch (IOException e) {
            LOG.error("cannot load pom.properties file");
        }
        result.getValidationResultsOverview().setValidationEngine(validationEngine);
        result.getValidationResultsOverview().setValidationTestResult("PASSED");
        if ((result.getDocumentValidXSD() != null) && (result.getDocumentValidXSD().getResult() != null)
                && (result.getDocumentValidXSD().getResult().equals("FAILED"))) {
            result.getValidationResultsOverview().setValidationTestResult("FAILED");
        }
        if ((result.getDocumentWellFormed() != null) && (result.getDocumentWellFormed().getResult() != null)
                && (result.getDocumentWellFormed().getResult().equals("FAILED"))) {
            result.getValidationResultsOverview().setValidationTestResult("FAILED");
        }
        if ((result.getMDAValidation() != null) && (result.getMDAValidation().getResult() != null)
                && (result.getMDAValidation().getResult().equals("FAILED"))) {
            result.getValidationResultsOverview().setValidationTestResult("FAILED");
        }
    }

    private void errorWhenExtracting(ValidatorException vexp, List<Notification> ln) {
        if (ln == null) {
            return;
        }
        if (vexp != null) {
            if (vexp.getDiagnostic() != null) {
                for (Notification notification : vexp.getDiagnostic()) {
                    ln.add(notification);
                }
            }
        } else {
            net.ihe.gazelle.validation.Error err = new net.ihe.gazelle.validation.Error();
            err.setTest("structure");
            err.setLocation("All the document");
            err.setDescription("The root of the element is not the one expected by the tool");
            ln.add(err);
        }
    }

    protected <T> void validateAgainstSchema(DetailedResult result, String messageToValidate, ValidatorType type) {
        DocumentWellFormed documentWellFormedResult;
        XMLValidation xmlValidation = new XMLValidation(saxParserFactory, xsdLocation);
        documentWellFormedResult = xmlValidation.isXMLWellFormed(messageToValidate);
        result.setDocumentWellFormed(documentWellFormedResult);
        if (result.getDocumentWellFormed().getResult().equals("PASSED")) {
            try {
                result.setDocumentValidXSD(xmlValidation.isXSDValid(messageToValidate));
            } catch (Exception e) {
                ByteArrayOutputStream baos = new ByteArrayOutputStream();
                PrintStream ps = new PrintStream(baos);
                e.printStackTrace(ps);
                DocumentValidXSD docv = new DocumentValidXSD();
                docv.setResult("FAILED");
                docv.getXSDMessage().add(new XSDMessage());
                docv.getXSDMessage().get(0).setSeverity("error");
                docv.getXSDMessage()
                        .get(0)
                        .setMessage(
                                "error when validating with the schema. The error is : "
                                        + StringUtils.substring(baos.toString(), 0, 300));
                result.setDocumentValidXSD(docv);
            }
        }
    }

    public static String getDetailedResultAsString(DetailedResult detailedResult) {
        if (detailedResult != null) {
            ByteArrayOutputStream baos = new ByteArrayOutputStream();
            try {
                JAXBContext jc = JAXBContext.newInstance("net.ihe.gazelle.validation");
                Marshaller m = jc.createMarshaller();
                m.setProperty(Marshaller.JAXB_ENCODING, "UTF-8");
                m.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.TRUE);
                m.marshal(detailedResult, baos);
            } catch (JAXBException e) {
                e.printStackTrace();
            }
            return baos.toString();
        }
        return null;
    }
}
