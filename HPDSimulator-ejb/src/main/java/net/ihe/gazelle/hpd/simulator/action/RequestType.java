package net.ihe.gazelle.hpd.simulator.action;

import javax.faces.model.SelectItem;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by aberge on 26/04/17.
 */
public enum RequestType {

    ADD("addRequest", "/prov_info_src/add.xhtml", "PROV_INFO_SRC"),
    DEL("delRequest", "/prov_info_src/del.xhtml", "PROV_INFO_SRC"),
    MODIFY("modifyRequest", "/prov_info_src/modify.xhtml", "PROV_INFO_SRC"),
    MODDN("modDNRequest", "/prov_info_src/modDN.xhtml", "PROV_INFO_SRC"),
    SEARCH("searchRequest", "/prov_info_cons/search.xhtml", "PROV_INFO_CONS");

    String label;
    String editPage;
    String actorKeyword;

    RequestType(String label, String editPage, String actorKeyword){
        this.label = label;
        this.editPage = editPage;
        this.actorKeyword = actorKeyword;
    }

    public static List<SelectItem> getRequestTypesForActor(String actorKeyword){
        List<SelectItem> items = new ArrayList<SelectItem>();
        for (RequestType requestType: values()){
            if (requestType.actorKeyword.equals(actorKeyword)){
                items.add(new SelectItem(requestType, requestType.label));
            }
        }
        return items;
    }

    public String getLabel() {
        return label;
    }

    public String getEditPage() {
        return editPage;
    }

    public String getActorKeyword() {
        return actorKeyword;
    }
}
