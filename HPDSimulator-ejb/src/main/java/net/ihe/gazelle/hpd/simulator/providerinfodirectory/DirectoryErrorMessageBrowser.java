package net.ihe.gazelle.hpd.simulator.providerinfodirectory;

import java.io.Serializable;

import javax.faces.context.FacesContext;

import net.ihe.gazelle.common.filter.Filter;
import net.ihe.gazelle.common.filter.FilterDataModel;
import net.ihe.gazelle.hpd.simulator.model.DirectoryErrorMessage;
import net.ihe.gazelle.hpd.simulator.model.DirectoryErrorMessageQuery;
import net.ihe.gazelle.hql.criterion.HQLCriterionsForFilter;
import net.ihe.gazelle.simulator.common.model.ApplicationConfiguration;

import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Scope;

@Name("directoryErrorMessageBrowser")
@Scope(ScopeType.PAGE)
public class DirectoryErrorMessageBrowser implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = -6757471595598224496L;

    private Filter<DirectoryErrorMessage> filter;
    private FilterDataModel<DirectoryErrorMessage> data;
    private DirectoryErrorMessage selectedMessage;

    public Filter<DirectoryErrorMessage> getFilter() {
        if (filter == null) {
            filter = new Filter<DirectoryErrorMessage>(getHQLCriterions(), FacesContext.getCurrentInstance()
                    .getExternalContext().getRequestParameterMap());
        }
        return filter;
    }

    public FilterDataModel<DirectoryErrorMessage> getData() {
        if (data == null) {
            data = new FilterDataModel<DirectoryErrorMessage>(getFilter()) {
                @Override
                protected Object getId(DirectoryErrorMessage t) {
                    // TODO Auto-generated method stub
                    return t.getId();
                }
            };
        }
        return data;
    }

    private HQLCriterionsForFilter<DirectoryErrorMessage> getHQLCriterions() {
        DirectoryErrorMessageQuery query = new DirectoryErrorMessageQuery();
        HQLCriterionsForFilter<DirectoryErrorMessage> criteria = query.getHQLCriterionsForFilter();
        criteria.addPath("client", query.client());
        criteria.addPath("timestamp", query.timestamp());
        criteria.addPath("transaction", query.transactionKeyword());
        criteria.addPath("messageType", query.messageType());
        criteria.addPath("errorCode", query.codeMeaning());
        // criteria.addPath("messageId", query.transactionInstanceId());
        return criteria;
    }

    public void reset() {
        filter.clear();
        data.resetCache();
    }

    public DirectoryErrorMessage getSelectedMessage() {
        return selectedMessage;
    }

    public void setSelectedMessage(DirectoryErrorMessage selectedMessage) {
        this.selectedMessage = selectedMessage;
    }

    public String getLinkToTransactionInstance(Integer transactionInstanceId) {
        return ApplicationConfiguration.getValueOfVariable("message_permanent_link").concat(
                transactionInstanceId.toString());
    }

}
