package net.ihe.gazelle.hpd.simulator.admin;

import java.io.Serializable;
import java.util.List;

import javax.persistence.EntityManager;

import net.ihe.gazelle.hql.providers.EntityManagerService;
import net.ihe.gazelle.ldap.model.LDAPNode;
import net.ihe.gazelle.ldap.model.LDAPNodeQuery;
import net.ihe.gazelle.ldap.model.LDAPPartition;
import net.ihe.gazelle.ldap.model.LDAPPartitionQuery;

import org.jboss.seam.Component;
import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.Create;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Scope;

@Name("ldapPartitionManager")
@Scope(ScopeType.PAGE)
public class LDAPPartitionManager implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -2127105366197632254L;

	private LDAPPartition selectedPartition;
	private List<LDAPPartition> availablePartitions;
	private boolean displayList;
	private boolean editPartition;
	private boolean createPartitionOnServer;

	@Create
	public void listAvailablePartitions() {
		LDAPPartitionQuery query = new LDAPPartitionQuery();
		query.name().order(true);
		availablePartitions = query.getList();
		this.displayList = true;
		this.editPartition = false;
		this.selectedPartition = null;
	}

	public void editPartition(LDAPPartition inPartition) {
		if (inPartition.getId() != null) {
			EntityManager entityManager = (EntityManager) Component.getInstance("entityManager");
			this.selectedPartition = entityManager.find(LDAPPartition.class, inPartition.getId());
		} else {
			this.selectedPartition = inPartition;
		}
		this.editPartition = true;
		this.displayList = false;
	}

	public void createNewPartition() {
		editPartition(new LDAPPartition());
	}

	public void copyPartition(LDAPPartition inPartition) {
		editPartition(new LDAPPartition(inPartition));
	}

	public void cancelEdition() {
		this.displayList = true;
		this.editPartition = false;
		this.selectedPartition = null;
	}

	public void savePartition() {
		if (selectedPartition != null) {
			selectedPartition.save();
			listAvailablePartitions();
		}
	}

	public void deletePartition() {
		if (selectedPartition != null) {
			LDAPPartitionQuery query = new LDAPPartitionQuery();
			query.id().eq(selectedPartition.getId());
			LDAPPartition partitionToDelete = query.getUniqueResult();
			EntityManager entityManager = EntityManagerService.provideEntityManager();
			entityManager.remove(partitionToDelete);
			entityManager.flush();
			listAvailablePartitions();
		}
	}

	public List<LDAPNode> getAvailableNodes() {
		LDAPNodeQuery query = new LDAPNodeQuery();
		query.name().order(true);
		return query.getList();
	}

	public LDAPPartition getSelectedPartition() {
		return selectedPartition;
	}

	public void setSelectedPartition(LDAPPartition selectedPartition) {
		this.selectedPartition = selectedPartition;
	}

	public boolean isDisplayList() {
		return displayList;
	}

	public void setDisplayList(boolean displayList) {
		this.displayList = displayList;
	}

	public boolean isEditPartition() {
		return editPartition;
	}

	public void setEditPartition(boolean editPartition) {
		this.editPartition = editPartition;
	}

	public List<LDAPPartition> getAvailablePartitions() {
		return availablePartitions;
	}

	public boolean isCreatePartitionOnServer() {
		return createPartitionOnServer;
	}

	public void setCreatePartitionOnServer(boolean createPartitionOnServer) {
		this.createPartitionOnServer = createPartitionOnServer;
	}

}
