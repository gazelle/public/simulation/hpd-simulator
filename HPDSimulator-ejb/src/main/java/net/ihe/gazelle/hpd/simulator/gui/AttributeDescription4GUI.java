package net.ihe.gazelle.hpd.simulator.gui;

import net.ihe.gazelle.hpd.AttributeDescription;
import net.ihe.gazelle.hpd.simulator.providerinfoconsumer.FilterType;

import java.io.Serializable;

/**
 * Created by aberge on 23/06/17.
 */
public class AttributeDescription4GUI extends AbstractFilterItem {

    private AttributeDescription attributeDescription;

    public AttributeDescription4GUI(){
        super();
        setFilterType(FilterType.PRESENT);
        attributeDescription = new AttributeDescription();
    }

    @Override
    public String getIncludeLink() {
        return "/prov_info_cons/attributeDescription.xhtml";
    }

    public AttributeDescription getAttributeDescription() {
        return attributeDescription;
    }

    @Override
    public Serializable getFilterItem(){
        return attributeDescription;
    }

}
