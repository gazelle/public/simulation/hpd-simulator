package net.ihe.gazelle.hpd.simulator.initiator;

import net.ihe.gazelle.hpd.BatchRequest;
import net.ihe.gazelle.hpd.BatchResponse;
import net.ihe.gazelle.hpdTransformer.HPDTransformer;
import net.ihe.gazelle.preferences.PreferenceService;
import net.ihe.gazelle.simulator.common.ihewsinit.SoapSendException;
import net.ihe.gazelle.simulator.common.ihewsinit.SoapWebServiceClient;
import net.ihe.gazelle.simulator.common.tf.model.Actor;
import net.ihe.gazelle.simulator.common.tf.model.Domain;
import net.ihe.gazelle.simulator.message.model.EStandard;
import net.ihe.gazelle.simulator.sut.model.SystemConfiguration;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.w3c.dom.Element;

import javax.xml.bind.JAXBException;
import javax.xml.namespace.QName;
import javax.xml.soap.SOAPMessage;

public abstract class SoapHPDClient extends SoapWebServiceClient {

    private static final Logger LOG = LoggerFactory.getLogger(SoapHPDClient.class);
    public static final String SOAP_FAULT = "SOAP Fault";

    String domainKeyword;
    private static final String EPD = "EPD";
    protected static final String ITI = "ITI";

    public SoapHPDClient(SystemConfiguration sut) {
        super();
        if (sut != null) {
            setSutUrl(sut.getUrl());
            setSutName(sut.getName());
        }
        Boolean isEPD = PreferenceService.getBoolean("ch_hpd_enabled");
        this.domainKeyword = (isEPD != null && isEPD) ? EPD : ITI;
    }


    @Override
    public Domain getDomainForTransactionInstance() {
        return Domain.getDomainByKeyword(domainKeyword);
    }

    private byte[] batchResponseToByteArray(BatchResponse response) {
        if (response != null) {
            try {
                return HPDTransformer.getMessageAsByteArray(BatchResponse.class, response);
            } catch (JAXBException e) {
                LOG.warn(e.getMessage());
            }
        }
        return null;
    }

    private byte[] batchRequestToByteArray(BatchRequest request) {
        if (request != null) {
            try {
                return HPDTransformer.getMessageAsByteArray(BatchRequest.class, request);
            } catch (JAXBException e) {
                LOG.warn(e.getMessage());
            }
        }
        return null;
    }

    protected abstract String getAction();

    public BatchResponse sendBatchRequest(BatchRequest batchRequest) throws SoapSendException {
        transactionInstance = null;
        BatchResponse response = send(BatchResponse.class, batchRequest.get_xmlNodePresentation(), getAction());
        String responseType = null;
        if (response != null) {
            responseType = response.get_xmlNodePresentation().getLocalName();
            if (!"SOAP Fault".equals(responseType)) {
                responseType = getResponseType();
            }
        }
        saveTransactionInstance(batchRequestToByteArray(batchRequest), batchResponseToByteArray(response), getRequestType(),
                responseType);
        return response;
    }

    @Override
    protected Element getAssertionFromSTS(String username, String password) {
        return null;
    }

    @Override
    protected void appendAssertionToSoapHeader(SOAPMessage msg, Element assertion) {

    }

    @Override
    public String getIssuerName() {
        return "Gazelle HPDSimulator";
    }

    @Override
    protected QName getServiceQName() {
        return null;
    }

    @Override
    protected QName getPortQName() {
        return null;
    }

    @Override
    protected Actor getSutActor() {
        return Actor.findActorWithKeyword("PROV_INFO_DIR");
    }

    @Override
    protected EStandard getStandard() {
        return null;
    }

    protected abstract String getRequestType();

    protected abstract String getResponseType();
}
