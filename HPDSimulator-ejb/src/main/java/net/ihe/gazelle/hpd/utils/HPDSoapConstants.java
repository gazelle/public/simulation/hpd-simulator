package net.ihe.gazelle.hpd.utils;

/**
 * <p>HPDSoapConstants class.</p>
 *
 * @author aberge
 * @version 1.0: 09/10/17
 */

public final class HPDSoapConstants {

    private HPDSoapConstants(){

    }

    public static final String HPD_TARGET_NAMESPACE = "urn:ihe:iti:hpd:2010";
    public static final String DSML_NAMESPACE = "urn:oasis:names:tc:DSML:2:0:core";
    public static final String HPD_NAMESPACE = "urn:ihe:iti:2010";
    public static final String HPD_QUERY_ACTION = HPD_NAMESPACE + ":ProviderInformationQuery";
    public static final String HPD_FEED_ACTION = HPD_NAMESPACE + ":ProviderInformationFeed";
    public static final String HPD_FEED_RESPONSE_ACTION = HPD_NAMESPACE + ":ProviderInformationFeedResponse";
    public static final String HPD_QUERY_RESPONSE_ACTION = HPD_NAMESPACE + ":ProviderInformationQueryResponse";
    public static final String HPD_PORT_NAME = "ProviderInformationDirectory_Port_Soap12";
    public static final String HPD_PORT_TYPE = "ProviderInformationDirectory_PortType";
    public static final String HPD_SERVICE_NAME = "ProviderInformationDirectory_Service";

    public static final String CHCPI_TARGET_NAMESPACE = "urn:ch:admin:bag:epr:cpi:2017/Imports";
    public static final String CHCPI_NAMESPACE = "urn:ch:admin:bag:epr:2017";
    public static final String CHCPI_COMMUNITY_QUERY_ACTION = CHCPI_NAMESPACE + ":CommunityQuery";
    public static final String CHCPI_COMMUNITY_QUERY_RESPONSE_ACTION = CHCPI_NAMESPACE + ":CommunityQueryResponse";
    public static final String CHCPI_COMMUNITY_DOWNLOAD_ACTION = CHCPI_NAMESPACE + ":CommunityDownload";
    public static final String CHCPI_COMMUNITY_DOWNLOAD_ACTION_RESPONSE = CHCPI_NAMESPACE + ":CommunityDownloadResponse";
    public static final String CHCPI_PORT_NAME = "CPIProvider_Port_Soap12";
    public static final String CHCPI_PORT_TYPE = "CPIProvider_PortType";
    public static final String CHCPI_SERVICE_NAME = "CPIProvider_Service";

}
