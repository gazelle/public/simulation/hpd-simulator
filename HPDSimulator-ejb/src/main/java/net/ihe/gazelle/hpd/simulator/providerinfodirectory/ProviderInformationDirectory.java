package net.ihe.gazelle.hpd.simulator.providerinfodirectory;

import net.ihe.gazelle.hpd.*;
import net.ihe.gazelle.hpd.simulator.model.DirectoryErrorMessage;
import net.ihe.gazelle.hpd.validator.ValidatorType;
import net.ihe.gazelle.hpdTransformer.HPDTransformer;
import net.ihe.gazelle.ldap.model.LDAPPartition;
import net.ihe.gazelle.ldap.model.LDAPPartitionQuery;
import net.ihe.gazelle.preferences.PreferenceService;
import net.ihe.gazelle.simulator.common.model.ApplicationConfiguration;
import net.ihe.gazelle.simulator.common.tf.model.Actor;
import net.ihe.gazelle.simulator.common.tf.model.Domain;
import net.ihe.gazelle.simulator.common.tf.model.Transaction;
import net.ihe.gazelle.simulator.message.model.TransactionInstance;
import org.jboss.seam.Component;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.persistence.EntityManager;
import javax.xml.bind.JAXBException;
import java.io.Serializable;
import java.util.Date;
import java.util.List;

/**
 * This class is used for performing the binding between the WS interface and ApacheDS
 *
 * @author aberge
 */
public class ProviderInformationDirectory {

    public static final String FEED_TRANSACTION_KEYWORD = "ITI-59";
    public static final String QUERY_TRANSACTION_KEYWORD = "ITI-58";
    public static final String HPD_SIMULATOR = "HPDSimulator";
    protected static final String SEARCH_REQUEST = "searchRequest";
    private static final String EPD = "EPD";
    private static final String ITI = "ITI";
    private static Logger log = LoggerFactory.getLogger(ProviderInformationDirectory.class);

    protected Transaction transaction;
    protected Actor sutActor;
    protected Actor simulatedActor;
    protected LDAPPartition ldapPartition;
    protected String remoteHost;
    protected TransactionInstance transactionInstance;

    /**
     * Constructor
     *
     * @param inTransaction : IHE transaction performed (depends on soapAction)
     * @param inSutActor    : role played by the SUT (depends on soapAction)
     * @param remoteHost    : the client IP
     */
    public ProviderInformationDirectory(Transaction inTransaction, Actor inSutActor, String remoteHost) {
        this.transaction = inTransaction;
        this.sutActor = inSutActor;
        this.simulatedActor = Actor.findActorWithKeyword("PROV_INFO_DIR");
        this.remoteHost = remoteHost;
    }

    /**
     * Constructor for UT
     */
    protected ProviderInformationDirectory() {
    }

    /**
     * Create a new instance of TransactionInstance, check the content of the incoming message, forward the request to ApacheDS and format the response (to conform to IHE spec) before sending it back
     * to the sender
     *
     * @param request : request received from the SUT
     * @return the batchResponse to be sent back to the SUT
     */
    public BatchResponse handleRequest(BatchRequest request) {
        // initialize transactionInstance
        EntityManager entityManager = (EntityManager) Component.getInstance("entityManager");
        Boolean isEPD = PreferenceService.getBoolean("ch_hpd_enabled");
        String domainKeyword = (isEPD != null && isEPD) ? EPD : ITI;
        ValidatorType validatorType = null;
        if (ITI.equals(domainKeyword) && transaction.getKeyword().equals(QUERY_TRANSACTION_KEYWORD)) {
            validatorType = ValidatorType.QUERY_REQUEST;
        } else if (ITI.equals(domainKeyword) && transaction.getKeyword().equals(FEED_TRANSACTION_KEYWORD)) {
            validatorType = ValidatorType.FEED_REQUEST;
        } else if (EPD.equals(domainKeyword) && transaction.getKeyword().equals(QUERY_TRANSACTION_KEYWORD)) {
            validatorType = ValidatorType.CH_QUERY_REQUEST;
        } else if (EPD.equals(domainKeyword) && transaction.getKeyword().equals(FEED_TRANSACTION_KEYWORD)) {
            validatorType = ValidatorType.CH_FEED_REQUEST;
        }
        fillOutRequest(domainKeyword, validatorType, request);
        entityManager.persist(transactionInstance);
        BatchResponse response = checkRequest(request);
        if (response == null) {
            response = forwardRequestToLDAPServer(request);
        }
        // fill out response
        if (response != null) {
            String responseType = null;
            if (ITI.equals(domainKeyword) && transaction.getKeyword().equals(QUERY_TRANSACTION_KEYWORD)) {
                responseType = ValidatorType.QUERY_RESPONSE.getName();
            } else if (ITI.equals(domainKeyword) && transaction.getKeyword().equals(FEED_TRANSACTION_KEYWORD)) {
                responseType = ValidatorType.FEED_RESPONSE.getName();
            } else if (EPD.equals(domainKeyword) && transaction.getKeyword().equals(QUERY_TRANSACTION_KEYWORD)) {
                responseType = ValidatorType.CH_QUERY_RESPONSE.getName();
            } else if (EPD.equals(domainKeyword) && transaction.getKeyword().equals(FEED_TRANSACTION_KEYWORD)) {
                responseType = ValidatorType.CH_FEED_RESPONSE.getName();
            }
            response = fillOutResponse(response, HPD_SIMULATOR, responseType);
            checkResponseID(request, response);

        }
        transactionInstance.save(entityManager);
        return response;
    }

    protected void fillOutRequest(String domainKeyword, ValidatorType validator, BatchRequest request) {
        fillOutRequest(domainKeyword, validator);
        try {
            transactionInstance.getRequest().setContent(
                    HPDTransformer.getMessageAsByteArray(BatchRequest.class, request));
        } catch (JAXBException e) {
            log.error("Unable to marshall request: " + e.getMessage());
        }
    }

    protected void fillOutRequest(String domainKeyword, ValidatorType validator) {
        transactionInstance = new TransactionInstance();
        transactionInstance.setDomain(Domain.getDomainByKeyword(domainKeyword));
        transactionInstance.setSimulatedActor(simulatedActor);
        transactionInstance.setTimestamp(new Date());
        transactionInstance.setTransaction(transaction);
        transactionInstance.getRequest().setIssuingActor(sutActor);
        if (validator != null) {
            transactionInstance.getRequest().setType(validator.getName());
        }
        transactionInstance.getRequest().setIssuer(remoteHost);
    }

    protected BatchResponse fillOutResponse(BatchResponse response, String issuer, String responseType) {
        response = logErrorsAndFormatResponse(response);
        transactionInstance.getResponse().setIssuingActor(simulatedActor);
        transactionInstance.getResponse().setIssuer(issuer);
        transactionInstance.getResponse().setType(responseType);
        try {
            transactionInstance.getResponse().setContent(
                    HPDTransformer.getMessageAsByteArray(BatchResponse.class, response));
        } catch (JAXBException e) {
            log.error("Unable to marshall response: " + e.getMessage());
        }
        return response;
    }

    protected BatchResponse forwardRequestToLDAPServer(BatchRequest request, String username, String password) {
        DSMLConnector client = new DSMLConnector(ldapPartition.getServerIP(), ldapPartition.getServerPort(),
                username, password);
        BatchResponse batchResponse = null;
        try {
            batchResponse = client.processRequest(request);
        } catch (Exception e) {
            log.error(e.getMessage());
        }
        return batchResponse;
    }

    protected BatchResponse forwardRequestToLDAPServer(BatchRequest request) {
        log.info("forwarding request to LDAP server");
        String username = ApplicationConfiguration.getValueOfVariable("ldap_user");
        String password = ApplicationConfiguration.getValueOfVariable("ldap_password");
        return forwardRequestToLDAPServer(request, username, password);
    }

    /**
     * For each response present in the batchResponse, check the format and if errors are present, log them and remove them from the response content (errorMessage is not allowed by IHE and errorCode
     * shall be 0)
     *
     * @param inResponse
     * @return a batchResponse which complies with IHE rules
     */
    protected BatchResponse logErrorsAndFormatResponse(BatchResponse inResponse) {
        if (inResponse != null) {
            for (Serializable responsePart : inResponse.getBatchResponses()) {
                String localPart = ((javax.xml.bind.JAXBElement<?>) responsePart).getName().getLocalPart();
                if (localPart.equals("searchResponse")) {
                    SearchResponse searchResponse = (SearchResponse) ((javax.xml.bind.JAXBElement<?>) responsePart)
                            .getValue();
                    logErrorsAndFormatSearchResponse(searchResponse);
                } else if (localPart.equals("errorResponse")) {
                    ErrorResponse errorResponse = (ErrorResponse) ((javax.xml.bind.JAXBElement<?>) responsePart)
                            .getValue();
                    createAndSaveDirectoryErrorWithCode("unknown", errorResponse.getRequestID(), errorResponse.getMessage(), errorResponse.getType(), null);
                } else {
                    LDAPResult ldapResult = (LDAPResult) ((javax.xml.bind.JAXBElement<?>) responsePart).getValue();
                    if ((ldapResult.getErrorMessage() != null) && !ldapResult.getErrorMessage().isEmpty()) {
                        // save error message
                        String messageType = ((javax.xml.bind.JAXBElement<?>) responsePart).getName()
                                .getLocalPart().replace("Response", "Request");
                        createAndSaveDirectoryErrorWithCode(messageType, ldapResult.getRequestID(), ldapResult.getErrorMessage(), ldapResult.getResultCode().getDescr(), ldapResult.getResultCode().getCode());
                        // fix the response to match IHE spec
                        ldapResult.setErrorMessage(null);
                        ldapResult.getResultCode().setCode(0);
                        ldapResult.getResultCode().setDescr(null);
                    } else {
                        continue;
                    }
                }
            }
        }
        return inResponse;
    }

    protected void logErrorsAndFormatSearchResponse(SearchResponse searchResponse) {
        if ((searchResponse.getSearchResultDone() != null)
                && (searchResponse.getSearchResultDone().getErrorMessage() != null)
                && !searchResponse.getSearchResultDone().getErrorMessage().isEmpty()) {
            // save error message
            createAndSaveDirectoryErrorWithCode(SEARCH_REQUEST, searchResponse.getRequestID(), searchResponse.getSearchResultDone().getErrorMessage(), searchResponse.getSearchResultDone().getResultCode()
                    .getDescr(), searchResponse.getSearchResultDone().getResultCode().getCode());
            // fix the response to match IHE spec
            searchResponse.getSearchResultDone().setErrorMessage(null);
            searchResponse.getSearchResultDone().getResultCode().setCode(0);
            searchResponse.getSearchResultDone().getResultCode().setDescr(null);
        }
    }

    /**
     * Not all request types are allowed by IHE (only searchRequest, addRequest, delRequest, modifyRequest and modDNRequest) and in addition, we do not want users to access all the ldap partitions
     * present on the server side
     *
     * @param inRequest
     * @return true if the request can be forwarded to the LDAP server, false otherwise
     */
    protected BatchResponse checkRequest(BatchRequest inRequest) {
        boolean ok = true;
        BatchResponse response = new BatchResponse();
        if ((inRequest == null) || inRequest.getBatchRequests().isEmpty()) {
            return response;
        } else {
            for (Serializable requestPart : inRequest.getBatchRequests()) {
                String localName = ((javax.xml.bind.JAXBElement<?>) requestPart).getName().getLocalPart();
                if (transaction.getKeyword().equals("ITI-58")) {
                    ok = checkRequestIsSearchRequest(localName, requestPart, response, ok, "ITI-58 transaction only supports searchRequest");
                }
                if (transaction.getKeyword().equals("ITI-59")) {
                    ok = checkITI59Request(localName, requestPart, response, ok);
                }
            }
            if (ok) {
                return null;
            } else {
                return response;
            }
        }
    }

    protected void checkResponseID(BatchRequest request, BatchResponse response) {
        if (request != null && !request.batchRequests.isEmpty() && response != null && !response.getBatchResponses().isEmpty()) {
            if (response.batchResponses.size() != request.batchRequests.size()) {
                log.error("The number of responses does not match the number of requests");
            } else {
                for (int i = 0; i < request.batchRequests.size(); i++) {
                    Serializable requestPart = request.batchRequests.get(i);
                    Serializable responsePart = response.batchResponses.get(i);
                    DsmlMessage searchRequest = (DsmlMessage) ((javax.xml.bind.JAXBElement<?>) requestPart).getValue();
                    String localPart = ((javax.xml.bind.JAXBElement<?>) responsePart).getName().getLocalPart();
                    if (localPart !=null && localPart.equals("searchResponse")) {
                        SearchResponse searchResponse = (SearchResponse) ((javax.xml.bind.JAXBElement<?>) responsePart).getValue();
                        if (searchResponse != null && searchRequest !=null && searchResponse.getRequestID() !=null &&!searchResponse.getRequestID().equals(searchRequest.getRequestID())) {
                            log.info("The requestID of the searchResponse does not match the requestID of the request and was set to the proper value");
                            searchResponse.setRequestID(searchRequest.getRequestID());
                        }
                    }
                }
            }
        }
    }


    protected boolean checkRequestIsSearchRequest(String localName, Serializable requestPart, BatchResponse response, boolean ok, String errorMessage) {
        if (localName.equals(SEARCH_REQUEST)) {
            SearchRequest searchRequest = (SearchRequest) ((javax.xml.bind.JAXBElement<?>) requestPart)
                    .getValue();
            ok = ok && getLdapPartitionForDn(searchRequest.getDn(), localName);
            response.addSearchResponse(createResponsePart(searchRequest.getRequestID()));
        } else {
            ok = false;
            // log an error, transaction only accepts searchRequest messages
            DsmlMessage dsmlIncomingMessage = (DsmlMessage) ((javax.xml.bind.JAXBElement<?>) requestPart)
                    .getValue();
            createAndSaveDirectoryError(localName, dsmlIncomingMessage.getRequestID(), errorMessage);
            response.addSearchResponse(createResponsePart(dsmlIncomingMessage.getRequestID()));
        }
        return ok;
    }

    protected boolean checkITI59Request(String localName, Serializable requestPart, BatchResponse response, boolean ok) {
        switch (localName) {
            case "addRequest":
                AddRequest addRequest = (AddRequest) ((javax.xml.bind.JAXBElement<?>) requestPart).getValue();
                ok = ok && getLdapPartitionForDn(addRequest.getDn(), localName);
                response.addSearchResponse(createResponsePart(addRequest.getRequestID()));
                break;
            case "delRequest":
                DelRequest delRequest = (DelRequest) ((javax.xml.bind.JAXBElement<?>) requestPart).getValue();
                ok = ok && getLdapPartitionForDn(delRequest.getDn(), localName);
                response.addSearchResponse(createResponsePart(delRequest.getRequestID()));
                break;
            case "modifyRequest":
                ModifyRequest modifyRequest = (ModifyRequest) ((javax.xml.bind.JAXBElement<?>) requestPart)
                        .getValue();
                ok = ok && getLdapPartitionForDn(modifyRequest.getDn(), localName);
                response.addSearchResponse(createResponsePart(modifyRequest.getRequestID()));
                break;
            case "modDNRequest":
                ModifyDNRequest modDNRequest = (ModifyDNRequest) ((javax.xml.bind.JAXBElement<?>) requestPart)
                        .getValue();
                ok = ok && getLdapPartitionForDn(modDNRequest.getDn(), localName);
                response.addSearchResponse(createResponsePart(modDNRequest.getRequestID()));
                break;
            default:
                ok = false;
                // log an error, ITI-59 only accepts addRequest, delRequest, modifyRequest and modDNRequest messages
                DsmlMessage dsmlIncomingMessage = (DsmlMessage) ((javax.xml.bind.JAXBElement<?>) requestPart)
                        .getValue();
                createAndSaveDirectoryError(localName, dsmlIncomingMessage.getRequestID(), "ITI-59 transaction only supports addRequest, delRequest, modifyRequest and modDNRequest");
                response.addSearchResponse(createResponsePart(dsmlIncomingMessage.getRequestID()));
        }
        return ok;
    }


    protected void createAndSaveDirectoryErrorWithCode(String localName, String requestID, String errorMessage, TypeType codeMeaning, Integer code) {
        DirectoryErrorMessage directoryError = new DirectoryErrorMessage();
        directoryError.setCodeMeaning(codeMeaning);
        directoryError.setCode(code);
        saveDirectoryError(directoryError, localName, requestID, errorMessage);
    }

    protected void createAndSaveDirectoryErrorWithCode(String localName, String requestID, String errorMessage, LDAPResultCode codeMeaning, Integer code) {
        DirectoryErrorMessage directoryError = new DirectoryErrorMessage();
        directoryError.setCodeMeaning(codeMeaning);
        directoryError.setCode(code);
        saveDirectoryError(directoryError, localName, requestID, errorMessage);
    }

    protected void createAndSaveDirectoryError(String localName, String requestID, String errorMessage) {
        DirectoryErrorMessage directoryError = new DirectoryErrorMessage();
        saveDirectoryError(directoryError, localName, requestID, errorMessage);
    }

    protected void saveDirectoryError(DirectoryErrorMessage directoryError, String localName, String requestID, String errorMessage) {
        directoryError.setMessage(errorMessage);
        directoryError.setRequestId(requestID);
        directoryError.setTransactionInstanceId(transactionInstance.getId());
        directoryError.setClient(transactionInstance.getRequest().getIssuer());
        directoryError.setTransactionKeyword(transactionInstance.getTransaction().getKeyword());
        directoryError.setMessageType(localName);
        directoryError.save();
    }

    protected SearchResponse createResponsePart(String requestID) {
        SearchResponse responsePart = new SearchResponse();
        responsePart.setSearchResultDone(new LDAPResult());
        responsePart.getSearchResultDone().setResultCode(new ResultCode());
        responsePart.getSearchResultDone().getResultCode().setCode(0);
        responsePart.setRequestID(requestID);
        return responsePart;
    }

    /**
     * Verify that the dn is matching a partition
     *
     * @param dn
     * @param queryType
     * @return true if dn match a partition
     */
    protected boolean getLdapPartitionForDn(String dn, String queryType) {
        if ((dn != null) && !dn.isEmpty()) {
            String dnLowerCased = dn.toLowerCase();
            int index = dnLowerCased.indexOf("dc=");
            if (index >= 0) {
                setPartitionForDn(dn.substring(index));
                if (ldapPartition == null) {
                    return false;
                } else if (queryType.equals(SEARCH_REQUEST) && dn.matches("(?i)" + ldapPartition.getDnSearchRegex())) {
                    return true;
                } else {
                    return (!queryType.equals(SEARCH_REQUEST) && dn.matches("(?i)" + ldapPartition.getDnModifyRegex()));
                }
            } else {
                return false;
            }
        } else {
            ldapPartition = null;
            return false;
        }
    }

    /**
     * Search in database the partition by baseDN and set it in ldapPartition
     *
     * @param baseDN
     */
    protected void setPartitionForDn(String baseDN) {
        LDAPPartitionQuery query = new LDAPPartitionQuery();
        List<LDAPPartition> ldapPartitionList = query.getList();
        for (LDAPPartition partition : ldapPartitionList) {
            if (partition.getBaseDN().equalsIgnoreCase(baseDN)) {
                ldapPartition = partition;
            }
        }
    }
}
