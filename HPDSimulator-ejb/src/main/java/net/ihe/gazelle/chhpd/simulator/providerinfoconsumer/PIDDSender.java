package net.ihe.gazelle.chhpd.simulator.providerinfoconsumer;

import net.ihe.gazelle.chhpd.pidd.model.DownloadRequest;
import net.ihe.gazelle.chhpd.pidd.model.DownloadResponse;
import net.ihe.gazelle.hpd.simulator.initiator.SoapHPDClient;
import net.ihe.gazelle.hpd.validator.ValidatorType;
import net.ihe.gazelle.hpdTransformer.HPDTransformer;
import net.ihe.gazelle.simulator.common.ihewsinit.SoapSendException;
import net.ihe.gazelle.simulator.common.tf.model.Actor;
import net.ihe.gazelle.simulator.common.tf.model.Transaction;
import net.ihe.gazelle.simulator.sut.model.SystemConfiguration;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.xml.bind.JAXBException;

public class PIDDSender extends SoapHPDClient {

    private static final String PIDD_ACTION = "urn:ihe:iti:hpd:2010:ProviderInformationDownloadRequest";
    private static final Logger LOG = LoggerFactory.getLogger(PIDDSender.class);

    public PIDDSender(SystemConfiguration sut) {
        super(sut);
    }

    @Override
    protected String getAction() {
        return PIDD_ACTION;
    }

    @Override
    protected Actor getSimulatedActor() {
        return Actor.findActorWithKeyword("PROV_INFO_CONS");
    }

    @Override
    protected Transaction getSimulatedTransaction() {
        return Transaction.GetTransactionByKeyword("PIDD");
    }

    public DownloadResponse sendDownloadRequest(DownloadRequest request) throws SoapSendException {
        transactionInstance = null;
        DownloadResponse response = send(DownloadResponse.class, request.get_xmlNodePresentation(), PIDD_ACTION);
        String responseType = null;
        if (response != null) {
            responseType = response.get_xmlNodePresentation().getLocalName();
            if (!"SOAP Fault".equals(responseType)) {
                responseType = getResponseType();
            }
        }
        saveTransactionInstance(downloadRequestToByteArray(request),
                downloadResponseToByteArray(response), getRequestType(),
                responseType);
        return response;
    }

    protected byte[] downloadResponseToByteArray(DownloadResponse response) {
        if (response != null) {
            try {
                return HPDTransformer.getMessageAsByteArray(DownloadResponse.class, response);
            } catch (JAXBException e) {
                LOG.warn(e.getMessage());
            }
        } else {
            return null;
        }
        return null;
    }

    protected byte[] downloadRequestToByteArray(DownloadRequest request) {
        if (request != null) {
            try {
                return HPDTransformer.getMessageAsByteArray(DownloadRequest.class, request);
            } catch (JAXBException e) {
                LOG.warn(e.getMessage());
            }
        } else {
            return null;
        }
        return null;
    }

    @Override
    protected String getRequestType() {
        return ValidatorType.CH_PIDD_REQUEST.getName();
    }

    @Override
    protected String getResponseType() {
        return ValidatorType.CH_PIDD_RESPONSE.getName();
    }
}
