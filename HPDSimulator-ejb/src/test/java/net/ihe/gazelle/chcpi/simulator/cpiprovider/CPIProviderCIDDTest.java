package net.ihe.gazelle.chcpi.simulator.cpiprovider;

import net.ihe.gazelle.chcpi.action.CIDDMessageDAO;
import net.ihe.gazelle.chcpi.model.CIDDMessage;
import net.ihe.gazelle.chcpi.model.DownloadRequest;
import net.ihe.gazelle.chcpi.model.DownloadResponse;
import net.ihe.gazelle.simulator.common.tf.model.Actor;
import net.ihe.gazelle.simulator.common.tf.model.Domain;
import net.ihe.gazelle.simulator.common.tf.model.Transaction;
import org.apache.commons.io.IOUtils;
import org.jboss.seam.Component;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentMatchers;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

import javax.persistence.EntityManager;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import static org.junit.Assert.assertEquals;

@RunWith(PowerMockRunner.class)
@PrepareForTest({Component.class, Actor.class, Domain.class, CIDDMessage.class, CIDDMessageDAO.class})
public class CPIProviderCIDDTest {

    @Mock
    private EntityManager entityManager;

    CPIProvider provider;

    @Before
    public void setUpMock() {
        PowerMockito.mockStatic(Component.class);
        PowerMockito.mockStatic(Actor.class);
        PowerMockito.mockStatic(Domain.class);
        PowerMockito.mockStatic(CIDDMessage.class);
        PowerMockito.mockStatic(CIDDMessageDAO.class);
        Mockito.when(Component.getInstance("entityManager")).thenReturn(entityManager);
        Actor actor = new Actor("CPI_PROV", "CPI Provider", "CPI Provider");
        Mockito.when(Actor.findActorWithKeyword("CPI_PROV")).thenReturn(actor);

        Domain domain = new Domain();
        domain.setKeyword("EPD");
        domain.setName("CH EPD");
        domain.setDescription("Elektronische Patient Dossier");
        Mockito.when(Domain.getDomainByKeyword("EPD")).thenReturn(domain);
    }

    public void setUpNoCiddMessages() {
        List<CIDDMessage> ciddMessageList = new ArrayList<>();
        Mockito.when(CIDDMessageDAO.queryValidCIDDMessages(ArgumentMatchers.<Date>anyObject(), ArgumentMatchers.<Date>anyObject())).thenReturn(ciddMessageList);
    }

    public void setUpInInvalidCiddMessages() {
        List<CIDDMessage> ciddMessageList = new ArrayList<>();
        CIDDMessage ciddMessage = new CIDDMessage();
        ciddMessage.setMessage("blabla");
        ciddMessageList.add(ciddMessage);
        Mockito.when(CIDDMessageDAO.queryValidCIDDMessages(ArgumentMatchers.<Date>anyObject(), ArgumentMatchers.<Date>anyObject())).thenReturn(ciddMessageList);
    }

    public void setUpInValidCiddMessages() throws IOException {
        List<CIDDMessage> ciddMessageList = new ArrayList<>();
        CIDDMessage ciddMessage = new CIDDMessage();
        String message = IOUtils.toString(new FileReader("src/test/resources/ciddMessages/validCiddMessage.xml"));
        ciddMessage.setMessage(message);
        ciddMessageList.add(ciddMessage);
        Mockito.when(CIDDMessageDAO.queryValidCIDDMessages(ArgumentMatchers.<Date>anyObject(), ArgumentMatchers.<Date>anyObject())).thenReturn(ciddMessageList);
    }

    @Test
    public void ciddRequestNoMessage() {
        Transaction transaction = new Transaction("CIDD", "CIDD", "CIDD");
        Actor actor = new Actor("CPI_PROV", "CPI Provider", "CPI Provider");
        provider = Mockito.spy(new CPIProvider(
                transaction, actor, "::1"));
        DownloadRequest request = new DownloadRequest();
        request.setRequestID("id");
        request.setFromDate("2018-01-01T00:00:00.000Z");
        request.setToDate("2018-01-01T00:00:00.000Z");

        setUpNoCiddMessages();
        DownloadResponse response = provider.handleRequest(request);
        assertEquals(0, response.getBatchRequest().size());
    }

    @Test
    public void ciddRequestIncorrectMessage() {
        Transaction transaction = new Transaction("CIDD", "CIDD", "CIDD");
        Actor actor = new Actor("CPI_PROV", "CPI Provider", "CPI Provider");
        provider = Mockito.spy(new CPIProvider(
                transaction, actor, "::1"));
        DownloadRequest request = new DownloadRequest();
        request.setRequestID("id");
        request.setFromDate("2018-01-01T00:00:00.000Z");
        request.setToDate("2018-01-01T00:00:00.000Z");

        setUpInInvalidCiddMessages();
        DownloadResponse response = provider.handleRequest(request);
        assertEquals(0, response.getBatchRequest().size());
    }

    @Test
    @Ignore
    //Issue with marshalling in UT but not in jboss
    public void ciddRequestValidMessage() throws IOException {
        Transaction transaction = new Transaction("CIDD", "CIDD", "CIDD");
        Actor actor = new Actor("CPI_PROV", "CPI Provider", "CPI Provider");
        provider = Mockito.spy(new CPIProvider(
                transaction, actor, "::1"));
        DownloadRequest request = new DownloadRequest();
        request.setRequestID("id");
        request.setFromDate("2018-01-01T00:00:00.000Z");
        request.setToDate("2018-01-01T00:00:00.000Z");

        setUpInValidCiddMessages();
        DownloadResponse response = provider.handleRequest(request);
        assertEquals(1, response.getBatchRequest().size());
    }
}
