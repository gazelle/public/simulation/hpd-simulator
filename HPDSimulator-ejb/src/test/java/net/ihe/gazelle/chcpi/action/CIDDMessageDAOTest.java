package net.ihe.gazelle.chcpi.action;

import net.ihe.gazelle.chcpi.model.CIDDMessage;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;

import javax.persistence.EntityManager;
import javax.persistence.Persistence;
import java.util.Calendar;

public class CIDDMessageDAOTest {

    private static final String PERSISTENCE_UNIT_NAME_CONST = "PersistenceUnitTest";

    static EntityManager entityManager;

    @BeforeClass
    public static void initializeDB() {
        entityManager = Persistence.createEntityManagerFactory(PERSISTENCE_UNIT_NAME_CONST).createEntityManager();
    }

    @Test
    public void saveMessage() {
        CIDDMessage ciddMessage = new CIDDMessage();
        ciddMessage.setMessage("Message");
        ciddMessage.setValid(true);
        Calendar calendar = Calendar.getInstance();
        calendar.set(2019, 02, 01);
        ciddMessage.setTimestamp(calendar.getTime());

        entityManager.getTransaction().begin();
        CIDDMessage messageSaved = CIDDMessageDAO.save(ciddMessage, entityManager);
        entityManager.getTransaction().commit();
        Assert.assertNotNull(messageSaved.getId());
    }
}
