package net.ihe.gazelle.chcpi.simulator.cpiprovider;

import net.ihe.gazelle.hpd.AddRequest;
import net.ihe.gazelle.hpd.BatchRequest;
import net.ihe.gazelle.hpd.BatchResponse;
import net.ihe.gazelle.hpd.utils.SAXParserFactoryProvider;
import net.ihe.gazelle.hql.providers.EntityManagerService;
import net.ihe.gazelle.simulator.common.tf.model.Actor;
import net.ihe.gazelle.simulator.common.tf.model.Domain;
import net.ihe.gazelle.simulator.common.tf.model.Transaction;
import net.ihe.gazelle.validator.hpd.util.XMLValidation;
import org.jboss.seam.Component;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

import javax.persistence.EntityManager;

import static org.junit.Assert.assertEquals;

@RunWith(PowerMockRunner.class)
@PrepareForTest({Component.class, Actor.class, Domain.class, EntityManagerService.class, SAXParserFactoryProvider.class})
public class CPIProviderTest {

    @Mock
    private EntityManager entityManager;

    CPIProvider directory;


    @Before
    public void setUpMock() {
        PowerMockito.mockStatic(Component.class);
        PowerMockito.mockStatic(Actor.class);
        PowerMockito.mockStatic(Domain.class);
        PowerMockito.mockStatic(EntityManagerService.class);
        Mockito.when(Component.getInstance("entityManager")).thenReturn(entityManager);
        Mockito.when(EntityManagerService.provideEntityManager()).thenReturn(entityManager);
        Actor actor = new Actor("CPI_PROV", "CPI Provider", "CPI Provider");
        Mockito.when(Actor.findActorWithKeyword("CPI_PROV")).thenReturn(actor);

        Domain domain = new Domain();
        domain.setKeyword("EPD");
        domain.setName("CH EPD");
        domain.setDescription("Elektronische Patient Dossier");
        Mockito.when(Domain.getDomainByKeyword("EPD")).thenReturn(domain);

        PowerMockito.mockStatic(SAXParserFactoryProvider.class);
        String xsdPath = "src/test/resources/xsd/DSMLv2.xsd";
        Mockito.when(SAXParserFactoryProvider.getXsd()).thenReturn(xsdPath);
        Mockito.when(SAXParserFactoryProvider.getHpdFactory()).thenReturn(XMLValidation.initializeFactory(xsdPath));
    }

    @Test(expected = SchemaValidationException.class)
    public void cpiNotSearchRequest() throws SchemaValidationException {
        Transaction transaction = new Transaction("CIQ", "CIQ", "CIQ");
        Actor actor = new Actor("CPI_PROV", "CPI Provider", "CPI Provider");
        directory = Mockito.spy(new CPIProvider(
                transaction, actor, "::1"));
        BatchRequest request = new BatchRequest();
        AddRequest addRequest = new AddRequest();
        request.addAddRequest(addRequest);

        BatchResponse batchResponse = directory.handleCPIRequest(request);
    }
}
